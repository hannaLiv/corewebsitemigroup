﻿using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Enterbuy.Services.Recruitment.Controllers
{
    public class SearchController : BaseController
    {
        [Route("tim-kiem")]
        [Route("tim-kiem/{keyword}")]
        public IActionResult Index(string keyword)
        {
            ViewData["IsExpand"] = false;
            keyword = keyword.Replace("+", " ");
            ViewBag.TuKhoa = keyword;
            long page = 0;
            var data = MI.ES.BCLES.AutocompleteService.SuggestEnterBuyAsync(keyword, Utils.Utility.DefaultLang, 1, 24, out page, 0, new List<string>());

            ViewBag.total = page;
            return View(data);

        }

    }
}