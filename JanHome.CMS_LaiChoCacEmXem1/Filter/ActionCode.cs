
namespace JanHome.CMS_LaiChoCacEmXem1.Filter
{
    public enum ActionCode
    {
        CREATE,
        UPDATE,
        DELETE,
        VIEW,
        IMPORT,
        EXPORT,
        APPROVE,
        PUBLISH,
        UNPUBLISH
    }
}
