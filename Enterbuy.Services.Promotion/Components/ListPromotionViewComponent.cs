﻿using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Enterbuy.Services.Blog.Services;
using Enterbuy.Services.Common.Services.Interfaces;

namespace Enterbuy.Services.Promotion.Components
{
    public class ListPromotionViewComponent : ViewComponent
    {
        private readonly IBlogServices _blogServices;
        private readonly IMenuZoneServices _menuZoneServices;

        public ListPromotionViewComponent(IBlogServices blogServices, IMenuZoneServices menuZoneServices)
        {
            _blogServices = blogServices;
            _menuZoneServices = menuZoneServices;
        }
        public async Task<IViewComponentResult> InvokeAsync(int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 6;
            int total = 0;
            string culture_code = "vi-VN";
            var list_blog_childs = new List<ZoneByTreeViewMinify>();
            var zone_blog_parent = new ZoneByTreeViewMinify();
            var top_newest = _blogServices.GetArticlesInZoneId_Minify(0, (int)TypeZone.Promotion, culture_code, "", pageIndex, pageSize, out total);
            try
            {
                var zoneByTreeViewMinifies = await _menuZoneServices.GetZoneByTreeViewMinifies((int)TypeZone.Promotion, "vi-VN", 0);
                if (zoneByTreeViewMinifies.Count() > 0)
                {
                    zone_blog_parent = zoneByTreeViewMinifies.Where(r => r.ParentId == 0).FirstOrDefault();
                }

                if (zone_blog_parent.Id > 0)
                {
                    list_blog_childs = zoneByTreeViewMinifies.Where(r => r.ParentId == zone_blog_parent.Id).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            ViewBag.TotalPage = total;
            ViewBag.pageIndex = pageIndex;
            ViewBag.pageSize = pageSize;
            ViewBag.ZoneParent = zone_blog_parent;
            ViewBag.ZoneChilds = list_blog_childs;
            ViewBag.Top_newest = top_newest;
            return View();
        }
    }
}