﻿using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Blog.Services;
using Enterbuy.Services.Common.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.Services.Promotion.Controller
{
    public class PromotionController : BaseController
    {
        [Route("news/km-{alias}")]
        public async Task<IActionResult> Index(string alias, int? pageIndex, int? pageSize)
        {
            ViewData["IsExpand"] = false;
            ViewBag.pageIndex = pageIndex;
            ViewBag.pageSize = pageSize;
            return View();
        }
    }
}