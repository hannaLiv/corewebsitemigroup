﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class PromotionInLanguage
    {
        public int PromotionId { get; set; }
        public string LanguageCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
