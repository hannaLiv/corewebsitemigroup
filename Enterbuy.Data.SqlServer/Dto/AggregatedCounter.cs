﻿using System;
using Enterbuy.Data.Sql.Dto;

namespace Enterbuy.Data.SqlServer.Dto
{
    public class AggregatedCounter:BaseEntity
    {
        public string Key { get; set; }
        public long Value { get; set; }
        public DateTime? ExpireAt { get; set; }
    }
}
