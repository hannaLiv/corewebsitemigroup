﻿namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class LocationInLanguage
    {
        public string Name { get; set; }
        public string LanguageCode { get; set; }
        public int LocationId { get; set; }

        public Location Location { get; set; }
    }
}
