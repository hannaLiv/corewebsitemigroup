﻿using System;

namespace Enterbuy.Data.SqlServer.Dto
{
    public partial class AspNetUserTokens
    {
        public Guid UserId { get; set; }
        public string LoginProvider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public AspNetUsers User { get; set; }
    }
}
