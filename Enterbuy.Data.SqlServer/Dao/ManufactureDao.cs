﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class ManufactureDao : BaseSqlServerDao<Manufacturer>, IManufactureDao
    {
        public ManufactureDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public async Task<List<ManufactureModel>> GetManufactures(string langCode,string ids)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetManufactures";
            p.Add("@ids", ids);
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<ManufactureModel>(spName, p);
            return result;
        }
        public async Task<List<ManufactureModel>> GetManufacturesAll(string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetManufacturesAll";
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<ManufactureModel>(spName, p);
            return result;
        }
    }
}