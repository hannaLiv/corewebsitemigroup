﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.Sql;
using Dapper;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class ContactDao : BaseSqlServerDao<Contact>, IContactDao
    {
        public ContactDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }
        public int Insert(ContactViewModel contact)
        {
            int result = 0;
            Contact data = null;
            try
            {
                data = new Contact()
                {
                    Id = contact.Id,
                    Name = contact.Name,
                    Address = contact.Address,
                    Gender = contact.Gender,
                    Phone = contact.Phone,
                    Email = contact.Email,
                    Title = contact.Title,
                    Content = contact.Content,
                    Note = contact.Note,
                    Type = contact.Type,
                    Source = contact.Source,
                    CreatedDate = contact.CreatedDate,
                    ModifiedBy = contact.ModifiedBy,
                    ModifiedDate = contact.ModifiedDate,
                    Status = contact.Status,
                    UrlRef = contact.UrlRef
                };

                Add(data);
                result = 1;
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return result;
        }

    }
}
