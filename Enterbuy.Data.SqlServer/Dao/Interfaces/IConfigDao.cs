﻿using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface IConfigDao : ISqlServerDao<Config>
    {
        ConfigViewModel GetArticlesInZoneId_Minify(string configName, string langCode);
        int CreateRating(int objectId, int objectType, int rate);
        int CreateComment(int objectId, int objectType, string name, string phoneOrEmail, string avatar, string content, string type, int rate, string lang_code, int parentId);
        int CreateContact(ServiceTicket ticket);
        int CreateViewCount(int objectId, string type);
        List<ConfigView> GetAll();

        Redirect GetUrlRedrect(string url);
    }
}
