﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface IZoneDao : ISqlServerDao<Zone>
    {
        Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewMinifies(int type, string langCode, int parentId);

        Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewShowMenuMinifies(int type, string langCode, int parentId,
            int isShowMenu);

        List<ProductMinify> GetProductInRegionByZoneIdMinify(int zoneId, int locationId, string langCode,
            int pageNumber, int pageSize, out int total);

        Task<ZoneToRedirect> GetZoneByAlias(string url, string langCode);

        Task<ZoneDetail> GetZoneDetail(int zoneId, string langCode);
        Task<ZoneDetail> GetZoneInfo(int zoneId, string langCode);
        List<ZoneDetail> GetZoneUrl(List<int> zoneId, string langCode);

        Task<List<ZoneByTreeViewMinify>> GetBreadcrumbByZoneId(int zoneId, string langCode);

        Task<RedrectDetail> GetObjectByAlias(string url, string lang_code);
        Task<List<ZoneByTreeViewMinify>> GetListZoneByParentId(int type, string lang_code);
        ZoneId GetZoneByArticleId(int id);
    }
}
