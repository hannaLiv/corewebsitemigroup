﻿using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface ILocationDao
    {
        List<StoreResponse> GetNearLocation(float Longitude, float Latitude, int distance, string langCode, int sortOrder, out int totalRows);

        List<LocationViewModel> GetLocations(string lang_code);
        LocationViewModel GetLocationFirst(string lang_code);
    }
}
