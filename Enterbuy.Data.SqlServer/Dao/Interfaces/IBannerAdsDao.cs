﻿using System.Threading.Tasks;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface IBannerAdsDao : ISqlServerDao<Ads>
    {
        Task<BannerAdsViewModel> GetBannerAdsByCode(string langCode, string code);
        string GetConfigByName(string langCode, string name);
    }
}