﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface IManufactureDao : ISqlServerDao<Manufacturer>
    {
        Task<List<ManufactureModel>> GetManufactures(string langCode,string Ids);
        Task<List<ManufactureModel>> GetManufacturesAll(string langCode);
    }
}