﻿using System;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System.Collections.Generic;
using System.Threading.Tasks;
using Enterbuy.Statics.Utilities;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface IProductDao : ISqlServerDao<Product>
    {
        List<ProductMinify> FilterProductBySpectificationsInZone(FilterProductBySpectification fp, out int total);
        List<ProductSaleInMonth> GetProductInFlashSale(int fSaleId, int locationId, string lanngCode, int pageIndex,
            int pageSize, out int total);
        Task<List<DiscountProgram>> GetFlashSaleByTime(DateTime time);
        List<ProductMinify> GetProductMinifiesTreeViewByZoneParentId(int parentId, string langCode, int locationId, int pageNumber, int pageSize, out int total);
        List<ProductMinify> GetProductMinifiesTreeViewByZoneParentId_Rss(int parentId, string langCode, int locationId, int pageNumber, int pageSize, out int total);
        Task<ProductDetail> GetProductInfomationDetail(int id, string langCode);
        Task<List<BankInstallment>> GetAllBankInstallment(string langCode);
        Task<List<ProductSpectificationDetail>> GetProductSpectificationDetail(int id, string langCode);
        List<FilterAreaCooked> GetFilterProductByZoneId(int zone_id, string lang_code);
        Task<List<ProductPriceInLocationDetail>> GetProductPriceInLocationDetail(int productId, string langCode);
        List<ProductMinify> GetProductsInProductById(int productId, string type, int locationId, string langCode,
            int pageIndex, int pageSize, out int totalRow);
        Task<List<PromotionInProduct>> GetPromotionInProduct(int productId, string langCode);
        List<ProductMinify> GetProductInZoneByZoneIdMinify(int zoneId, int locationId, string langCode,
            int pageNumber, int pageSize, out int total);
        Task<List<CommentDetail>> GetCommentPublisedByObjectId(int id, int type, int pageIndex = 1, int pageSize = 10);
        decimal GetRatingByObjectId(int objectId, int objectType);
        List<ProductMinify> GetProductInListProductsMinifyWithTotalPrice(string productIds, int locationId, string langCode, int pageIndex, int pageSize, out int totalRow, out int totalPrice);
        List<ProductMinify> GetProductInListProductsMinify(string productIds, int locationId, string langCode, int pageIndex, int pageSize, out int totalRow);
        List<PromotionViewModel> GetAllPromotions(string langCode);
        List<PromotionViewModel> GetPromotionsById(string langCode, List<int> id);
        List<Coupon> GetCouponsById(List<string> lstCode,List<int> lstIdProduct);
        int CreateOrderInWebsite(OrderViewModel orders);
        int CreateOrderInstallmentInWebsite(OrderViewModel orders);
        List<ProductMinify> GetProductInRegionByZoneIdMinify(int zone_id, int locationId, string lang_code, int pageNumber, int pageSize, out int total);
        List<PropertyDetail> GetPropertyDetails(string lang_code);
        List<ProductMinify> GetProductsInRegionByZoneParentIdSkipping(int parentId, string lang_code, int locationId, int skip, int size);
        List<ProductMinify> GetProductsInRegionByZoneParentIdShowLayout_Skipping(int parentId, string lang_code, int locationId, int skip, int size);
        Coupon ExistVoucher(int productId, string voucher);

        RedrectDetail GetObjectByAlias(string url, string lang_code);

    }
}