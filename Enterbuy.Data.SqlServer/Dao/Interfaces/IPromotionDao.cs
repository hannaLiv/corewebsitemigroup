﻿using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface IPromotionDao : ISqlServerDao<Promotion>
    {
        List<PromotionViewModel> GetAllPromotions(string lang_code);

        List<ProductField> CheckPromotion_ByProductId(List<int> listId);
    }
}
