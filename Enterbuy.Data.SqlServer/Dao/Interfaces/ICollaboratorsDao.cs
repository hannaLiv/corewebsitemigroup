﻿using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.Dao.Interfaces
{
    public interface ICollaboratorsDao
    {
        List<OrderDetailViewModel> GetOrderByEmail(string email);
    }
}
