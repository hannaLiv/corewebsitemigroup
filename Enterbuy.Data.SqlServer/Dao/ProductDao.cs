﻿using System;
using Dapper;
using Enterbuy.Data;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Enterbuy.Statics.Configurations;
using Enterbuy.Statics.Utilities;
using Microsoft.Extensions.Configuration;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class ProductDao : BaseSqlServerDao<Product>, IProductDao
    {
        private readonly string _connStr;
        private readonly IExecuters _executers;
        public ProductDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
            _connStr = AppSettings.Get<string>("Databases:SqlServer:ConnectionStrings:Default");
        }

        public List<ProductSaleInMonth> GetProductInFlashSale(int fSaleId, int locationId, string lanng_code, int pageIndex, int pageSize, out int total)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductInFlashSale_Enterbuy";
            p.Add("@fSaleId", fSaleId);
            p.Add("@locationId", locationId);
            p.Add("@lang_code", lanng_code);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductSaleInMonth>(spName, p);
            total = p.Get<int>("@total");
            return result;
        }
        public List<ProductMinify> FilterProductBySpectificationsInZone(FilterProductBySpectification fp, out int total)
        {
            DataTable fillter_cooked = new DataTable();
            fillter_cooked.Columns.Add("SpectificationId", typeof(int));
            fillter_cooked.Columns.Add("Value", typeof(string));

            if (fp.filter != null)
            {
                foreach (var item in fp.filter)
                {
                    if (item.Value != null)
                        fillter_cooked.Rows.Add(item.SpectificationId, item.Value);
                }
            }
            var p = new DynamicParameters();
            var spName = "usp_Web_FilterProductBySpectifications";
            p.Add("@parentId", fp.parentId);
            p.Add("@lang_code", fp.lang_code);
            p.Add("@locationId", fp.locationId);
            p.Add("@manufacture_id", fp.manufacture_id);
            p.Add("@min_price", fp.min_price);
            p.Add("@max_price", fp.max_price);
            p.Add("@sort_price", fp.sort_price);
            p.Add("@sort_rate", fp.sort_rate);
            p.Add("@color_code", "");
            p.Add("@filter_text", fp.filter_text == null ? "" : fp.filter_text);
            p.Add("@material_type", fp.material_type);
            p.Add("@filter", fillter_cooked.AsTableValuedParameter("type_filterProductBySpectification"));
            p.Add("@pageNumber", fp.pageNumber);
            p.Add("@pageSize", fp.pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductMinify>(spName, p);
            
            total = p.Get<int>("@total");
            return result;
        }
        public async Task<List<DiscountProgram>> GetFlashSaleByTime(DateTime time)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetFlashSaleByTime";
            p.Add("@time", time);
            var result = await QuerySPAsync<DiscountProgram>(spName, p);
            return result;
        }

        public List<ProductMinify> GetProductMinifiesTreeViewByZoneParentId(int parentId, string langCode, int locationId, int pageNumber,
            int pageSize, out int total)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductTreeviewByZoneParentShowLayout";
            p.Add("@parentId", parentId);
            p.Add("@lang_code", langCode);
            p.Add("@locationId", locationId);
            p.Add("@pageNumber", pageNumber);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductMinify>(spName, p);
            total = p.Get<int>("@total");
            return result;
        }

        public List<ProductMinify> GetProductMinifiesTreeViewByZoneParentId_Rss(int parentId, string langCode, int locationId, int pageNumber,
    int pageSize, out int total)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductTreeviewByZoneParentShowLayout_Rss";
            p.Add("@parentId", parentId);
            p.Add("@lang_code", langCode);
            p.Add("@locationId", locationId);
            p.Add("@pageNumber", pageNumber);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductMinify>(spName, p);
            total = p.Get<int>("@total");
            return result;
        }



        public async Task<ProductDetail> GetProductInfomationDetail(int id, string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductInfomationDetail";
            p.Add("@id", id);
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<ProductDetail>(spName, p);
            return result.FirstOrDefault();
        }
        public async Task<List<BankInstallment>> GetAllBankInstallment(string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetAllBankInstallment";
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<BankInstallment>(spName, p);
            return result;
        }


        public RedrectDetail GetObjectByAlias(string url, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetObjectByAlias";
            p.Add("@url", url);
            p.Add("@lang_code", lang_code);
            var result = QuerySP<RedrectDetail>(commandText, p);
            return result.FirstOrDefault();
        }

        public async Task<List<ProductSpectificationDetail>> GetProductSpectificationDetail(int id, string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductSpecificationDetail";
            p.Add("@id", id);
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<ProductSpectificationDetail>(spName, p);
            return result;
        }

        public async Task<List<ProductPriceInLocationDetail>> GetProductPriceInLocationDetail(int productId, string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductPriceInLocations";
            p.Add("@productId", productId);
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<ProductPriceInLocationDetail>(spName, p);
            return result;
        }

        public List<FilterAreaCooked> GetFilterProductByZoneId(int zone_id, string lang_code)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetFilterAreaByZoneId";
            p.Add("@zoneId", zone_id);
            p.Add("@lang_code", lang_code);
            var result = QuerySP<FilterArea>(spName, p);

            //start cooking
            var q = from r in result
                    group r by new
                    {
                        r.SpectificationId,
                        r.Name
                    } into r_after
                    select new FilterAreaCooked()
                    {
                        SpectificationId = r_after.Key.SpectificationId,
                        Name = r_after.Key.Name,
                        Values = r_after.ToList()
                    };

            return q.ToList();

        }

        public List<ProductMinify> GetProductsInProductById(int productId, string type, int locationId, string langCode, int pageIndex, int pageSize,
            out int totalRow)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductInProductMinify";
            p.Add("@productId", productId);
            p.Add("@locationId", locationId);
            p.Add("@type", type);
            p.Add("@lang_code", langCode);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@total_row", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductMinify>(spName, p);

            totalRow = p.Get<int>("@total_row");
            return result;
        }

        public async Task<List<PromotionInProduct>> GetPromotionInProduct(int productId, string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetPromotionsInProduct";
            p.Add("@productId", productId);
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<PromotionInProduct>(spName, p);

            return result;
        }

        public List<ProductMinify> GetProductInZoneByZoneIdMinify(int zoneId, int locationId, string langCode, int pageNumber, int pageSize,
            out int total)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductInZoneByZoneId_Minify";
            p.Add("@zone_id", zoneId);
            p.Add("@lang_code", langCode);
            p.Add("@locationId", locationId);
            p.Add("@pageNumber", pageNumber);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductMinify>(spName, p);

            total = p.Get<int>("@total");
            return result;
        }

        public async Task<List<CommentDetail>> GetCommentPublisedByObjectId(int id, int type, int pageIndex = 1, int pageSize = 10)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetCommentPublisedByObjectId";
            p.Add("@id", id);
            p.Add("@type", type);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            var result = await QuerySPAsync<CommentDetail>(spName, p);

            return result;
        }

        public decimal GetRatingByObjectId(int objectId, int objectType)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetRatingByObjectId";
            p.Add("@objectId", objectId);
            p.Add("@objectType", objectType);
            p.Add("@rateAvg", dbType: System.Data.DbType.Decimal, direction: System.Data.ParameterDirection.Output);
            var exec = ExecuteScalarSp(spName, p);
            var result = p.Get<decimal>("@rateAvg");
            return result;
        }

        public List<ProductMinify> GetProductInListProductsMinifyWithTotalPrice(string productIds, int locationId, string langCode, int pageIndex,
            int pageSize, out int totalRow, out int totalPrice)
        {
            using (var conn = new SqlConnection(_connStr))
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                var p = new DynamicParameters();
                var spName = "usp_Web_GetProductInListProductsMinify_WithTotalPrice";
                p.Add("@productIds", productIds);
                p.Add("@locationId", locationId);
                p.Add("@lang_code", langCode);
                p.Add("@pageIndex", pageIndex);
                p.Add("@pageSize", pageSize);
                p.Add("@total_row", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                p.Add("@total_price", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                var result = conn.Query<ProductMinify>(spName, p, commandType: System.Data.CommandType.StoredProcedure)
                    .ToList();
                totalRow = p.Get<int>("@total_row");
                totalPrice = p.Get<int>("@total_price");
                return result;
            }

        }

        public List<ProductMinify> GetProductInListProductsMinify(string productIds, int locationId, string langCode, int pageIndex, int pageSize,
            out int totalRow)
        {
            using (var conn = new SqlConnection(_connStr))
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }

                var p = new DynamicParameters();
                var spName = "usp_Web_GetProductInListProductsMinify";
                p.Add("@productIds", productIds);
                p.Add("@locationId", locationId);
                p.Add("@lang_code", langCode);
                p.Add("@pageIndex", pageIndex);
                p.Add("@pageSize", pageSize);
                p.Add("@total_row", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                var result = conn.Query<ProductMinify>(spName, p, commandType: System.Data.CommandType.StoredProcedure)
                    .ToList();
                totalRow = p.Get<int>("@total_row");
                return result;
            }
        }

        public List<PromotionViewModel> GetAllPromotions(string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetAllPromotions";
            p.Add("@lang_code", langCode);
            var result = QuerySP<PromotionViewModel>(spName, p);
            return result;
        }
        public List<PromotionViewModel> GetPromotionsById(string langCode, List<int> lstId)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_Promotion_GetByListId";
            p.Add("@lang_code", langCode);
            p.Add("@id", String.Join(",", lstId));
            var result = QuerySP<PromotionViewModel>(spName, p);
            return result;
        }
        public List<Coupon> GetCouponsById(List<string> lstCode,List<int> lstIdProduct)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_Voucher_GetByListCode";
            p.Add("@code", String.Join(",", lstCode));
            p.Add("@productId", String.Join(",", lstIdProduct));
            var result = QuerySP<Coupon>(spName, p);
            return result;
        }

        public int CreateOrderInWebsite(OrderViewModel orders)
        {
            DataTable or_Detail = new DataTable();
            or_Detail.Columns.Add("ProductId", typeof(int));
            or_Detail.Columns.Add("Quantity", typeof(decimal));
            or_Detail.Columns.Add("LogPrice", typeof(decimal));
            or_Detail.Columns.Add("OrderSourceType", typeof(int));
            or_Detail.Columns.Add("OrderSourceId", typeof(int));
            or_Detail.Columns.Add("Voucher", typeof(string));
            or_Detail.Columns.Add("VoucherType", typeof(byte));
            or_Detail.Columns.Add("VoucherPrice", typeof(double));
            or_Detail.Columns.Add("VoucherMeta", typeof(string));
            or_Detail.Columns.Add("Vat", typeof(decimal));
            or_Detail.Columns.Add("VatPrice", typeof(string));
            DataTable or_Promotions = new DataTable();
            or_Promotions.Columns.Add("ProductId", typeof(int));
            or_Promotions.Columns.Add("LogName", typeof(string));
            or_Promotions.Columns.Add("LogValue", typeof(decimal));
            or_Promotions.Columns.Add("LogType", typeof(string));
            
            foreach (var item in orders.Products)
            {
                or_Detail.Rows.Add(item.ProductId, item.Quantity, item.LogPrice, item.OrderSourceType, item.OrderSourceId, item.Voucher, item.VoucherType, item.VoucherPrice, item.VoucherMeta,item.Vat,item.VatPrice);
                if (item.Promotions != null)
                {
                    foreach (var b in item.Promotions)
                        or_Promotions.Rows.Add(item.ProductId, b.LogName, b.LogValue, b.LogType);
                }
            }

            var p = new DynamicParameters();
            var spName = "usp_Web_CreateOrder";
            p.Add("@Vat", orders.Customer.VAT);
            p.Add("@cus_Name", orders.Customer.Name);
            p.Add("@cus_Gender", orders.Customer.Gender);
            p.Add("@cus_PhoneNumber", orders.Customer.PhoneNumber);
            p.Add("@cus_Address", orders.Customer.Address);
            p.Add("@or_Code", orders.OrderCode);
            p.Add("@or_MetaData", Newtonsoft.Json.JsonConvert.SerializeObject(orders.Extras));
            p.Add("@or_Note", orders.Customer.Note);
            p.Add("@or_Detail", or_Detail.AsTableValuedParameter("type_OrderProduct_v6"));
            p.Add("@or_Promotions", or_Promotions.AsTableValuedParameter("type_OrderPromotionInProduct_v1"));
            p.Add("@out_order_id", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

            var result = ExecuteSp(spName, p);
            result = p.Get<int>("@out_order_id");
            return result;

        }

        public int CreateOrderInstallmentInWebsite(OrderViewModel orders)
        {
            DataTable or_Detail = new DataTable();
            or_Detail.Columns.Add("ProductId", typeof(int));
            or_Detail.Columns.Add("Quantity", typeof(decimal));
            or_Detail.Columns.Add("LogPrice", typeof(decimal));
            or_Detail.Columns.Add("OrderSourceType", typeof(int));
            or_Detail.Columns.Add("OrderSourceId", typeof(int));
            or_Detail.Columns.Add("Voucher", typeof(string));
            or_Detail.Columns.Add("VoucherType", typeof(byte));
            or_Detail.Columns.Add("VoucherPrice", typeof(double));

            DataTable or_Promotions = new DataTable();
            or_Promotions.Columns.Add("ProductId", typeof(int));
            or_Promotions.Columns.Add("LogName", typeof(string));
            or_Promotions.Columns.Add("LogValue", typeof(decimal));
            or_Promotions.Columns.Add("LogType", typeof(string));
            foreach (var item in orders.Products)
            {
                or_Detail.Rows.Add(item.ProductId, item.Quantity, item.LogPrice, item.OrderSourceType, item.OrderSourceId, item.Voucher, item.VoucherType, item.VoucherPrice);
                if (item.Promotions != null)
                {
                    foreach (var b in item.Promotions)
                        or_Promotions.Rows.Add(item.ProductId, b.LogName, b.LogValue, b.LogType);
                }
            }

            var p = new DynamicParameters();
            var spName = "[usp_Web_CreateOrderInstallment]";
            p.Add("@Vat", orders.Customer.VAT);
            p.Add("@cus_Name", orders.Customer.Name);
            p.Add("@cus_Gender", orders.Customer.Gender);
            p.Add("@cus_PhoneNumber", orders.Customer.PhoneNumber);
            p.Add("@cus_Address", orders.Customer.Address);
            p.Add("@or_Code", orders.OrderCode);
            p.Add("@or_MetaData", Newtonsoft.Json.JsonConvert.SerializeObject(orders.Extras));
            p.Add("@or_MetaOrder", orders.Extras_MetaOrder);
            p.Add("@or_Note", orders.Customer.Note);
            p.Add("@or_Detail", or_Detail.AsTableValuedParameter("type_OrderProduct_v4"));
            p.Add("@or_Promotions", or_Promotions.AsTableValuedParameter("type_OrderPromotionInProduct_v1"));
            p.Add("@out_order_id", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            p.Add("@or_source_type", orders.Order_Source_Type);
            var result = ExecuteSp(spName, p);
            result = p.Get<int>("@out_order_id");
            return result;

        }

        public List<PropertyDetail> GetPropertyDetails(string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetPropertiesByLanguage";
            p.Add("@lang_code", lang_code);
            var result = QuerySP<PropertyDetail>(commandText, p);
            return result;
        }

        public List<ProductMinify> GetProductInRegionByZoneIdMinify(int zone_id, int locationId, string lang_code, int pageNumber, int pageSize, out int total)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductInRegionByZoneId_Minify";
            p.Add("@zone_id", zone_id);
            p.Add("@lang_code", lang_code);
            p.Add("@locationId", locationId);
            p.Add("@pageNumber", pageNumber);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductMinify>(commandText, p);
            total = p.Get<int>("@total");
            return result;
        }

        public List<ProductMinify> GetProductsInRegionByZoneParentIdSkipping(int parentId, string lang_code, int locationId, int skip, int size)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductsInRegionByZoneParentMinify_Skipping";
            p.Add("@parentId", parentId);
            p.Add("@lang_code", lang_code);
            p.Add("@locationId", locationId);
            p.Add("@skip", skip);
            p.Add("@size", size);
            var result = QuerySP<ProductMinify>(commandText, p);
            return result;
        }
        public List<ProductMinify> GetProductsInRegionByZoneParentIdShowLayout_Skipping(int parentId, string lang_code, int locationId, int skip, int size)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetProductTreeviewByZoneParentShowLayout_Skipping";
            p.Add("@parentId", parentId);
            p.Add("@lang_code", lang_code);
            p.Add("@locationId", locationId);
            p.Add("@skip", skip);
            p.Add("@size", size);
            var result = QuerySP<ProductMinify>(commandText, p);
            return result;
        }

        public Coupon ExistVoucher(int productId, string voucher)
        {
            var p = new DynamicParameters();
            var commandText = "SP_ExistVoucherByProductId";
            p.Add("@ProductId", productId);
            p.Add("@@Voucher", voucher);
            var result = QuerySP<Coupon>(commandText, p);
            if (result != null && result.Any())
                return result.LastOrDefault();
            return new Coupon(); ;
        }
        //public List<ProductMinify> FilterProductBySpectificationsInZone(FilterProductBySpectification fp, out int total)
        //{
        //    DataTable fillter_cooked = new DataTable();
        //    fillter_cooked.Columns.Add("SpectificationId", typeof(int));
        //    fillter_cooked.Columns.Add("Value", typeof(string));

        //    if (fp.filter != null)
        //    {
        //        foreach (var item in fp.filter)
        //        {
        //            if (item.Value != null)
        //                fillter_cooked.Rows.Add(item.SpectificationId, item.Value);
        //        }
        //    }
        //    var p = new DynamicParameters();
        //    var commandText = "usp_Web_FilterProductBySpectifications";
        //    p.Add("@parentId", fp.parentId);
        //    p.Add("@lang_code", fp.lang_code);
        //    p.Add("@locationId", fp.locationId);
        //    p.Add("@manufacture_id", fp.manufacture_id);
        //    p.Add("@min_price", fp.min_price);
        //    p.Add("@max_price", fp.max_price);
        //    p.Add("@sort_price", fp.sort_price);
        //    p.Add("@sort_rate", fp.sort_rate);
        //    p.Add("@color_code", fp.color_code == null ? "" : fp.color_code);
        //    p.Add("@filter_text", fp.filter_text == null ? "" : fp.filter_text);
        //    p.Add("@material_type", fp.material_type);
        //    p.Add("@filter", fillter_cooked.AsTableValuedParameter("type_filterProductBySpectification"));
        //    p.Add("@pageNumber", fp.pageNumber);
        //    p.Add("@pageSize", fp.pageSize);
        //    p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
        //    var result = _executers.ExecuteCommand(_connStr, conn => conn.Query<ProductMinify>(commandText, p, commandType: System.Data.CommandType.StoredProcedure)).ToList();
        //    total = p.Get<int>("@total");
        //    return result;
        //}

    }
}