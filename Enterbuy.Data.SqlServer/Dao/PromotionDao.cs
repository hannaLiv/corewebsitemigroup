﻿using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class PromotionDao : BaseSqlServerDao<Promotion>, IPromotionDao
    {
        public PromotionDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }

        public List<PromotionViewModel> GetAllPromotions(string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetAllPromotions";

            p.Add("@lang_code", lang_code);
            var result = QuerySP<PromotionViewModel>(commandText, p);
            return result;
        }

        public List<ProductField> CheckPromotion_ByProductId(List<int> listId)
        {
            var p = new DynamicParameters();
            var commandText = "SP_CheckPromotion_ByProductId";
            p.Add("@Id", String.Join(",", listId));
            var result = QuerySP<ProductField>(commandText, p);
            return result;
        }
    }
}
