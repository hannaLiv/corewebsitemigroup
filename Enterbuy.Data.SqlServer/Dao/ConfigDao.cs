﻿using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Enterbuy.Data.SqlServer.Dao.Interfaces;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class ConfigDao : BaseSqlServerDao<Config>, IConfigDao
    {
        public ConfigDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }


        public int CreateComment(int objectId, int objectType, string name, string phoneOrEmail, string avatar, string content, string type, int rate, string lang_code, int parentId)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_CreateCommentInWebsite";
            p.Add("@objectId", objectId);
            p.Add("@objectType", objectType);
            p.Add("@name", name);
            p.Add("@phoneOrMail", phoneOrEmail);
            p.Add("@avatar", avatar);
            p.Add("@content", content);
            p.Add("@type", type);
            p.Add("@rate", rate);
            p.Add("@parentId", parentId);
            p.Add("@lang_code", lang_code);
            p.Add("@insertedId", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

            ExecuteScalarSp(commandText, p);
            var insertedId = p.Get<int>("@insertedId");
            return insertedId;
        }

        public int CreateContact(ServiceTicket ticket)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_CreateContact";
            p.Add("@Name", ticket.Name);
            p.Add("@Phone", ticket.Phone);
            p.Add("@Address", ticket.Address);
            p.Add("@Title", ticket.Title);
            p.Add("@Content", ticket.Content);
            p.Add("@Type", ticket.Type);
            p.Add("@Source", ticket.Source);
            p.Add("@Inserted", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

            ExecuteScalarSp(commandText, p);
            var insertedId = p.Get<int>("@Inserted");
            return insertedId;
        }

        public int CreateRating(int objectId, int objectType, int rate)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_CreateRating";
            p.Add("@objectId", objectId);
            p.Add("@objectType", objectType);
            p.Add("@rate", rate);
            p.Add("@insertedId", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            ExecuteScalarSp(commandText, p);
            var insertedId = p.Get<int>("@insertedId");
            return insertedId;
        }

        public int CreateViewCount(int objectId, string type)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_AddViewCount";
            p.Add("@objectId", objectId);
            p.Add("@type", type);

            p.Add("@insertedId", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);

            ExecuteScalarSp(commandText, p);
            var insertedId = p.Get<int>("@insertedId");
            return insertedId;
        }

        public ConfigViewModel GetArticlesInZoneId_Minify(string configName, string langCode)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetConfigByName";
            p.Add("@configName", configName);
            p.Add("@lang_code", langCode);
            var result = QuerySP<ConfigViewModel>(commandText, p);
            return result.FirstOrDefault();
        }
        public List<ConfigView> GetAll()
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetConfigAll";
            var result = QuerySP<ConfigView>(commandText, p);
            return result.ToList();
        }

        public Redirect GetUrlRedrect(string url)
        {
            var p = new DynamicParameters();
            var commandText = "SP_Get_UrlRediect";
            p.Add("@Url", url);
            var result = QuerySP<Redirect>(commandText, p);
            if (result != null && result.Count > 0)
                return result.FirstOrDefault();
            else return new Redirect();
        }
    }
}
