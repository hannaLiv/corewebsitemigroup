﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System.Linq;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class ZoneDao : BaseSqlServerDao<Zone>, IZoneDao
    {
        public ZoneDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }

        public async Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewMinifies(int type, string langCode, int parentId)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetZoneByTreeView_Minify_v1";
            p.Add("@type", type);
            p.Add("@lang_code", langCode);
            p.Add("@parentId", parentId);
            var result = await QuerySPAsync<ZoneByTreeViewMinify>(spName, p);
            return result;
        }

        public async Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewShowMenuMinifies(int type, string lang_code, int parentId, int isShowMenu)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetZoneByTreeView_Minify_ShowMenu_Enterbuy";
            p.Add("@type", type);
            p.Add("@lang_code", lang_code);
            p.Add("@parentId", parentId);
            p.Add("@isShowMenu", isShowMenu);
            var result = await QuerySPAsync<ZoneByTreeViewMinify>(spName, p);
            return result;
        }

        public List<ProductMinify> GetProductInRegionByZoneIdMinify(int zoneId, int locationId, string langCode, int pageNumber, int pageSize,
            out int total)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetProductInRegionByZoneId_Minify";
            p.Add("@zone_id", zoneId);
            p.Add("@lang_code", langCode);
            p.Add("@locationId", locationId);
            p.Add("@pageNumber", pageNumber);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ProductMinify>(spName, p);
            total = p.Get<int>("@total");
            return result;

        }
        public async Task<ZoneToRedirect> GetZoneByAlias(string url, string lang_code)
        {
            var result = new ZoneToRedirect();
            var p = new DynamicParameters();
            var spName = "usp_Web_GetZoneByAlias";
            p.Add("@url", url);
            p.Add("@lang_code", lang_code);
            var resultZoneToRedirects = await QuerySPAsync<ZoneToRedirect>(spName, p);
            result = resultZoneToRedirects.FirstOrDefault();
            return result;
        }

        public async Task<ZoneDetail> GetZoneDetail(int zoneId, string langCode)
        {
            var result = new ZoneDetail();
            var p = new DynamicParameters();
            var spName = "usp_Web_GetZoneDetail";
            p.Add("@id", zoneId);
            p.Add("@lang_code", langCode);
            var resultZonetDetail = await QuerySPAsync<ZoneDetail>(spName, p);
            result = resultZonetDetail.FirstOrDefault();
            return result;
        }

        public async Task<List<ZoneByTreeViewMinify>> GetBreadcrumbByZoneId(int zoneId, string langCode)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_BreadcrumbByZoneId";
            p.Add("@zone_id", zoneId);
            p.Add("@lang_code", langCode);
            var result = await QuerySPAsync<ZoneByTreeViewMinify>(spName, p);
            return result;
        }

        public async Task<RedrectDetail> GetObjectByAlias(string url, string lang_code)
        {
            var p = new DynamicParameters();
            var spName = "usp_Web_GetObjectByAlias";
            p.Add("@url", url);
            p.Add("@lang_code", lang_code);
            var result = await QuerySPAsync<RedrectDetail>(spName, p);
            return result.FirstOrDefault();
        }

        public async Task<List<ZoneByTreeViewMinify>> GetListZoneByParentId(int type, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetZoneParentByType";
            p.Add("@type", type);
            p.Add("@lang_code", lang_code);

            var result = await QuerySPAsync<ZoneByTreeViewMinify>(commandText, p);
            return result;
        }

        public async Task<ZoneDetail> GetZoneInfo(int id, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetZoneInfo";
            p.Add("@id", id);
            p.Add("@lang_code", lang_code);

            var result = await QuerySPAsync<ZoneDetail>(commandText, p);
            if (result != null && result.Count > 0)
            {
                return result.FirstOrDefault();
            }
            return new ZoneDetail();
        }
        public List<ZoneDetail> GetZoneUrl(List<int> id, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetZoneUrl";
            p.Add("@id", string.Join(",", id));
            p.Add("@lang_code", lang_code);

            var result = QuerySP<ZoneDetail>(commandText, p);
            return result;
        }
        public ZoneId GetZoneByArticleId(int id)
        {
            var p = new DynamicParameters();
            var commandText = "SP_GetZoneByArticleId";
            p.Add("@ArticleId", id);
            var result = QuerySP<ZoneId>(commandText, p);
            if (result != null && result.Count > 0)
                return result.FirstOrDefault();
            else
                return new ZoneId() { Id = 0 };
        }
    }
}

