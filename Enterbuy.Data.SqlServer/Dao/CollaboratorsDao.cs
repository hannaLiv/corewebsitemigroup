﻿using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class CollaboratorsDao : BaseSqlServerDao<Product>, ICollaboratorsDao
    {
        public CollaboratorsDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }
        public List<OrderDetailViewModel> GetOrderByEmail(string email)
        {
            var p = new DynamicParameters();
            var commandText = "SP_GetOrder_ByEmail";
            p.Add("@Email", email);
            var result = QuerySP<OrderDetailViewModel>(commandText, p);
            return result;
        }
    }
}
