﻿using AutoMapper.Configuration;
using Dapper;
using Enterbuy.Data.Sql;
using Enterbuy.Data.Sql.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enterbuy.Data.SqlServer.Dao
{
    public class ArticleDao : BaseSqlServerDao<Article>, IArticleDao
    {
        public ArticleDao(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {

        }

        public ArticleDetail GetArticleDetail(int article_id, string lang_code)
        {
            var param = new DynamicParameters();
            var spName = "usp_Web_GetArticleById";
            param.Add("@id", article_id);
            param.Add("@lang_code", lang_code);
            var result = QuerySP<ArticleDetail>(spName, param);
            return result.FirstOrDefault();
        }
        public List<ArticleMinify> GetArticlesInZoneId_Minify_Rss(int zoneId, int zone_type, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticlesInZoneId_Minify_Rss";
            p.Add("@zoneId", zoneId);
            p.Add("@Type", zone_type);
            p.Add("@lang_code", lang_code);
            p.Add("@filter", filter);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ArticleMinify>(commandText, p);
            totalRow = p.Get<int>("@totalRow");
            return result;
        }

        public List<ArticleMinify> GetArticlesInZoneId_Minify(int zoneId, int zone_type, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticlesInZoneId_Minify";
            p.Add("@zoneId", zoneId);
            p.Add("@Type", zone_type);
            p.Add("@lang_code", lang_code);
            p.Add("@filter", filter);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ArticleMinify>(commandText, p);
            totalRow = p.Get<int>("@totalRow");
            return result;
        }

        public List<ArticleMinify> GetArticlesInZoneId_Top5(int zoneId, int zone_type, int article_type, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticlesInZoneId_Top5";
            p.Add("@zoneId", zoneId);
            p.Add("@typeZone", zone_type);
            p.Add("@typeArticle", article_type);
            p.Add("@lang_code", lang_code);
            p.Add("@filter", filter);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ArticleMinify>(commandText, p);
            totalRow = p.Get<int>("@totalRow");
            return result;
        }
        public List<ArticleMinify> GetArticlesRelated(string lang_code, string lstId)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticleRelated";
            p.Add("@lang_code", lang_code);
            p.Add("@id", lstId);
            var result = QuerySP<ArticleMinify>(commandText, p);

            return result;
        }
        public List<RedrectDetail> GetCategoryByIdObj(List<int> lstId, int type = 1, string languageCode = "vi-VN")
        {
            var p = new DynamicParameters();
            var commandText = "SP_GetZoneById";
            p.Add("@Id", String.Join(",", lstId));
            p.Add("@Type", type);
            p.Add("@LanguageCode", languageCode);
            var result = QuerySP<RedrectDetail>(commandText, p);

            return result;
        }

        public List<ArticleMinify> GetArticlesInZoneId_Minify_AddFilterHot(int zoneId, int zone_type, int? isHot, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticlesInZoneId_Minify_AddFilterHot";
            p.Add("@zoneId", zoneId);
            p.Add("@Type", zone_type);
            p.Add("@isHot", isHot);
            p.Add("@lang_code", lang_code);
            p.Add("@filter", filter);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ArticleMinify>(commandText, p);
            totalRow = p.Get<int>("@totalRow");
            return result;
        }
        public List<ArticleMinify> GetArticlesInZoneId_Minify_FullFilter(int zoneId, int zone_type, int article_type, int? isHot, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticlesInZoneId_Minify_Full";
            p.Add("@zoneId", zoneId);
            p.Add("@Type", zone_type);
            p.Add("@Article_type", article_type);
            p.Add("@isHot", isHot);
            p.Add("@lang_code", lang_code);
            p.Add("@filter", filter);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@totalRow", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ArticleMinify>(commandText, p);
            totalRow = p.Get<int>("@totalRow");
            return result;
        }

        public List<ArticleMinify> GetArticlesSameTag(string tag, string lang_code, int? pageIndex, int? pageSize, out int total)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticleSameTag";
            p.Add("@tag", tag);
            p.Add("@lang_code", lang_code);
            p.Add("@pageIndex", pageIndex);
            p.Add("@pageSize", pageSize);
            p.Add("@total", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
            var result = QuerySP<ArticleMinify>(commandText, p);
            total = p.Get<int>("@total");
            return result;
        }



        public ArticleDetail GetObjByAlias(string url, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetArticleByAlias";
            p.Add("@url", url);
            p.Add("@lang_code", lang_code);
            var result = QuerySP<ArticleDetail>(commandText, p);
            if (result != null && result.Any())
                return result.FirstOrDefault();
            else
                return new ArticleDetail();
        }

        public List<RelatedArticle> GetRelatedArticle(int id, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "SP_GetRelatedArticle_ById";
            p.Add("@ArticleId", id);
            p.Add("@LangCode", lang_code);
            var result = QuerySP<RelatedArticle>(commandText, p);
            if (result != null && result.Any())
                return result;
            else
                return new List<RelatedArticle>();
        }
        public RelatedArticle GetRedirectByArticleId(int id, string lang_code)
        {
            var p = new DynamicParameters();
            var commandText = "SP_GetRedirectByArticleId";
            p.Add("@ArticleId", id);
            p.Add("@LangCode", lang_code);
            var result = QuerySP<RelatedArticle>(commandText, p);
            if (result != null && result.Any())
                return result.FirstOrDefault();
            else
                return new RelatedArticle();
        }
    }
}
