﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class Redirect
    {
        public string UrlOld { get; set; }
        public string UrlNew { get; set; }
        public int UrlType { get; set; }
        public bool Status { get; set; }
    }
}
