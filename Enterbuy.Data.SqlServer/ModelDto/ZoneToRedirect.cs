﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ZoneToRedirect
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public int isHaveChild { get; set; }
        public string MetaNoIndex { get; set; }
        public string MetaCanonical { get; set; }
        public string MetaWebPage { get; set; }
    }
    public class ZoneId
    {
        public int Id { get; set; }
    }
}
