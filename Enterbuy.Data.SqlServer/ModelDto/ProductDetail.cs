﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ProductDetail
    {
        public int Id { get; set; }
        public string AvatarArray { get; set; }
        public string Warranty { get; set; }
        public int ManufacturerId { get; set; }
        public string Code { get; set; }
        public string Unit { get; set; }
        public int Quantity { get; set; }
        public string PropertyId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
        public string PromotionInfo { get; set; }
        public string Catalog { get; set; }
        public string MetaTitle { get; set; }
        public string MetaKeyword { get; set; }
        public string MetaDescription { get; set; }

        public string MetaNoIndex { get; set; }
        public string MetaCanonical { get; set; }
        public string SocialTitle { get; set; }
        public string SocialDescription { get; set; }
        public string SocialImage { get; set; }
        public string LanguageCode { get; set; }
        public double RateAvg { get; set; }
        public int TotalRate { get; set; }
        public int ZoneId { get; set; }
        public string ZoneUrl { get; set; }
        public string Avatar { get; set; }
        public int Five_Star { get; set; }
        public int Four_Star { get; set; }
        public int Three_Star { get; set; }
        public int Two_Star { get; set; }
        public int One_Star { get; set; }
        public int FlashSaleId { get; set; }
        public DateTime FlashSaleStartTime { get; set; }
        public DateTime FlashSaleEndTime { get; set; }
        public DateTime? ExprirePromotion { get; set; }
        public decimal ProductPriceInFlashSale { get; set; }
        public int ProductQuantityInFlashSale { get; set; }
        public string MetaFile { get; set; }
        public int MaterialType { get; set; }
        public int ViewCount { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountPrice { get; set; }
        public string MetaWebPage { get; set; }
        public decimal RateAVG { get; set; }
        public bool isInstallment { get; set; }
        public bool Vat { get; set; }
        public string ArticleId { get; set; }

        public ProductDetail()
        {
            isInstallment = false;
            Vat = false;
        }
    }
}

