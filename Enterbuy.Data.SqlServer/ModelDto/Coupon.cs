﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class Coupon
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int ValueDiscount { get; set; }
        public int Id { get; set; }
        public byte DiscountOption { get; set; }
        public int ProductId { get; set; }
        public Coupon()
        {
            this.Name = string.Empty;
            this.Code = string.Empty;
            this.ValueDiscount = 0;
            this.Id = 0;
            this.DiscountOption = 0;
            this.ProductId = 0;
        }
    }
}
