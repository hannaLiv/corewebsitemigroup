﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ProductMinify
    {
        public int ZoneId { get; set; }
        public int ProductId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Avatar { get; set; }
        public string Url { get; set; }
        public string PropertyId { get; set; }
        public decimal Price { get; set; }
        public decimal SalePrice { get; set; }
        public int IsHot { get; set; }
        public string BigThumb { get; set; }
        public decimal Rate { get; set; }
        public string SpecName { get; set; }
        public string SpecValue { get; set; }
        public int CountRate { get; set; }
        public string Color { get; set; }
        public string PromotionIds { get; set; }
        public int FlashSaleId { get; set; }
        public DateTime FlashSaleStartTime { get; set; }
        public DateTime FlashSaleEndTime { get; set; }
        public decimal ProductPriceInFlashSale { get; set; }
        public int ProductQuantityInFlashSale { get; set; }
        public string ZoneName { get; set; }
        public string MetaDescription { get; set; }
        public string ZoneUrl { get; set; }
        public string Voucher { get; set; } = "";
        public bool IsVoucher { get; set; } = false;
        public bool Vat { get; set; }
        public int DiscountPercent { get; set; }
        public DateTime? ExprirePromotion { get; set; } = new DateTime(2000, 01, 01);
        public ProductMinify()
        {
            this.Vat = false;
        }
    }

    public class ProductVoucher
    {
        public int ProductId { get; set; }
        public string Voucher { get; set; }

    }

}
