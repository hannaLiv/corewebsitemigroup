﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class OrderDetailViewModel
    {
        public int Id { get; set; } //Order Id
        public string OrderCode { get; set; } //Order code
        public DateTime CrearDate { get; set; } //Ngày tạo
        public string Name { get; set; } //Tên sản phẩm
        public double Quantity { get; set; } //Số lượng
        public double LogPrice { get; set; } //Giá sản phẩm
        public string Voucher { get; set; } //Voucher sử dụng
        public string Code { get; set; } //Mã sản phẩm
        public string CusName { get; set; } //Mã sản phẩm
        public OrderDetailViewModel()
        {
            this.Id = 0;
            this.OrderCode = string.Empty;
            this.CrearDate = DateTime.Now;
            this.Name = string.Empty;
            this.Quantity = 0;
            this.LogPrice = 0;
            this.Voucher = string.Empty;
            this.Code = string.Empty;
            this.CusName = string.Empty;
        }
    }
}
