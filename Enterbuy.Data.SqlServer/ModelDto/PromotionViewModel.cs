﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class PromotionViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descripton { get; set; }
        public int IsDiscountPrice { get; set; }
        public string Type { get; set; }
        public decimal Value { get; set; }
    }
    public class ProductField
    {
        public int ProductId { get; set; }

    }
}
