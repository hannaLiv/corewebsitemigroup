﻿using Enterbuy.Data.SqlServer.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class InstallmentViewModel
    {        
        public ProductDetail v_ProductDetail { get; set; }
        public ProductPriceInLocationDetail v_ProductPriceInLocationDetail { get; set; }
        public List<BankInstallment> v_BankInstallment { get; set; }
        public List<PromotionInProduct> v_ProductInPromotion { get; set; }
    }
}
