﻿using Enterbuy.Data.Sql.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ContactViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Note { get; set; }
        public byte? Type { get; set; }
        public string Source { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public byte? Status { get; set; }
        public string[] AgentName { get; set; }
        public string UrlRef { get; set; }
        public ContactViewModel()
        {
            this.Id = 0;
            this.Name = string.Empty;
            this.Address = string.Empty;
            this.Phone = string.Empty;
            this.Gender = string.Empty;
            this.Email = string.Empty;
            this.Title = string.Empty;
            this.Content = string.Empty;
            this.Note = string.Empty;
            this.Type = 0;
            this.Source = string.Empty;
            this.CreatedDate = DateTime.Now;
            this.ModifiedBy = string.Empty;
            this.ModifiedDate = DateTime.Now;
            this.Status = 1;
            this.UrlRef = string.Empty;
        }
    }
}
