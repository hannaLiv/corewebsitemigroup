﻿namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ProductPriceInLocationDetail
    {
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public decimal SalePrice { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }
}