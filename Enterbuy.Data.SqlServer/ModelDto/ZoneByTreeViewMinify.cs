﻿using System;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class ZoneByTreeViewMinify
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ParentId { get; set; }
        public int SortOrder { get; set; }
        public string LanguageCode { get; set; }
        public string Icon { get; set; }
        public int Root { get; set; }
        public string Url { get; set; }
        public int Level { get; set; }
        public string Order { get; set; }
        public int IsShowMenu { get; set; }
        public string Banner { get; set; }
        public int Type { get; set; }
        public string Avatar { get; set; }
        public int isHaveChild { get; set; }
        public string BreadcrumbFirst { get; set; }
        public string BreadcrumUrl { get; set; }
        public int IsShowHome { get; set; }

    }
}