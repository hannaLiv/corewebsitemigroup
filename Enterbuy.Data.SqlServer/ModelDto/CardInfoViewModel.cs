﻿using Enterbuy.Data.SqlServer.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Data.SqlServer.ModelDto
{
    public class CardInfoViewModel
    {
        public List<InfoCard> InfoCard { get; set; }
    }
}
