﻿using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Statics.Configurations;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Enterbuy.Data.SqlServer.DbContexts
{
    public class EnterbuyDbContext : DbContext
    {
        public EnterbuyDbContext()
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlServer(AppSettings.Get<string>("Databases:SqlServer:ConnectionStrings:Default"));

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.Relational().TableName = entity.Relational().TableName.ToLower();

                // Replace column names
                foreach (var property in entity.GetProperties())
                {
                    property.Relational().ColumnName = property.Name.ToLower();
                }

                foreach (var key in entity.GetKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToLower();
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.Relational().Name = key.Relational().Name.ToLower();
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.Relational().Name = index.Relational().Name.ToLower();
                }
            }

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Product> Product { get; set; }
        //public virtual DbSet<ActionInFunctions> ActionInFunctions { get; set; }
        //public virtual DbSet<Actions> Actions { get; set; }
        //public virtual DbSet<Ads> Ads { get; set; }
        //public virtual DbSet<AggregatedCounter> AggregatedCounter { get; set; }
        //public virtual DbSet<Article> Article { get; set; }
        //public virtual DbSet<ArticleInLanguage> ArticleInLanguage { get; set; }
        //public virtual DbSet<ArticlesInZone> ArticlesInZone { get; set; }
        //public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        //public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        //public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        //public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        //public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        //public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        //public virtual DbSet<BannerAds> BannerAds { get; set; }
        //public virtual DbSet<Colors> Colors { get; set; }
        //public virtual DbSet<Comment> Comment { get; set; }
        //public virtual DbSet<Config> Config { get; set; }
        //public virtual DbSet<ConfigInLanguage> ConfigInLanguage { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        //public virtual DbSet<Customer> Customer { get; set; }
        //public virtual DbSet<Department> Department { get; set; }
        //public virtual DbSet<DepartmentInLanguage> DepartmentInLanguage { get; set; }
        //public virtual DbSet<FileUpload> FileUpload { get; set; }
        //public virtual DbSet<FlashSale> FlashSale { get; set; }
        //public virtual DbSet<Functions> Functions { get; set; }
        //public virtual DbSet<Hash> Hash { get; set; }
        //public virtual DbSet<Job> Job { get; set; }
        //public virtual DbSet<JobParameter> JobParameter { get; set; }
        //public virtual DbSet<JobQueue> JobQueue { get; set; }
        //public virtual DbSet<Language> Language { get; set; }
        //public virtual DbSet<List> List { get; set; }
        //public virtual DbSet<Location> Location { get; set; }
        //public virtual DbSet<LocationInLanguage> LocationInLanguage { get; set; }
        //public virtual DbSet<MaintainSpectificatinInProduct> MaintainSpectificatinInProduct { get; set; }
        //public virtual DbSet<MaintainSpectificationInLanguage> MaintainSpectificationInLanguage { get; set; }
        //public virtual DbSet<MaintainSpectifications> MaintainSpectifications { get; set; }
        //public virtual DbSet<MaintainSpectificationTemplate> MaintainSpectificationTemplate { get; set; }
        //public virtual DbSet<Manufacturer> Manufacturer { get; set; }
        //public virtual DbSet<ManufacturerInLanguage> ManufacturerInLanguage { get; set; }
        //public virtual DbSet<Menu> Menu { get; set; }
        //public virtual DbSet<OrderDetail> OrderDetail { get; set; }
        //public virtual DbSet<OrderPromotionDetail> OrderPromotionDetail { get; set; }
        //public virtual DbSet<Orders> Orders { get; set; }
        //public virtual DbSet<Permission> Permission { get; set; }
        //public virtual DbSet<ProductInArticle> ProductInArticle { get; set; }
        //public virtual DbSet<ProductInFlashSale> ProductInFlashSale { get; set; }
        //public virtual DbSet<ProductInLanguage> ProductInLanguage { get; set; }
        //public virtual DbSet<ProductInProduct> ProductInProduct { get; set; }
        //public virtual DbSet<ProductInPromotion> ProductInPromotion { get; set; }
        //public virtual DbSet<ProductInRegion> ProductInRegion { get; set; }
        //public virtual DbSet<ProductInZone> ProductInZone { get; set; }
        //public virtual DbSet<ProductPriceInLocation> ProductPriceInLocation { get; set; }
        //public virtual DbSet<ProductSpecifications> ProductSpecifications { get; set; }
        //public virtual DbSet<ProductSpecificationTemplate> ProductSpecificationTemplate { get; set; }
        //public virtual DbSet<ProductSpecificationTemplateInLanguage> ProductSpecificationTemplateInLanguage { get; set; }
        //public virtual DbSet<Promotion> Promotion { get; set; }
        //public virtual DbSet<PromotionInLanguage> PromotionInLanguage { get; set; }
        //public virtual DbSet<Property> Property { get; set; }
        //public virtual DbSet<PropertyLanguage> PropertyLanguage { get; set; }
        //public virtual DbSet<Rating> Rating { get; set; }
        //public virtual DbSet<Schema> Schema { get; set; }
        //public virtual DbSet<Server> Server { get; set; }
        //public virtual DbSet<Set> Set { get; set; }
        //public virtual DbSet<State> State { get; set; }
        //public virtual DbSet<Tag> Tag { get; set; }
        //public virtual DbSet<TagInProductLanguage> TagInProductLanguage { get; set; }
        //public virtual DbSet<User> User { get; set; }
        //public virtual DbSet<UserPermission> UserPermission { get; set; }
        //public virtual DbSet<Zone> Zone { get; set; }
        //public virtual DbSet<ZoneInLanguage> ZoneInLanguage { get; set; }
    }
}