﻿R.Extra = {
    Init: function () {
        R.Extra.RegisterEvent();
        R.Extra.culture = R.Culture();
        R.Extra.GetPropertyDetails();
        R.Extra.BindingExtraToProduct();
        R.Extra.GenerateAvatarCircle();
        R.Extra.CommentPaging();
        R.Extra.Url = "http://cmsenterbuy.migroup.asia/";
    },
    RegisterEvent: function () {
        $('#comment-form').on('submit', function () {
            var type = $(this).data('type');
            var object_id = $(this).data('object-id');
            var object_type = $(this).data('object-type');
            var name = $(this).find('.txt_name').val();
            var phoneOrEmail = $(this).find('.txt_phoneOrEmail').val();
            var avatar = $(this).find('.input-choose-file').data('target-url');
            var content = $(this).find('.txt_content').val();
            var rate = 0;
            //uploadfile
            var fileUpload = $(this).find(".input-choose-file").get(0);
            var files = fileUpload.files;
            var params = {
                objectId: object_id,
                objectType: object_type,
                name: name,
                phoneOrEmail: phoneOrEmail,
                avatar: avatar,
                content: content,
                type: type,
                rate: rate
            }
            R.Extra.CreateComment(files, params);
            return false;
        });
        $(".star-rating i").off('click').on('click', function () {
            $(this).parent().find(".star-rating i").removeClass("checked");
            $(this).addClass("checked");
            $(this).prevAll().addClass("checked");
            $(this).nextAll().removeClass("checked");
            var type = $(this).closest('.star-rating').data('type');
            var object_id = $(this).closest('.star-rating').data('id');
            var el = $(this).parent();
            //var el = $(this).parent();
            //Dem so phan tu co class active
            R.Extra.Rating(el, object_id, type);
        });

        $('#rating-form').off('submit').on('submit', function () {
            var type = $(this).data('type');
            var object_id = $(this).data('object-id');
            var object_type = $(this).data('object-type');
            var name = $(this).find('.txt_name').val();
            var phoneOrEmail = $(this).find('.txt_phoneOrEmail').val();
            var avatar = $(this).find('.input-choose-file').data('target-url');
            var content = $(this).find('.txt_content').val();
            var rate = 0;
            //Getrate
            $(this).find('.star-rating').find('.checked').each(function (element) {
                rate++;
            })
            //alert(rate);
            //uploadfile
            var fileUpload = $(this).find(".input-choose-file").get(0);
            var files = fileUpload.files;
            var params = {
                objectId: object_id,
                objectType: object_type,
                name: name,
                phoneOrEmail: phoneOrEmail,
                avatar: avatar,
                content: content,
                type: type,
                rate: rate
            }
            R.Extra.CreateComment(files, params);
            return false;
        });

        $('.reply_comment').off('click').on('click', function () {
            var el = $(this);
            var id = $(this).closest('.comment_infomation').data('id');
            var obj_id = $(this).closest('.comment_infomation').data('object-id');
            var obj_type = $(this).closest('.comment_infomation').data('object-type');
            var fullName = $(this).closest('.comment_infomation').data('fullname');
            var email = $(this).closest('.comment_infomation').data('email');
            $('._comment_reply').each(function (element) {
                $(this).html('');
            })
            R.Extra.RenderReplyComment(el, id, obj_id, obj_type, fullName, email);

        });
        $('.send-comment-reply').off('click').on('click', function () {
            var parent_comment = $(this).data('parent')
            var obj_id = $(this).data('object-id');
            var obj_type = $(this).data('object-type');
            var type = $(this).data('type');
            var name = $(this).closest('.reply').find('.txt_name').val();
            var phoneOrEmail = $(this).closest('.reply').find('.txt_phoneOrEmail').val();
            var avatar = $(this).closest('.reply').find('.input-choose-file').data('target-url');
            var content = $(this).closest('.reply').find('.txt_content').val();
            var rate = 0;
            //uploadfile
            var fileUpload = $(this).parent().parent().find(".input-choose-file").get(0);
            var files = fileUpload.files;
            var params = {
                objectId: obj_id,
                objectType: obj_type,
                name: name,
                phoneOrEmail: phoneOrEmail,
                avatar: avatar,
                content: content,
                type: type,
                rate: rate,
                parentId: parent_comment
            }
            R.Extra.CreateComment(files, params);
        });
        $('.input-choose-file').off('change').on('change', function () {
            var parent_el = $(this).closest('.upload-parent');
            readURL(this, parent_el);
        });
        $('._place-img-close').off('click').on('click', function () {
            $(this).parent().parent().find('._place-img').attr("src", "");
            $(this).parent().parent().hide();
            $('.input-choose-file').val('');
        })
    },
    GenerateAvatarCircle: function () {
        $('.profileImage').each(function (element) {
            var name = $(this).data('name');
            if (name != null && name.indexOf(" ") > -1) {
                var cutted_name = name.split(" ");
                var first = "";
                var second = "";
                first = cutted_name[0].charAt(0);
                if (cutted_name.length >= 2) {
                    second = cutted_name[1].charAt(0);
                }
                var avt = $(this).text(first.toUpperCase() + second.toUpperCase());
            }
            else {
                var first = name.charAt(0);
                var avt = $(this).text(first);
            }


        })
    },
    GetPropertyDetails: function () {
        //Kiem tra session co chua
        var url = "/Product/GetPropertyDetails";
        $.post(url, null, function (response) {
            sessionStorage.setItem("properties", response);
        });

    },
    BindingExtraToProduct: function () {

        $('.item-product').each(function (element) {
            var properties = $(this).data('properties');
            if (properties !== undefined && properties != "") {
                var p = properties.toString();
                var el = $(this);
                if (p != "") {
                    var list_property = p.split(',');
                    BindExtraTop($(this), list_property);

                }
            }
            else {
                $(this).find('.tag-1').hide();
            }
        })

        $('.product-detail').each(function (element) {
            var properties = $(this).data('properties');
            if (properties !== undefined && properties != "") {
                var p = properties.toString();
                var el = $(this);
                if (p != "") {
                    var list_property = p.split(',');
                    BindExtraTop($(this), list_property);

                }
            }
            else {
                $(this).find('.tag').hide();
            }
        })
    },
    BindingExtraToProductInElement: function (el) {
        $(el).find('.item-product').each(function (element) {
            var properties = $(this).data('properties').toString();
            if (properties !== undefined) {
                var list_property = [];
                if (!properties || typeof properties === "string" && properties.includes(',') == true)
                    list_property = properties.split(',');
                if (!properties || typeof properties === "string" && properties.includes(',') == false && properties != null)
                    list_property.push(properties)
                BindExtraTop($(this), list_property);
            }


        })
    },
    CreateComment: function (files, params) {
        var send_email = R.Order.culture + '/Extra/SendMail';
        var params_email = {
            type: "comment",
            body: params.content
        }
        if (files.length > 0) {
            var dataFile = new FormData();
            for (var i = 0; i < files.length; i++) {
                dataFile.append(files[i].name, files[i]);
            }
            $.ajax({
                url: R.Extra.Url + 'api/UploadFile/UploadImage',
                type: "POST",
                data: dataFile,
                contentType: false,
                processData: false,
                success: function (result) {
                    console.log(result);
                    params.avatar = result.linkImage;
                    var url = "/Common/CreateComment";
                    $.post(url, params, function (response) {
                        $('#modal-xn').modal('show');
                        $.post(send_email, params_email, function (response) {
                            R.Extra.CloseModalAndClearText();
                            return false;
                        })
                    })
                },
                error: function (err) {
                    alert(err.statusText)
                }
            });
        } else {
            var url = "/Common/CreateComment";
            $.post(url, params, function (response) {
                $('#modal-xn').modal('show');
                $.post(send_email, params_email, function (response) {
                    R.Extra.CloseModalAndClearText();
                    return false;
                })
            })
        }
    },
    ModalServiceSlideBanner: function (articleId) {
        var url = "/Common/ModalServiceSlideBanner?articleId=" + articleId;
        $.get(url, function (response) {
            $('#modalDefault').modal('show');
            $('#modalDefaultContent').html(response);

        })
    },
    CloseModalAndClearText: function () {
        $('.txt_name').val('');
        $('.txt_phoneOrEmail').val('');
        $('.txt_content').val('');
        $('#modal-danh-gia').modal('hide');
    },
    Rating: function (el, id, type) {
        //console.log(el);
        //Dem so sao
        var count = 0;
        el.find('.checked').each(function (element) {
            count++;
        });
        //console.log("dem sao " + count);
        var url = "/Common/CreateRating";
        //int objectId, int objectType, int rate
        var params = {
            objectId: id,
            objectType: type,
            rate: count
        }
        $.post(url, params, function (response) {
            el.find('i').off('click');
        })
    },
    RenderReplyComment: function (el, id, obj_id, obj_type, fullName, email) {
        var url = "/Common/GetReplyComment";
        var params = {
            id: id,
            obj_id: obj_id,
            obj_type: obj_type,
            fullName: fullName,
            email: email
        }
        $.post(url, params, function (response) {
            el.closest('.media-body').find('._comment_reply').first().html('').html(response);
            R.Extra.RegisterEvent();
        })
    },
    CommentPaging: function () {
        if ($('.comment-paging').length > 0) {
            var total = parseInt($('.comment-paging').data('total'));
            var size = parseInt($('.comment-paging').data('size'));
            var obj_id = parseInt($('.comment-paging').data('object-id'));
            var obj_type = parseInt($('.comment-paging').data('object-type'));
            var arr = [];
            for (var i = 0; i < total; i++) {
                arr.push(i);
            }
            $('.comment-paging').pagination({
                dataSource: arr,
                pageSize: size,
                showPrevious: false,
                showNext: false,
                callback: function (data, pagination) {
                    var page = pagination.pageNumber;
                    var url = "/Common/GetCommentList";
                    var params = {
                        object_id: obj_id,
                        object_type: obj_type,
                        pageIndex: page
                    }
                    $.post(url, params, function (response) {
                        $('.comment_binding').html('').html(response);
                        R.Extra.GenerateAvatarCircle();
                        R.Extra.RegisterEvent();
                    })

                }
            })
        }
    }

}



$(function () {
    R.Extra.Init();
});

let bo = false;

function BindExtraTop(element, list_property) {

    var propertySession = getWithExpiry("properties");
    if (propertySession == null) {
        var url = "/Product/GetPropertyDetails";
        $.post(url, null, function (response) {
            setWithExpiry("properties", response, 5000000);
            var property_list = JSON.parse(response);
            //console.log(property_list, element, list_property);
            var isDoneTop = false;
            var isDoneBottom = false;
            if (property_list.length != null) {
                property_list.forEach(function (i1) {
                    if (list_property != null) {
                        list_property.find(function (e) {
                            if (parseInt(e) == i1.Id && i1.Position == 'top') {
                                $(element).find('.tag-1').html(i1.Name).show();
                                //$(element).find('.tag').html(i1.Name);
                                isDoneTop = true;
                            }
                            if (parseInt(e) == i1.Id && i1.Position == 'bottom') {
                                //console.log($(element))
                                var src = R.StoreFilePath(true) + i1.Thumb;
                                $(element).find('.tag-2').show();
                                $(element).find('.tag-2 img').attr("src", src);
                                isDoneBottom = true;
                            }
                        })
                    }
                })
            }
            //if (isDoneTop == false)
            //    $(element).find('.tag-1').hide();
            //if (isDoneBottom == false)
            //    $(element).find('.tag-2').hide();
        });
    } else {
        var property_list = JSON.parse(propertySession);
        var isDoneTop = false;
        var isDoneBottom = false;
        if (property_list.length != null) {
            property_list.forEach(function (i1) {
                if (list_property != null) {
                    list_property.find(function (e) {
                        if (parseInt(e) == i1.Id && i1.Position == 'top') {
                            $(element).find('.tag-1').html(i1.Name).show();
                            //$(element).find('.tag').html(i1.Name);
                            isDoneTop = true;
                        }
                        if (parseInt(e) == i1.Id && i1.Position == 'bottom') {
                            //console.log($(element))
                            var src = R.StoreFilePath(true) + i1.Thumb;
                            $(element).find('.tag-2').show();
                            $(element).find('.tag-2 img').attr("src", src);
                            isDoneBottom = true;
                        }
                    })
                }
            });
        }
        //if (isDoneTop == false) {
        //    $(element).find('.tag-1').hide();

        //    //$(element).find('.tag').hide();
        //}
        //if (isDoneBottom == false) {
        //    $(element).find('.tag-2').hide();

        //    //$(element).find('.tag').hide();
        //}

    }
};
function readURL(input, el) {
   
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            el.find('._binding_pickup_img').show();
            el.find('._place-img').attr('src', e.target.result).css("max-width", "300").css("max-height", "200");
            R.Extra.RegisterEvent();
        }
        reader.readAsDataURL(input.files[0]);
    }
};