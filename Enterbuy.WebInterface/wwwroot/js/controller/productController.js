﻿R.Product = {
    Init: function () {
        //R.Test();
        R.Product.BindingTotal();
        R.Product.RegisterEvent();
        R.Product.culture = R.Culture();
        R.Product.location_id = R.CurrentLocationId();
        R.Extra.BindingExtraToProduct();
    },
    RegisterEvent: function () {
        $('.select-zone').off('click').on('click', function () {
            $(this).closest('.swiper-wrapper').find('.swiper-slide-active').removeClass('swiper-slide-active');
            $(this).closest('.swiper-slide').addClass('swiper-slide-active');
            var el = $(this);
            var zone_id = $(this).data('id');
            //load ajax
            R.Product.BindingProduct(zone_id, el);
        });


        $(".nhanhieu").off("click").on("click", function () {

            var $container = $("html,body");
            var $scrollTo = $('.client-cate');
            if (window.innerWidth > 800) {
                $container.animate({ scrollTop: $scrollTo.scrollTop() + 80 }, 500);
            }
            else {
                $container.animate({ scrollTop: $scrollTo.offset().top }, 500);
            }

            if (!$(this).hasClass("active")) {
                $(".nhanhieu").removeClass("active");
            }
            $(this).toggleClass("active");

            R.Product.Filter(1);
        });
        $('.select-zone-filterd').off('click').on('click', function () {
            $(this).closest('.swiper-wrapper').find('.swiper-slide-active').removeClass('swiper-slide-active');
            $(this).closest('.swiper-slide').addClass('swiper-slide-active');
            var el = $(this);
            var parent_id = $(this).data('id');
            //var parent_id = $('#zone-current').data('id');
            var manu_id = $('.picking-active').data('manu-id');
            var range_price = $('.range-price').val();
            var min_price = 0;
            var max_price = 0
            if (range_price != "") {
                var arr = range_price.split('-');
                min_price = arr[0];
                max_price = arr[1];
            }
            var fp = [];
            $('.dynamic-filter').each(function (element) {
                var filter_item = {
                    SpectificationId: $(this).data('spec-id'),
                    Value: $(this).val()
                }
                fp.push(filter_item);
            })
            var color_code = $('.color').val();
            var extra = $('.extra-sort').val();
            var sort_price = 0;
            var sort_rate = 0;
            if (extra == "2")
                sort_price = 1;
            if (extra == "3")
                sort_rate = 1;
            var locationId = R.Product.location_id;
            var params = {
                parentId: parent_id,
                lang_code: 'vi-VN',
                locationId: locationId,
                manufacture_id: manu_id,
                min_price: parseInt(min_price),
                max_price: parseInt(max_price),
                sort_price: sort_price,
                sort_rate: sort_rate,
                color_code: color_code,
                filter: fp,
                filter_text: '',
                material_type: 0,
                pageNumber: 1,
                pageSize: 20
            }
            //load ajax
            R.Product.BindingProductFilterd(params, el);
        });
        $(".tags .badge .close").off('click').on('click', function () {
            var el = $(this);
            if (el.hasClass("v-nhanhieu")) {
                $(".nhanhieu").removeClass("active");
            } else {
                var spec = el.attr("v-spec-id");
                $("select[data-spec-id='" + spec + "']").val("");
            }

            el.closest(".badge").remove();
            R.Product.Filter(1);

        });
        $('.view-more').off('click').on('click', function () {

            var el = $(this);
            var id = $(this).data('id');
            var skip = $(this).closest('.container').find('.set-total').data('is-rendered');
            var size = $(this).data('size');
            R.Product.ViewMore(id, el, skip, size);
        });
        $('.view-more-1').off('click').on('click', function () {
            var page = parseInt($(this).data("page"));
            $(this).data("page", page + 1);
            R.Product.Filter(page);
        });
        $('.button-view-more').off('click').on('click', function () {
            console.log(1);
            var el = $(this);
            var id = $(this).data('id');
            var skip = $(this).closest('.container').find('.set-total').data('is-rendered');
            var size = $(this).data('size');
            R.Product.ViewMore(id, el, skip, size);
        });
        $(".star-rating i").off('click').on('click', function () {
            $(this).parent().find(".star-rating i").removeClass("checked");
            $(this).addClass("checked");
            $(this).prevAll().addClass("checked");
            $(this).nextAll().removeClass("checked");
            var type = 2;
            var zone_id = $(this).closest('.star-rating').data('id');
            //Dem so phan tu co class active
            R.Product.Rating(zone_id, type);
        });
        $('.filter-picking').off('click').on('click', function () {
            var el = $(this);
            el.closest('.container').find('.filter-picking').each(function (el) {
                $(this).removeClass('picking-active');
            })
            el.addClass('picking-active');
            R.Product.Filter(1);
        });
        $('.filter-select').off('change').on('change', function () {

            R.Product.Filter(1);
        })


    },
    BindingTotal: function () {
        $('.set-total').each(function (el) {
            var total = $(this).data('total');
            var id = $(this).data('id');
            var isRendered = $(this).data('is-rendered');
            console.log(total, isRendered);
            if (total <= isRendered) {
                //button-view-more
                $(this).closest('.container').find('.tong_so_sp').text(0);
                $(this).closest('.container').find('.tong_so_sp').parent().show();
                $(this).closest('.container').find('.button-view-more').parent().show();
            }
            else {
                $(this).closest('.container').find('.tong_so_sp').text(total - isRendered);
                $(this).closest('.container').find('.tong_so_sp').parent().show();
                $(this).closest('.container').find('.tong_so_sp').parent().data('size', 18);
                $(this).closest('.container').find('.tong_so_sp').parent().data('id', id);
                $(this).closest('.container').find('.button-view-more').parent().show();
                $(this).closest('.container').find('.button-view-more').data('size', 18);
                $(this).closest('.container').find('.button-view-more').data('id', id);
            }
        });
    },
    BindingProduct: function (id, el) {
        console.log(id);
        var params = {
            zone_id: id,
            location_id: 3
        }
        $.post(R.Product.culture + '/Product/GetProductByZoneId', params, function (response) {
            console.log(response);
            el.closest('.container').find('._binding_product').html(response);
            var new_element = el.closest('.container').find('._binding_product');
            R.Extra.BindingExtraToProductInElement(new_element);
            R.Product.BindingTotal();
            R.Product.RegisterEvent();
        })
    },
    BindingProductFilterd: function (params, el) {
        //FilterSpectificationInZoneListProductList
        $.post(R.Product.culture + '/Product/FilterSpectificationInZoneListProductList', params, function (response) {
            console.log(response);
            el.closest('.container').find('._binding_product').html(response);
            var new_element = el.closest('.container').find('._binding_product');
            R.Extra.BindingExtraToProductInElement(new_element);
            R.Product.BindingTotal();
            R.Product.RegisterEvent();
        });
    },
    ViewMore: function (id, el, skip, size) {
        //int zone_parent_id, int locationId, int skip, int size
        var params = {
            zone_parent_id: id,
            locationId: 3,
            skip: skip,
            size: size
        }
        $.post(R.Product.culture + '/Product/ViewMore', params, function (response) {
            el.closest('.container').find('._binding_product').append(response);
            var new_element = el.closest('.container').find('._binding_product');
            //Update Isrendered
            var isItemRenderd = skip + size;
            el.closest('.container').find('.set-total').data('is-rendered', isItemRenderd);
            R.Extra.BindingExtraToProductInElement(new_element);
            R.Product.BindingTotal();
            R.Product.RegisterEvent();
        })
    },
    Rating: function (id, type) {
        //Dem so sao
        var count = 0;
        $('.star-rating').find('.checked').each(function (element) {
            count++;
        });
        console.log(count);
        var url = R.Product.culture + "/Extra/CreateRating";
        //int objectId, int objectType, int rate
        var params = {
            objectId: id,
            objectType: type,
            rate: count
        }
        $.post(url, params, function (response) {
            console.log(response);
            $(".star-rating i").off('click');
        })
    },
    Filter: function (pageNum) {
        debugger
        //Get thuong hieu
        let tags = "";

        var parent_id = $('#zone-current').data('id');

        var range_price = $('.range-price').val();
        var min_price = 0;
        var max_price = 0
        if (range_price != undefined && range_price != null && range_price != "") {
            tags += "<span class=\"badge badge-info\"> " + $('.range-price option:selected').text() + " <button v-spec-id='0' class=\"close\"> <span aria-hidden=\"true\">&times;</span></button></span>";
            var arr = range_price.split('-');
            min_price = arr[0];
            max_price = arr[1];
        }
        var fp = [];
        
        $('.dynamic-filter').each(function (element) {
            debugger
            if ($(this).val().length > 0) {
                tags += "<span class=\"badge badge-info\"> " + $(this).val() + " <button v-spec-id='" + $(this).data('spec-id') + "' class=\"close\"> <span aria-hidden=\"true\">&times;</span></button></span>";
            }
            var filter_item = {
                SpectificationId: $(this).data('spec-id'),
                Value: $(this).val()
            }
            fp.push(filter_item);
        })

        var extra = $('.extra-sort').val();
        var sort_price = 0;
        var sort_rate = 0;
        if (extra == "2")
            sort_price = 1;
        if (extra == "3")
            sort_rate = 1;
        var locationId = R.Product.location_id;

        var nh = ($(".nhanhieu.active").data("id") != null && $(".nhanhieu.active").data("id") != undefined) ? true : false;
        if (nh) {
            tags += "<span class=\"badge badge-info\"> " + $(".nhanhieu.active").attr("title") + " <button class=\"close  v-nhanhieu\"> <span aria-hidden=\"true\">&times;</span></button></span>";
        }
        $(".tags").html(tags);
        var params = {
            parentId: parent_id,
            lang_code: 'vi-VN',
            locationId: locationId,
            manufacture_id: $(".nhanhieu.active").data("id") || 0,
            min_price: parseInt(min_price),
            max_price: parseInt(max_price),
            sort_price: sort_price,
            sort_rate: sort_rate,

            filter: fp,
            filter_text: '',
            material_type: 0,
            pageNumber: pageNum,
            pageSize: 36
        }
        var url = R.Product.culture + "/Product/FilterSpectificationInZone";
        $.post(url, params, function (response) {
            if (response === null || response.trim().length < 0) {
                response = "<p> Không có sản phẩm nào </p>";
            }
            $('.append-filter .search-category').show();
            if (pageNum > 1) {
                $('.append-filter .search-category .search-product-category').append(response);
            }
            else {
                $('.append-filter .search-category .search-product-category').html(response);
            }
            slidermenu();
            R.Extra.BindingExtraToProduct();
            R.Product.BindingTotal();
            R.Product.RegisterEvent();
        })
        //R.Estimates.RegisterEvent();

        //console.log(params);
    }


}
$(function () {
    R.Product.Init();
})