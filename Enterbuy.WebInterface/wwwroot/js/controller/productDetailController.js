﻿R.ProductDetail = {
    Init: function () {
        R.ProductDetail.RegisterEvent();
        R.ProductDetail.culture = R.Culture();
        R.ProductDetail.CountDownFlashSale('.timer');
        R.ProductDetail.AddLastSeenProduct();
    },
    RegisterEvent: function () {
        $('.location-item').off('click').on('click', function () {
            //Get text
            var text = $(this).text();
            $(this).closest(".select-local").find(".dropdown-toggle").text(text);
            //Get gia
            var normal_price = $(this).data('price');
            var sale_price = $(this).data('sale-price');
            console.log(normal_price, sale_price);
            $('#price-sale').text(R.FormatNumber(sale_price) + ' đ');
            $('#default_price').text(R.FormatNumber(sale_price) + ' đ');
            $('#default-sale-price').data('sale-price', sale_price);
            $('#price-normal').text(R.FormatNumber(normal_price) + 'đ');
            //Goi lay gia cua combo
            var product_id = $(this).data('product-id');
            var location_id = $(this).data('location-id');
            console.log(product_id, location_id);
            R.ProductDetail.GetComboInProduct(product_id, location_id);
        });
        $('.choose_combo').off('change').on('change', function () {
            // debugger
            R.ProductDetail.InfomationCombo();
        });
        $('#plus').off('click').on('click', function () {
            console.log('plus');
            var quantity = parseInt($('#quantity').val()) + 1;
            $('#quantity').val(quantity);
        });
        $('#minus').off('click').on('click', function () {
            console.log('minus');
            var quantity = parseInt($('#quantity').val()) - 1;
            if (quantity <= 1)
                quantity = 1;
            $('#quantity').val(quantity);
        });

        $(".btn-voucher-installment").off("click").on("click", function (e) {
            var voucher = $(this).closest(".input-group").find(".existCoupon").val();
            R.ProductDetail.ExsitVoucher(voucher);
        });
        $(".btn-voucher-installment-mobile").off("click").on("click", function (e) {
            var voucher = $(this).closest(".check-ms").find(".existCoupon").val();
            R.ProductDetail.ExsitVoucher(voucher);

        });
        $(".existCoupon").on("keydown", function (e) {
            if (e.keyCode == 13) {
                var voucher = $(this).val();
                R.ProductDetail.ExsitVoucher(voucher);
            }
        });

        $('#add-to-cart').off('click').on('click', function () {
            var ls = $(this).closest('.product-detail').find('#list_combo');
            var thumb = ls.find('.image-product');
            var cardElm = $('.dropdown-cart-trigger');
            if (thumb.length > 0)
                flyToElement(thumb[0], $(cardElm));
            R.ProductDetail.AddToCart();
        });

        $('#add-to-cart-mobile').off('click').on('click', function () {
            var ls = $(this).closest('.product-detail').find('#list_combo');
            var thumb = ls.find('.image-product');
            var cardElm = $('.dropdown-cart-trigger');
            if (thumb.length > 0)
                flyToElement(thumb[0], $(cardElm));
            R.ProductDetail.AddToCart();
        });

        //buy-now
        $('#buy-now').off('click').on('click', function () {
            R.ProductDetail.AddToCart();
        });
        $('#buy-now-mobi').off('click').on('click', function () {
            R.ProductDetail.AddToCart();
        });
        $('#add-cart-combo').off('click').on('click', function () {
            R.ProductDetail.AddToCartCombo();
        });

        $('#picking-estimates').off('click').on('click', function () {
            var id = $(this).data('id');
            var material = $(this).data('material');
            var quantity = parseInt($('#quantity').val());
            var price = $(this).data('price');
            var sale_price = $(this).data('sale-price');
            var avatar = $(this).data('avatar');
            var SpecName = $(this).data('spec-name');
            var SpecValue = $(this).data('spec-value');
            var title = $(this).data('title');
            var item_obj = {
                Id: id,
                Title: title,
                SalePrice: sale_price,
                Price: price,
                MaterialType: material,
                Avatar: avatar,
                SpecName: SpecName,
                SpecValue: SpecValue
            };
            R.ProductDetail.AddEstimatesLocal(item_obj);
        });
    },
    ExsitVoucher: function (voucher) {
        if (voucher !== undefined && voucher.length > 0) {
            var id = $(".product-detail").data("id");
            let url = "/Product/CheckVoucher?productId=" + id + "&voucher=" + voucher;
            $.get(url, function (data) {
                if (data.key == true) {
                    $("#mess-voucher").show();
                    $("#mess-voucher").addClass("alert-success");
                    $("#mess-voucher").removeClass("alert-danger");
                    $(".mess-vc").text(data.value.name);
                    $("#default-sale-price").attr("data-voucher", data.value.code);
                    $("#add-to-cart").attr("data-voucher", data.value.code);

                    $(".icon-check-right-failed").hide();
                    $(".icon-check-right-success").show();
                } else {
                    $("#mess-voucher").show();
                    $("#mess-voucher").addClass("alert-danger");
                    $("#mess-voucher").removeClass("alert-success");
                    $(".mess-vc").text(data.value.name);
                    $("#default-sale-price").attr("data-voucher", "");
                    $("#add-to-cart").attr("data-voucher", "");

                    $(".icon-check-right-success").hide();
                    $(".icon-check-right-failed").show();
                }
            });
        }

    },
    InfomationCombo: function () {
        var count = 0;
        var sale_price = 0;
        $('.choose_combo').each(function (element) {
            if (this.checked) {
                count++;
                sale_price += parseInt($(this).data('sale-price'));
            }
        });
        $('#count_combo').text(count);
        $('#total_combo_price').text(R.FormatNumber(sale_price) + "đ");
        R.ProductDetail.RegisterEvent();
    },
    GetComboInProduct: function (product_id, location_id) {
        var params = {
            product_id: product_id,
            location_id: location_id
        };
        $.post(R.ProductDetail.culture + '/Product/GetComboByLocationId', params, function (response) {
            $('#_binding_combo').html(response);
            R.ProductDetail.InfomationCombo();
            R.ProductDetail.RegisterEvent();
        });
    },
    AddToCart: function () {
        debugger
        var product_id = $('#add-to-cart').data('product-id');
        var voucher = $('#add-to-cart').attr('data-voucher');
        var quantity = $('#quantity').val();
        if (quantity != undefined) {
            var list_promotion = [];
            $('.choose_promotion').each(function (element) {
                var id = $(this).data('promotion-id');
                var checked = false;
                if (this.checked)
                    checked = true;
                var obj = {
                    id: id,
                    checked: checked
                }
                list_promotion.push(obj);
            })
            var cart_item = {
                product_id: product_id,
                voucher: voucher,
                quantity: parseInt(quantity),
                add_time: Date.parse(new Date()),
                promotion: list_promotion
            }
            //Add to localStorage
            var arrProduct = JSON.parse(localStorage.getItem("arrProduct"));
            arrProduct.push(cart_item);
            localStorage.setItem("arrProduct", JSON.stringify(arrProduct));
            R.LoadCart();
        } else {
            alert("Sản phẩm hiện đang hết hàng .");
        }
    },
    AddToCartCombo: function () {
        var arrProduct = JSON.parse(localStorage.getItem("arrProduct"));
        $('.choose_combo').each(function (element) {
            if (this.checked) {
                var product_id = $(this).data('product-id');
                var product_voucher = $(this).data('voucher') || "";
                var quantity = 1;
                var promotions = [];
                var cart_item = {
                    product_id: product_id,
                    quantity: quantity,
                    voucher: product_voucher,
                    add_time: Date.parse(new Date()),
                    promotion: promotions
                };
                arrProduct.push(cart_item);
            }
        });
        localStorage.setItem("arrProduct", JSON.stringify(arrProduct));
        R.LoadCart();
    },
    //CreateComment: function (files, params) {
    //    var url = R.ProductDetail.culture + "/Extra/CreateComment";
    //    $.post(url, params, function (response) {
    //        $('#modal-xn').modal('show');
    //        R.ProductDetail.CloseModalAndClearText();
    //        return false;
    //    })
    //},
    CloseModalAndClearText: function () {
        $('.txt_name').val('');
        $('.txt_phoneOrEmail').val('');
        $('.txt_content').val('');
        $('.modal-danh-gia').modal(hide);
    },
    AddLastSeenProduct: function () {
        var arr_empty = [];
        if (localStorage.getItem("lastSeen") == null)
            localStorage.setItem("lastSeen", JSON.stringify(arr_empty));
        var r = JSON.parse(localStorage.getItem("lastSeen"));
        //Get Id
        var seen_id = $('.product-detail').data('id');
        var flag = false;
        r.forEach(function (element) {
            if (parseInt(element) == parseInt(seen_id))
                flag = true;
        })

        if (flag == false)
            r.unshift(seen_id);
        localStorage.setItem("lastSeen", JSON.stringify(r));
    },
    AddEstimatesLocal: function (item_obj) {
        var r = localStorage.getItem("arrEstimates");

        if (r == null) {
            var arr = [];
            arr.push(item_obj);
            localStorage.setItem("arrEstimates", JSON.stringify(arr));
            var url = "/du-toan-cong-trinh";
            window.location.href = url;
        }
        if (r != null) {
            var arr = JSON.parse(r);
            arr.find(function (element) {
                if (element.MaterialType == item_obj.MaterialType) {
                    Object.assign(element, item_obj);
                } else
                    arr.push(item_obj);
            })
            localStorage.setItem("arrEstimates", JSON.stringify(arr));
            var url = "/du-toan-cong-trinh";
            window.location.href = url;

        }

    },
    CountDownFlashSale: function (el) {
        var end_time = $(el).data('end-time');
        R.CountDown(end_time, el);

    }

}


$(function () {
    R.ProductDetail.Init();
})

function flyToElement(flyer, flyingTo) {
    var $func = $(this);

    // Nhân bản đối tượng(hình ảnh) sẽ bay vào giỏ hàng
    var flyerClone = $(flyer).clone();

    // Thiết lập đối tượng nhân bản này trùng với đối tượng thực tế 
    $(flyerClone).css({
        position: 'absolute',
        top: $(flyer).offset().top + "px",
        left: $(flyer).offset().left + "px",
        opacity: 1,
        'z-index': 1000
    }).appendTo($('body'));

    // Lấy về tọa độ của giỏ hàng
    var gotoX = $(flyingTo).offset().left;
    var gotoY = $(flyingTo).offset().top;

    // Hiệu ứng bay vào giỏ hàng
    $(flyerClone).animate({
        opacity: 0.4,
        left: gotoX,
        top: gotoY,
        width: $(flyingTo).width(),
        height: $(flyingTo).height()
    }, 700,
        function () {
            // Hiệu ứng rung lắc khi sản phẩm đã bay vào giỏ hàng
            $(flyingTo).effect("shake", function () {
                // Ẩn đối tượng sản phẩm bay vào giỏ hàng và delete nó
                $(flyerClone).fadeOut('fast', function () {
                    $(flyerClone).remove();
                });
            });
        });
}