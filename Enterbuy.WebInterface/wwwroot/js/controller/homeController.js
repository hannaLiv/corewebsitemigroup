﻿R.Home = {
    Init: function () {
        R.Home.RegisterEvent();
        R.Home.culture = R.Culture();
        R.Home.currentLocationId = R.CurrentLocationId();
        R.Home.BindingTotal();
        R.Home.CountDownFlashSale('.flash-sale');

    },
    RegisterEvent: function () {
        $('.tab-region').off('click').on('click', function () {
            var tab = $(this).data('tab');
            $('.tab-region-categories .tab-region').each(function (index) {
                if ($(this).data('tab') == tab) {
                    $(this).removeClass('active');
                }
            });
            $(this).addClass('active');
            $(this).closest('.swiper-wrapper').find('.swiper-slide-active').removeClass('swiper-slide-active');
            $(this).closest('.swiper-slide').addClass('swiper-slide-active');
            var region_id = $(this).data('region-id');
            R.Home.SwitchRegion($(this), region_id)
        });
        $('.view-remain-product').off('click').on('click', function () {
            var el = $(this);
            var id = $(this).data('id');
            var skip = $(this).data('skip');
            var size = $(this).data('size');
            R.Home.ViewMoreRegion(id, el, skip, size);
        });
       
    },
    SwitchRegion: function (el, region_id) {
        var url = R.Home.culture + "/Home/SwitchRegion";
        var params = {
            region_id: region_id
        };
        $.post(url, params, function (response) {
            el.closest('.container').find("._binding_product").replaceWith(response);
            var total_product = el.closest('.container').find("._binding_product").data('total-product');
            var page_size = el.closest('.container').find(".remain-product").data("page-size");
            //remain-product
            R.Home.BindingTotal();
            R.Extra.BindingExtraToProduct();
        });
    },
    ViewMoreRegion: function (id, el, skip, size) {
        //int zone_parent_id, int locationId, int skip, int size
        var params = {
            zone_parent_id: id,
            locationId: R.Home.currentLocationId,
            skip: skip,
            size: size
        }
        $.post(R.Home.culture + '/Home/ViewMoreRegion', params, function (response) {
            el.closest('.container').find('._binding_product').append(response);
            el.hide();
            el.closest(".san-go-nb").find(".view-remain-product").hide();
            //R.Home.BindingTotal();
            R.Extra.BindingExtraToProduct();
            R.Home.RegisterEvent();
        })
    },
    BindingTotal: function () {
        $('.set-total').each(function (el) {
            var total = $(this).data('total');
            var id = $(this).data('id');
            if (total <= 10)
                $(this).closest('.container').find('.remain-product').html("<b>" + 0 + "</b>");
            else {
                var remain = total - 10;
                $(this).closest('.container').find('.remain-product').html("<b>" + remain + "</b>");
                $(this).closest('.container').find('.remain-product').parent().show();
                $(this).closest('.container').find('.remain-product').parent().data('size', total - 10);
                $(this).closest('.container').find('.remain-product').parent().data('id', id);
            }

        })
    },
    CountDownFlashSale: function (el) {
        var end_time = $(el).data('end-time');
        R.CountDown(end_time, el);

    }


}



$(function () {
    R.Home.Init()
})