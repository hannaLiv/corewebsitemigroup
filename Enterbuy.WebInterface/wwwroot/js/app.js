﻿new WOW().init();


function sliderPR() {
    var sliderPR = new Swiper('.slide-product.slide-1 .swiper-container', {
        slidesPerView: 6,
        spaceBetween: -1,
        loop: true,
        autoplay: {
            delay: 1000,
        },
        navigation: {
            nextEl: '.slide-product.slide-1 .swiper-button-next',
            prevEl: '.slide-product.slide-1 .swiper-button-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
            },
            567: {
                slidesPerView: 2.3,
            },
            767: {
                slidesPerView: 3,
            },
            991: {
                slidesPerView: 4,
            },
            1023: {
                slidesPerView: 5,
            },
            1365: {
                slidesPerView: 6,
            },
        },

    });
}

function sliderPR2() {
    var sliderPR2 = new Swiper('.slide-product.slide-2 .swiper-container', {
        slidesPerView: 6,
        spaceBetween: -1,
        loop: true,
        autoplay: {
            delay: 1000,
        },
        navigation: {
            nextEl: '.slide-product.slide-2 .swiper-button-next',
            prevEl: '.slide-product.slide-2 .swiper-button-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
            },
            567: {
                slidesPerView: 2,
            },
            767: {
                slidesPerView: 3,
            },
            991: {
                slidesPerView: 4,
            },
            1023: {
                slidesPerView: 5,
            },
            1365: {
                slidesPerView: 6,
            },
        },

    });
}


function sliderPR3() {
    var sliderPR3 = new Swiper('.slide-product.slide-3 .swiper-container', {
        slidesPerView: 6,
        spaceBetween: -1,
        loop: true,
        autoplay: {
            delay: 1000,
        },
        navigation: {
            nextEl: '.slide-product.slide-3 .swiper-button-next',
            prevEl: '.slide-product.slide-3 .swiper-button-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
            },
            567: {
                slidesPerView: 2,
            },
            767: {
                slidesPerView: 3,
            },
            991: {
                slidesPerView: 4,
            },
            1023: {
                slidesPerView: 5,
            },
            1365: {
                slidesPerView: 6,
            },
        },

    });
}




function slidercate() {
    var slidercate = new Swiper('.slide-cate .swiper-container', {
        slidesPerView: 2.5,
        spaceBetween: 20,
        loop: true,
        autoplay: true,
        breakpoints: {
            320: {
                slidesPerView: 1.2,
                spaceBetween: 10,
            },
            767: {
                slidesPerView: 1.2,
            },
            991: {
                slidesPerView: 1.5,
            },
            1023: {
                slidesPerView: 2,
            },
            1365: {
                slidesPerView: 2.5,
            },
        },

    });
}
slidercate();
sliderPR3();
sliderPR2();
sliderPR();


function topFunction() {
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 500);
}



function expand() {
    $(".content-cate-intro").toggleClass("expand");
    $('body,html').animate({
        scrollTop: 0                       // Scroll to top of body
    }, 0);
}

function expandClient() {
    $(".full-client").toggleClass("expand");
}

$(".list-menu-cate .list-group-item:first-child").click(function () {
    $(this).closest(".list-menu-cate").toggleClass("un-expand");
})


$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('.header').addClass("fixed");    // Fade in the arrow
        //   $(".list-menu-cate").addClass("un-expand");

    } else {
        $('.header').removeClass("fixed");   // Else fade out the arrow

    }
});



function slidebrand() {
    var slidebrand = new Swiper('.slide-brand.swiper-container', {
        slidesPerView: 6,
        spaceBetween: -1,
        loop: true,
        autoplay: true,
        breakpoints: {
            320: {
                slidesPerView: 1.5,
            },
            567: {
                slidesPerView: 2,
            },
            767: {
                slidesPerView: 3,
            },
            991: {
                slidesPerView: 4.5,
            },
            1023: {
                slidesPerView: 5,
            },
            1365: {
                slidesPerView: 6,
            },
        },

    });
}
slidebrand();

function spvx() {
    $(".menu-fix .spvx").toggleClass("show");
}




$(".select-local .dropdown-item").click(function () {
    var text = $(this).text();
    $(this).closest(".select-local").find(".dropdown-toggle").text(text);
})

$(".list-menu-right .heading").click(function () {
    $(this).closest(".list-menu-right").toggleClass("expand");
})

$(".star-rating i").click(function () {
    $(this).parent().find(".star-rating i").removeClass("checked");
    $(this).addClass("checked");
    $(this).prevAll().addClass("checked");
    $(this).nextAll().removeClass("checked");

});

function slidethumb1() {
    var galleryThumbs = new Swiper('.slide-product-detail .gallery-thumbs', {
        spaceBetween: 15,
        slidesPerView: 6,

        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            320: {
                slidesPerView: 3,
            },
            567: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
            767: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
            991: {
                slidesPerView: 7,
                spaceBetween: 20,
            },
            1023: {
                slidesPerView: 5,
                spaceBetween: 15,
            },
            1365: {
                slidesPerView: 5,
                spaceBetween: 25,
            },
        },
        navigation: {
            nextEl: '.slide-product-detail .swiper-button-next',
            prevEl: '.slide-product-detail .swiper-button-prev',
        },
    });
    var galleryTop = new Swiper('.slide-product-detail .gallery-top', {
        spaceBetween: 10,
        zoom: {
            maxRatio: 3,
        },
        thumbs: {
            swiper: galleryThumbs
        }
    });

}

function slidethumb2() {
    var galleryThumbs2 = new Swiper('.slide-product-detail-2 .gallery-thumbs-2', {
        spaceBetween: 10,
        slidesPerView: 6,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            320: {
                slidesPerView: 3,
            },
            567: {
                slidesPerView: 3,
                spaceBetween: 15,
            },
            767: {
                slidesPerView: 3.5,
                spaceBetween: 10,
            },
            991: {
                slidesPerView: 4,
                spaceBetween: 10,
            },
            1023: {
                slidesPerView: 4,
                spaceBetween: 15,
            },
            1365: {
                slidesPerView: 5,
                spaceBetween: 10,
            },
        },
        navigation: {
            nextEl: '.slide-product-detail-2 .swiper-button-next',
            prevEl: '.slide-product-detail-2 .swiper-button-prev',
        },
    });
    var galleryTop2 = new Swiper('.slide-product-detail-2 .gallery-top-2', {
        spaceBetween: 10,
        thumbs: {
            swiper: galleryThumbs2
        }
    });
}



function slideCate() {

    var sliderBlogcate = new Swiper('.slide-blog-cate.swiper-container', {
        slidesPerView: 1,
        spaceBetween: 15,
        loop: true,
        speed: 600,
        autoplay: true,
        navigation: {
            nextEl: '.slide-blog-cate .swiper-button-next',
            prevEl: '.slide-blog-cate .swiper-button-prev',
        },

    });
}
slideCate();
slidethumb1();
slidethumb2();

function expandListStore() {
    $(".list-add-store").toggleClass("expand");
}

$(".tab-flash-sale .link-tab-flash-sale").click(function () {
    $(".tab-flash-sale .link-tab-flash-sale").removeClass("active");
    $(this).addClass("active");
})

function sidevideo() {
    var sidevideo = new Swiper('.slide-video .swiper-container', {
        slidesPerView: 4,
        spaceBetween: 20,
        autoplay: true,
        loop: true,
        breakpoints: {
            320: {
                slidesPerView: 1.2,

            },
            567: {
                slidesPerView: 1.5,
            },
            767: {
                slidesPerView: 2.3,

            },
            991: {
                slidesPerView: 3,
            },
            1023: {
                slidesPerView: 3.5,
            },
            1365: {
                slidesPerView: 3.5,
            },
        },

    });
}

sidevideo();


function expandProductDes() {
    $(".product-description").toggleClass("expand");
    if ($(".product-description").hasClass("expand")) {
        $(".btn-outline-view-more").text("Đóng lại");
       
    } else {
        $(".btn-outline-view-more").text("Xem thêm");
        $('body,html').animate({
            scrollTop: 700
        }, 500);
    }
}
function expandZoneDes() {
    $(".content-cate-intro").toggleClass("expand");
    if ($(".content-cate-intro").hasClass("expand")) {
        $(".btn-outline-view-more").text("Đóng lại");

    } else {
        $(".btn-outline-view-more").text("Xem thêm");
        $('body,html').animate({
            scrollTop: 0
        }, 500);
    }
}



function expandAticleDes() {
    $(".article-description").toggleClass("expand");

}




$(".list-filter-modal .heading").click(function () {
    $(this).closest(".menu.sub").toggleClass("show");
});

$(".btn-x").click(function () {
    $(".btn-x").removeClass("active");
    $(this).toggleClass("active");
});

function slidethumbhome() {
    var galleryThumbs3 = new Swiper('.slide-banner-home .gallery-thumbs-home', {
        spaceBetween: -1,
        slidesPerView: 5,
        autoplay: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            320: {
                slidesPerView: 2,

            },
            567: {
                slidesPerView: 2.2,
            },
            767: {
                slidesPerView: 2.2,
            },
            991: {
                slidesPerView: 4,
            },
            1023: {
                slidesPerView: 4.5,
            },
            1365: {
                slidesPerView: 4.8,
            },
        },
    });
    var galleryTop3 = new Swiper('.slide-banner-home .gallery-top-home', {
        spaceBetween: 10,
        autoplay: true,
        thumbs: {
            swiper: galleryThumbs3
        },
        pagination: {
            el: '.slide-banner-home .swiper-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

}

slidethumbhome();

function slidermenu() {
    var slidermenu = new Swiper('.menu-op .swiper-container', {
        slidesPerView: 5,
        spaceBetween: 20,

        breakpoints: {
            320: {
                slidesPerView: 2.2,
            },
            567: {
                slidesPerView: 3.5,
            },
            767: {
                slidesPerView: 3,
            },
            991: {
                slidesPerView: 3.5,
            },
            1365: {
                slidesPerView: 4,
            },
        },

    });
}
slidermenu();


$(".list-menu-cate .list-group-item:gt(12)").css("display", "none");
$(".list-menu-cate .list-group-item:last-child").css("display", "block").click(function () {
    $(".list-menu-cate .list-group-item:gt(3)").toggleClass("d-block");
});


function timeIcon() {
    $(".btn-local .onhover").addClass("hide");
}



function slidernews() {
    var sliderPR = new Swiper('.slide-news  .swiper-container', {
        slidesPerView: 1.3,
        spaceBetween: 25,
        loop: true,
        autoplay: {
            delay: 1000,
        },

    });
}

slidernews();


$(document).ready(function () {
    $('form input').keydown(function (e) {

        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
});
