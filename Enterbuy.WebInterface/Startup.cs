﻿using Enterbuy.Data.Sql;
using Enterbuy.Statics.Configurations;
using Enterbuy.Statics.Extensions;
using Enterbuy.WebInterface.Registrations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;
using System.Text.Encodings.Web;

namespace Enterbuy.WebInterface
{
    public class Startup
    {


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AppSettings.SetConfig(configuration);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(MapperInitiator.Init().CreateMapper().RegisterMap());
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            MI.ES.ConfigES.Start();

            DependencyRegistrar.RegisterTypes(services);
            DependencyManager.SetServiceProvider(services.BuildServiceProvider());
            TypeMapRegistrar.Register();
            #region Register Localization
            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });




            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("vi-VN"),
                    new CultureInfo("en-US"),
                };

                options.DefaultRequestCulture = new RequestCulture(culture: "vi-VN", uiCulture: "vi-VN");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                //options.RequestCultureProviders = new[]{ new RouteDataRequestCultureProvider{
                //    IndexOfCulture = 1,
                //    IndexofUICulture = 1
                //}};
                options.RequestCultureProviders = new[] { new CookieRequestCultureProvider() };


            });

            services.AddSingleton<HtmlEncoder>(
            HtmlEncoder.Create(allowedRanges: new[] {
               System.Text.Unicode.UnicodeRanges.All
            }));
            #endregion
            services.AddMvc().AddXmlSerializerFormatters();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 404)
                {
                    context.Request.Path = "/Error/404.html";
                    await next();
                }

            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();




            app.UseMvc(routes =>
             {
                 routes.MapRoute(
                     name: "default",
                     //template: "{controller=Home}/{action=Index}/{id?}");
                     template: "{controller=Home}/{action=Index}");
                 //routes.MapRoute(
                 //  name: "P404",
                 //  template: "",
                 //  defaults: new { controller = "P404", action = "P404" },
                 //  constraints: new
                 //  {
                 //      alias = "p404.html"
                 //  });

                 //routes.MapRoute(
                 //  name: "BlogUrlRemaster",
                 //  template: "news/{alias}",
                 //  defaults: new { controller = "Blog", action = "RedirectAction" }
                 //);
                 //routes.MapRoute(
                 //  name: "BlogWithPaging",
                 //  template: "news/{alias}/page/{pageIndex}",
                 //  defaults: new { controller = "Blog", action = "RedirectAction" }
                 //);
                 routes.MapRoute(
                    name: "RecruitmentUrlRemaster",
                    template: "thong-tin/{alias}",
                    defaults: new { controller = "Recruitment", action = "Index" }
                  );
                 routes.MapRoute(
                   name: "RecruitmentWithPaging",
                   template: "thong-tin/{alias}/page/{pageIndex}",
                   defaults: new { controller = "Recruitment", action = "Index" }
                 );

             });
        }
    }
}