﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Http.Controllers;
using MI.Entity.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.WebInterface.UtilityWeb
{
    public class UtilRedrect
    {
        IConfigDao _config;
        public UtilRedrect(IConfigDao config)
        {
            _config = config;
        }

        public KeyValuePair<bool, Data.SqlServer.ModelDto.Redirect> ExistRedirect(string url)
        {
            var obj = _config.GetUrlRedrect(url);
            if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
            {
                return new KeyValuePair<bool, Data.SqlServer.ModelDto.Redirect>(true, obj);
            }
            return new KeyValuePair<bool, Data.SqlServer.ModelDto.Redirect>(false, new Data.SqlServer.ModelDto.Redirect());
        }
    }
}
