namespace Enterbuy.WebInterface.Models
{
    public class MetaModel
    {
        public string Value { get; set; }

        public bool Status { get; set; }
    }
}