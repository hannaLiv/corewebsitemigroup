﻿using Dapper;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enterbuy.WebInterface.Models
{
    public interface IBannerAdsRepository
    {
        List<BannerAdsViewModel> GetBannerAds(string langCode);
        BannerAdsViewModel GetBannerAds_By_Code(string langCode, string code);
        string GetConfigByName(string lang_code, string name);
    }
    public class BannerAdss : IBannerAdsRepository
    {
        private readonly string _connStr;
        private readonly IExecuters _executers;
        public string GetConfigByName(string lang_code, string name)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_GetConfigByName";
            p.Add("@lang_code", lang_code);
            p.Add("@configName", name);
            var r = _executers.ExecuteCommand(_connStr, conn => conn.QueryFirstOrDefault<string>(commandText, p, commandType: System.Data.CommandType.StoredProcedure));
            return r;
        }
        public BannerAdsViewModel GetBannerAds_By_Code(string langCode, string code)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_Get_BannerAds_By_Code";
            p.Add("@langCode", langCode);
            p.Add("@code", code);
            var r = _executers.ExecuteCommand(_connStr, conn => conn.QueryFirstOrDefault<BannerAdsViewModel>(commandText, p, commandType: System.Data.CommandType.StoredProcedure));

            return r;
        }
        public List<BannerAdsViewModel> GetBannerAds(string langCode)
        {
            var p = new DynamicParameters();
            var commandText = "usp_Web_Get_BannerAds";
            p.Add("@langCode", langCode);

            var result = _executers.ExecuteCommand(_connStr, conn => conn.Query<BannerAdsViewModel>(commandText, p, commandType: System.Data.CommandType.StoredProcedure)).ToList();
            return result;
        }

    }
}
