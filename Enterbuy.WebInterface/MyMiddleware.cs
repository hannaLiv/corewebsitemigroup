﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Enterbuy.WebInterface
{
    public class MyMiddleware
    {
        private RequestDelegate _nextDelegate;
        private IServiceProvider _serviceProvider;

        public MyMiddleware(RequestDelegate nextDelegate, IServiceProvider serviceProvider)
        {
            _nextDelegate = nextDelegate;
            _serviceProvider = serviceProvider;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            string requestURL = httpContext.Request.Path.ToString().ToLower();

            //redirect domain/home/index and domain/home to domain
            if (requestURL.Contains("/home/index") || requestURL.EndsWith("/home"))
            {
                httpContext.Response.Redirect("/");
            }
            // redirect domain/home/something  to domain/something
            else if (requestURL.Contains("/home/"))
            {
                Regex reg = new Regex("/home/(.+)");
                Match match = reg.Match(requestURL);
                string value = match.Groups[1].Value;
                httpContext.Response.Redirect("/" + value);
            }
            else
            {

                await _nextDelegate.Invoke(httpContext);
            }

        }
    }
}
