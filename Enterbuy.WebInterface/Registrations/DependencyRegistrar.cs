﻿using Enterbuy.Data.Sql;
using Enterbuy.Data.SqlServer;
using Enterbuy.Data.SqlServer.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.DbContexts;
using Enterbuy.Services.Common.Services;
using Enterbuy.Services.Common.Services.Interfaces;
using Enterbuy.Services.Home.Services;
using Enterbuy.Services.Product.Services;
using Enterbuy.Services.Blog.Services;
using Microsoft.Extensions.DependencyInjection;
using Enterbuy.Services.Contact.Services;
using Enterbuy.Services.Contact.Services.Interfaces;
using Microsoft.AspNetCore.Http;

using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Utils;
using RamData;

namespace Enterbuy.WebInterface.Registrations
{
    public class DependencyRegistrar
    {
        public static void RegisterTypes(IServiceCollection services)
        {
            services.AddTransient<EnterbuyDbContext>();

            services.AddTransient<IDatabaseFactory, DatabaseFactory>();

            services.AddTransient<IProductDao, ProductDao>();

            services.AddTransient<IProductService, ProductService>();

            services.AddTransient<IBannerAdsDao, BannerAdsDao>();
            services.AddTransient<IArticleDao, ArticleDao>();
            services.AddTransient<IManufactureDao, ManufactureDao>();
            services.AddTransient<IBannerAdsService, BannerAdsService>();
            services.AddTransient<IArticleDao, ArticleDao>();
            services.AddTransient<IBlogServices, BlogServices>();

            services.AddTransient<IConfigDao, ConfigDao>();
            services.AddTransient<IConfigService, ConfigService>();

            services.AddTransient<IZoneDao, ZoneDao>();

            services.AddTransient<IMenuZoneServices, MenuZoneService>();

            services.AddTransient<IContactService, ContactService>();
            services.AddTransient<IContactDao, ContactDao>();

            services.AddTransient<ILocationDao, LocationDao>();
            services.AddTransient<ILocationService, LocationService>();

            services.AddTransient<IProductSaleInMonthService, ProductSaleInMonthService>();
            services.AddTransient<IBannerHomePageService, BannerHomePageService>();
            services.AddTransient<IBannerHomePageService, BannerHomePageService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<ICookieLocationUtility, CookieUtility>();
            services.AddTransient<IConfigSEOUtilityy, ConfigSEOUtility>();
            services.AddTransient<IConfig, Config>();
            services.AddTransient<IPromotionDao, PromotionDao>();
            services.AddTransient<ICollaboratorsDao, CollaboratorsDao>();
            #region Register Localization
            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            services.Configure<Microsoft.AspNetCore.Builder.RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("vi-VN"),
                    new CultureInfo("en-US"),
                };

                options.DefaultRequestCulture = new RequestCulture(culture: "vi-VN", uiCulture: "vi-VN");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                //options.RequestCultureProviders = new[]{ new RouteDataRequestCultureProvider{
                //    IndexOfCulture = 1,
                //    IndexofUICulture = 1
                //}};
                options.RequestCultureProviders = new[] { new CookieRequestCultureProvider() };
            });
            #endregion
        }
    }
}