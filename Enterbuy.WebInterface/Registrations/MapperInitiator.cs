﻿using AutoMapper;

namespace Enterbuy.WebInterface.Registrations
{
    public class MapperInitiator
    {
        public static MapperConfiguration Init()
        {
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
                cfg.AllowNullCollections = true;
                cfg.ForAllMaps((map, exp) =>
                {
                    foreach (var unmappedPropertyName in map.GetUnmappedPropertyNames())
                        exp.ForMember(unmappedPropertyName, opt => opt.Ignore());
                });
                RegisterMapping(cfg);
            });
            return mapperConfig;
        }

        public static void RegisterMapping(IMapperConfigurationExpression cfg)
        {
        }
    }
}