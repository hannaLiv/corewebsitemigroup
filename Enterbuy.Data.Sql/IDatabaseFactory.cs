﻿using Enterbuy.Data.Sql.Dto;
using Microsoft.EntityFrameworkCore;
using System;

namespace Enterbuy.Data.Sql
{
    public interface IDatabaseFactory
    {
        DbContext GetDbContext<T>() where T : BaseDto;

        string GetConnectionString(Type type, DbActionType dbActionType = DbActionType.Write);
    }
}