﻿using Dapper;
using Enterbuy.Data.Sql.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.Data.Sql.Dao
{
    public class BaseSqlServerDao<T> : ISqlServerDao<T> where T : BaseEntity
    {
        protected IDatabaseFactory databaseFactory;
        protected DbContext DbContext { get; set; }
        protected DbSet<T> DbSet { get; set; }

        public BaseSqlServerDao(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
            this.DbContext = databaseFactory.GetDbContext<T>();
            this.DbSet = DbContext.Set<T>();
        }

        public virtual IDbConnection ReadConnection => new SqlConnection(GetConnectionString(DbActionType.Read));

        public virtual IDbConnection WriteConnection => new SqlConnection(GetConnectionString(DbActionType.Write));

        protected IDbTransaction Transaction;

        public T GetById(object id)
        {
            var entity = DbSet.Find(id);
            return entity;
        }

        public async Task<T> GetByIdAsync(object id)
        {
            var entity = await DbSet.FindAsync(id);
            return entity;
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
            SaveChanges();
        }

        public async Task AddAsync(T entity)
        {
            await DbSet.AddAsync(entity);
            await SaveChangesAsync();
        }

        public void Update(T entity)
        {
            DbContext.Entry(entity).CurrentValues.SetValues(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
            SaveChanges();
        }

        public async Task UpdateAsync(T entity)
        {
            DbContext.Entry(entity).CurrentValues.SetValues(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
            await SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
            SaveChanges();
        }

        public async Task DeleteAsync(T entity)
        {
            DbSet.Remove(entity);
            await SaveChangesAsync();
        }

        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await DbContext.SaveChangesAsync();
        }

        public void AddWithoutTransaction(T entity)
        {
            DbSet.Add(entity);
        }

        public void UpdateWithoutTransaction(T entity)
        {
            DbContext.Entry(entity).CurrentValues.SetValues(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public List<V> QuerySP<V>(string spName, object param)
        {
            using (var dbConnection = ReadConnection)
            {
                dbConnection.Open();
                var queryResult = dbConnection.Query<V>(
                    spName,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                );
                var result = queryResult.ToList();
                return result;
            }
        }

        public async Task<List<V>> QuerySPAsync<V>(string spName, object param)
        {
            try
            {
                using (var dbConnection = ReadConnection)
                {

                    dbConnection.Open();
                    var queryResult = await dbConnection.QueryAsync<V>(
                        spName,
                        param,
                        null,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                    );
                    var result = queryResult.ToList();
                    return result;
                }
            }
            catch(Exception ex)
            {

            }
            return new List<V>();
        }

        public async Task<object> QueryScalarSPAsync(string spName, object param)
        {
            using (var dbConnection = ReadConnection)
            {
                dbConnection.Open();
                var queryResult = await dbConnection.ExecuteScalarAsync(
                    spName,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                );

                return queryResult;
            }
        }

        public async Task<object> QueryScalarStatementAsync(string sql, object param)
        {
            using (var dbConnection = ReadConnection)
            {
                dbConnection.Open();
                var queryResult = await dbConnection.ExecuteScalarAsync(
                    sql,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.Text
                );

                return queryResult;
            }
        }

        public object QueryScalarStatement(string sql, object param)
        {
            using (var dbConnection = ReadConnection)
            {
                dbConnection.Open();
                var queryResult = dbConnection.ExecuteScalar(
                    sql,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.Text
                );

                return queryResult;
            }
        }

        public List<V> QuerySP<V>(string spName, object param, string connectionString)
        {
            using (var dbConnection = CreateConnection(connectionString))
            {
                dbConnection.Open();
                var queryResult = dbConnection.Query<V>(
                    spName,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                );
                var result = queryResult.ToList();
                return result;
            }
        }

        public async Task<object> ExecuteScalarStatementAsync(string sql, object param)
        {
            using (var dbConnection = WriteConnection)
            {
                dbConnection.Open();
                var queryResult = await dbConnection.ExecuteScalarAsync(
                    sql,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.Text
                );

                return queryResult;
            }
        }

        public int ExecuteSp(string spName, object param)
        {
            using (var dbConnection = WriteConnection)
            {
                dbConnection.Open();
                var queryResult = dbConnection.Execute(
                    spName,
                    param,
                    null,
                    null,
                    CommandType.StoredProcedure
                );
                var result = queryResult;
                return result;
            }
        }

        public int ExecuteSp(string spName, object param, string connectionString)
        {
            using (var dbConnection = CreateConnection(connectionString))
            {
                dbConnection.Open();
                var queryResult = dbConnection.Execute(
                    spName,
                    param,
                    null,
                    null,
                    CommandType.StoredProcedure
                );
                var result = queryResult;
                return result;
            }
        }

        public object ExecuteScalarSp(string spName, object param)
        {
            using (var dbConnection = WriteConnection)
            {
                dbConnection.Open();
                var queryResult = dbConnection.ExecuteScalar(
                    spName,
                    param,
                    null,
                    null,
                    CommandType.StoredProcedure
                );
                var result = queryResult;
                return result;
            }
        }

        public object ExecuteScalarSp(string spName, object param, string connectionString)
        {
            using (var dbConnection = CreateConnection(connectionString))
            {
                dbConnection.Open();
                var queryResult = dbConnection.ExecuteScalar(
                    spName,
                    param,
                    null,
                    null,
                    CommandType.StoredProcedure
                );
                var result = queryResult;
                return result;
            }
        }

        private IDbConnection CreateConnection(string connectionString)
        {
            return new SqlConnection(connectionString);
        }

        public int ExecuteTransactionSp(string spName, object param)
        {
            var queryResult = WriteConnection.Execute(
                spName,
                param,
                Transaction,
                null,
                CommandType.StoredProcedure
            );
            var result = queryResult;
            return result;
        }

        public object ExecuteScalarTransactionSp(string spName, object param)
        {
            var queryResult = WriteConnection.ExecuteScalar(
                spName,
                param,
                Transaction,
                null,
                CommandType.StoredProcedure
            );
            var result = queryResult;
            return result;
        }

        public virtual List<V> QueryStatement<V>(string statement, object param)
        {
            using (var dbConnection = ReadConnection)
            {
                dbConnection.Open();
                var queryResult = dbConnection.Query<V>(
                    statement,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.Text
                );
                var result = queryResult.ToList();
                return result;
            }
        }

        public virtual async Task<List<V>> QueryStatementAsync<V>(string statement, object param)
        {
            using (var dbConnection = ReadConnection)
            {
                dbConnection.Open();
                var queryResult = await dbConnection.QueryAsync<V>(
                    statement,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.Text
                );
                var result = queryResult.ToList();
                return result;
            }
        }

        public virtual List<V> QueryStatement<V>(string statement, object param, string connectionString)
        {
            using (var dbConnection = CreateConnection(connectionString))
            {
                dbConnection.Open();
                var queryResult = dbConnection.Query<V>(
                    statement,
                    param,
                    null,
                    commandTimeout: null,
                    commandType: CommandType.Text
                );
                var result = queryResult.ToList();
                return result;
            }
        }

        public virtual int ExecuteStatement(string statement, object param, string connectionString)
        {
            using (var dbConnection = CreateConnection(connectionString))
            {
                dbConnection.Open();
                var queryResult = dbConnection.Execute(
                    statement,
                    param,
                    null,
                    null,
                    CommandType.Text
                );
                return queryResult;
            }
        }

        public virtual int ExecuteStatement(string statement, object param)
        {
            using (var dbConnection = WriteConnection)
            {
                dbConnection.Open();
                var queryResult = dbConnection.Execute(
                    statement,
                    param,
                    null,
                    null,
                    CommandType.Text
                );
                return queryResult;
            }
        }

        public virtual int ExecuteTransactionStatement(string statement, object param)
        {
            var queryResult = WriteConnection.Execute(
                statement,
                param,
                Transaction,
                null,
                CommandType.Text
            );
            return queryResult;
        }

        public virtual object ExecuteScalarStatement(string statement, object param, string connectionString)
        {
            using (var dbConnection = CreateConnection(connectionString))
            {
                dbConnection.Open();
                var queryResult = dbConnection.ExecuteScalar(
                    statement,
                    param,
                    null,
                    null,
                    CommandType.Text
                );
                return queryResult;
            }
        }

        public virtual object ExecuteScalarStatement(string statement, object param)
        {
            using (var dbConnection = WriteConnection)
            {
                dbConnection.Open();
                var queryResult = dbConnection.ExecuteScalar(
                    statement,
                    param,
                    null,
                    null,
                    CommandType.Text
                );
                return queryResult;
            }
        }

        public virtual object ExecuteScalarTransactionStatement(string statement, object param)
        {
            var queryResult = WriteConnection.ExecuteScalar(
                statement,
                param,
                Transaction,
                null,
                CommandType.Text
            );
            return queryResult;
        }

        public virtual string GetConnectionString(DbActionType dbActionType)
        {
            return databaseFactory.GetConnectionString(typeof(T), dbActionType);
        }
    }
}