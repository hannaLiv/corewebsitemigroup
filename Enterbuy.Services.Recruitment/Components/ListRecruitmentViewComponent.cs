﻿using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enterbuy.Services.Common.Services.Interfaces;
using Enterbuy.Services.Blog.Services;

namespace Enterbuy.Services.Recruitment.Components
{
    public class ListRecruitmentViewComponent : ViewComponent
    {
        private readonly IBlogServices _blogServices;
        private readonly IMenuZoneServices _menuZoneServices;

        public ListRecruitmentViewComponent(IBlogServices blogServices, IMenuZoneServices menuZoneServices)
        {
            _blogServices = blogServices;
            _menuZoneServices = menuZoneServices;
        }

        public async Task<IViewComponentResult> InvokeAsync(string alias, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 6;
            int total = 0;
            //string culture_code = "vi-VN";
            List<ArticleMinify> articleMinifies = new List<ArticleMinify>();
            List<ZoneByTreeViewMinify> zoneByTreeViewMinifies = new List<ZoneByTreeViewMinify>();
            var list_blog_childs = new List<ZoneByTreeViewMinify>();
            var zone_blog_parent = new ZoneByTreeViewMinify();
            try
            {
                zoneByTreeViewMinifies = await _menuZoneServices.GetZoneByTreeViewMinifies((int)TypeZone.Recruitment, "vi-VN", 0);
                articleMinifies = _blogServices.GetArticlesInZoneId_Minify(0, (int)TypeZone.Recruitment, "vi-VN", "", pageIndex, pageSize, out total);
                if (articleMinifies.Count() > 0)
                {
                    for (int i = 0; i < articleMinifies.Count(); i++)
                    {
                        articleMinifies[i].ArticleRecruitment = JsonConvert.DeserializeObject<ArticleRecruitment>(articleMinifies[i].Metadata);
                    }
                }
                if (zoneByTreeViewMinifies.Count() > 0)
                {
                    zone_blog_parent = zoneByTreeViewMinifies.Where(r => r.ParentId == 0).FirstOrDefault();
                }

                if (zone_blog_parent.Id > 0)
                {
                    list_blog_childs = zoneByTreeViewMinifies.Where(r => r.ParentId == zone_blog_parent.Id).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            ViewBag.TotalPage = total;
            ViewBag.pageIndex = pageIndex;
            ViewBag.pageSize = pageSize;
            ViewBag.ZoneParent = zone_blog_parent;
            ViewBag.ZoneChilds = list_blog_childs;
            ViewBag.Top_newest = articleMinifies;
            return View();
        }
    }
}