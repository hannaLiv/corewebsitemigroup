﻿using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Recruitment.Controllers
{
    public class RecruitmentController : BaseController
    {
        [Route("tuyen-dung.html")]
        public IActionResult Index(string alias, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 6;
            ViewData["IsExpand"] = false;
            ViewBag.Alias = alias;
            return View();
        }
        [Route("thong-tin/{alias}/{url}-{articleId}.html")]
        public IActionResult Details(int articleId)
        {
            ViewData["IsExpand"] = false;
            return View();
        }
    }
}