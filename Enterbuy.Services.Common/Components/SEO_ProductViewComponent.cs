﻿using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Common.Components
{
    public class SEO_ProductViewComponent : ViewComponent
    {

        public SEO_ProductViewComponent()
        {
            
        }
        public IViewComponentResult Invoke(ProductDetail product)
        {
            ViewBag.Detail = product;
            return View();
        }
    }
}
