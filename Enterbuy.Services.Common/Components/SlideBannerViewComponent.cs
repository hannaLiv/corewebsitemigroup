﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Common.Services.Interfaces;
using Utils;
using System;
using System.Collections.Generic;
using Enterbuy.Services.Common.Models;

namespace Enterbuy.Services.Common.Components
{
    public class SlideBannerViewComponent : ViewComponent
    {
        private readonly IBannerAdsService _bannerAdsService;

        public SlideBannerViewComponent(IBannerAdsService bannerAdsService)
        {
            _bannerAdsService = bannerAdsService;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool isService)
        {
            var result_Service = await _bannerAdsService.GetSlideBanner("vi-VN");
            var result = new TopSlideBannerHomePageViewModel();
            result.TopSlideBanner = new List<BannerHomePage>();
            var now = DateTime.Now;
            foreach (var item in result_Service.TopSlideBanner.OrderBy(n => n.Order))
            {
                if(!string.IsNullOrEmpty(item.DateStart) && !string.IsNullOrEmpty(item.DateEnd))
                {
                    DateTime DateStart, DateEnd;
                    if (DateTime.TryParse(item.DateStart, out DateStart) && DateTime.TryParse(item.DateEnd, out DateEnd))
                    {
                        if(now >= DateStart && now <= DateEnd)
                        {
                            item.Image = UIHelper.StoreFilePath(item.Image, false);                            
                            result.TopSlideBanner.Add(item);
                        }
                    }    
                }                
            }
            ViewBag.IsService = isService;
            return View(result);
        }
    }
}