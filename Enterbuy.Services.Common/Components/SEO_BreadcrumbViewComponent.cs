﻿using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Common.Components
{
    public class SEO_BreadcrumbViewComponent : ViewComponent
    {

        public SEO_BreadcrumbViewComponent()
        {

        }
        public IViewComponentResult Invoke(List<ZoneByTreeViewMinify> bread, string page, string type_page, string titleDetail = "")
        {
            if (bread != null)
            {
                ViewBag.BreadList = bread;
            }
            if (bread == null && !string.IsNullOrEmpty(page))
            {
                ViewBag.BreadSingle = page;
                ViewBag.BreadType = type_page;
                ViewBag.TitleDetail = titleDetail;
            }
            return View();
        }
    }
}
