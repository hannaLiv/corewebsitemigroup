﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class ViewMoreSeachViewComponent : ViewComponent
    {
        public ViewMoreSeachViewComponent()
        {

        }
        public IViewComponentResult Invoke(string keyword = "All", int pageIndex = 1, int pageSize = 12)
        {
            var data = new MI.ES.ProductSuggestResponse();
            long page = 0;
            try
            {
                if (keyword == "All")
                {
                    keyword = "";
                }
                keyword = keyword.Replace("_", " ");
                data = MI.ES.BCLES.AutocompleteService.SuggestEnterBuyAsync(keyword, Utils.Utility.DefaultLang, pageIndex, pageSize, out page, 0, new List<string>());
            }
            catch (Exception ex)
            {

            }
            return View(data);
        }
    }
}