﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Components
{
    public class BreadcrumbViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;

        public BreadcrumbViewComponent(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }

        public IViewComponentResult Invoke(int zoneId, string titleDetail = "")
        {
            //var breadcrumbs = await _zoneDao.GetBreadcrumbByZoneId(zoneId, "vi-VN");
            //return View(breadcrumbs);
            ViewBag.ZoneId = zoneId;
            ViewBag.TitleDetail = titleDetail;
            return View();
        }
    }
}