﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.Common.Services.Interfaces
{
    public interface IMenuZoneServices
    {
        Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewMinifies(int type, string langCode, int parentId);
        Task<ZoneDetail>  GetZoneDetail(int zoneId, string langCode);
        Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewShowMenuMinifies(int type, string lang_code, int parentId, int isShowMenu);
        Task<List<ZoneByTreeViewMinify>> GetListZoneByParentId(int type, string lang_code);
       
    }
}
