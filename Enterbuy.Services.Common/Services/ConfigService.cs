﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Common.Services.Interfaces;
using Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Common.Services
{
    public class ConfigService : IConfigService
    {
        private readonly IConfigDao _configDao;
        private readonly RamData.IConfig _config;
        private readonly IBannerAdsService _bannerAds;
        public ConfigService(IConfigDao configDao, IBannerAdsService bannerAdsService, RamData.IConfig config)
        {
            _configDao = configDao;
            _bannerAds = bannerAdsService;
            _config = config;
        }

        public int CreateComment(int objectId, int objectType, string name, string phoneOrEmail, string avatar, string content, string type, int rate, string lang_code, int parentId)
        {
            int result = 0;
            try
            {
                result = _configDao.CreateComment(objectId, objectType, name, phoneOrEmail, avatar, content, type, rate, lang_code, parentId);
                if (result > 0)
                {
                    Utils.Utility.SendMail("Vừa có một comment mới từ khách hàng. Mời bạn đăng nhập cms để check tin nhắn .", _config.GetByName("MailMess"), _config.GetByName("MailManager"));
                }
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return result;
        }

        public int CreateContact(ServiceTicket ticket)
        {
            int result = 0;
            try
            {
                result = _configDao.CreateContact(ticket);
                if (result > 0)
                {
                    Utils.Utility.SendMail("Bạn có một tin nhắn mới từ khách hàng. Truy cập hệ thống để xem .", _config.GetByName("MailMess"), _config.GetByName("MailManager"));
                }

            }
            catch (Exception ex)
            {
                result = -1;
            }
            return result;
        }

        public int CreateRating(int objectId, int objectType, int rate)
        {
            int result = 0;
            try
            {
                result = _configDao.CreateRating(objectId, objectType, rate);
            }
            catch (Exception)
            {
                result = -1;
            }
            return result;
        }

        public int CreateViewCount(int objectId, string type)
        {
            int result = 0;
            try
            {
                result = _configDao.CreateViewCount(objectId, type);
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return result;
        }

        public ConfigViewModel GetArticlesInZoneId_Minify(string configName, string langCode)
        {
            ConfigViewModel configViewModel = new ConfigViewModel();
            try
            {
                configViewModel = _configDao.GetArticlesInZoneId_Minify(configName, langCode);
            }
            catch (Exception)
            {

            }
            return configViewModel;
        }

        public string ConfigWebsite(string domain, string page_name)
        {
            var favicon = _bannerAds.GetConfigByName("vi-VN", "Favicon");
            var r = "";
            r += "<meta name=\"viewport\" content=\"width = device-width, initial-scale = 1\">\r\n";
            r += "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n";
            r += "<meta name=\"theme-color\" content=\"#081b62\" />\r\n";
            r += "<meta name=\"msapplication-navbutton-color\" content=\"#081b62\">\r\n";
            r += "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\r\n";
            r += "<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\r\n";
            r += "<meta http-equiv=\"Content-Language\" content=\"vi\" />\r\n";
            r += "<meta http-equiv=\"audience\" content=\"general\" />\r\n";
            r += "<meta name=\"resource-type\" content=\"document\" />\r\n";
            r += "<meta name=\"abstract\" content=\"" + page_name + "\" />\r\n";
            r += "<meta name=\"classification\" content=\"" + page_name + "\" />\r\n";
            r += "<meta name=\"area\" content=\"" + page_name + "\" />\r\n";
            r += "<meta name=\"placename\" content=\"Việt Nam\" />\r\n";
            r += "<meta name=\"author\" content=\"" + domain + "\" />\r\n";
            r += "<meta name=\"copyright\" content=\"©2020" + domain + "\" />\r\n";
            r += "<meta name=\"owner\" content=\"" + domain + "\" />\r\n";
            r += "<meta name=\"distribution\" content=\"Global\" />\r\n";
            r += "<link rel=\"alternate\" href=\"" + domain + "\" />\r\n";

            r += "<link rel=\"shortcut icon\" type=\"image / png\" href=\"" + UIHelper.StoreFilePath(favicon, false) + "\"/>";
            return r;
        }
    }
}
