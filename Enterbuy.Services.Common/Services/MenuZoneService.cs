﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Services.Common.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Enterbuy.Services.Common.Services
{
    public class MenuZoneService : IMenuZoneServices
    {
        private readonly IZoneDao _zoneDao;

        public MenuZoneService(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }

        public async Task<List<ZoneByTreeViewMinify>> GetListZoneByParentId(int type, string lang_code)
        {
            var result = await _zoneDao.GetListZoneByParentId(type, lang_code);
            return result;
        }

        //public async Task<ZoneToRedirect> GetZoneByAlias(string url, string langCode)
        //{
        //    var result = await _zoneDao.GetZoneByAlias(url, langCode);
        //    return result;
        //}

        public async Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewMinifies(int type, string langCode, int parentId)
        {
            var result = await _zoneDao.GetZoneByTreeViewMinifies(type, langCode, parentId);
            return result;
        }

        public async Task<List<ZoneByTreeViewMinify>> GetZoneByTreeViewShowMenuMinifies(int type, string lang_code, int parentId, int isShowMenu)
        {
            var result = await _zoneDao.GetZoneByTreeViewShowMenuMinifies(type, lang_code, parentId, isShowMenu);
            return result;
        }

        public async Task<ZoneDetail>  GetZoneDetail(int zoneId, string langCode)
        {
            var result = await _zoneDao.GetZoneDetail(zoneId, langCode);
            return result;
        }
    }
}