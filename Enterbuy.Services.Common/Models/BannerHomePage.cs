﻿namespace Enterbuy.Services.Common.Models
{
    public class BannerHomePage
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Order { get; set; }
        public bool Show { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        //1 là banner , 2 side
        public BannerHomePage()
        {
            this.Title = string.Empty;
            this.Url = string.Empty;
            this.Description = string.Empty;
            this.Image = string.Empty;
            this.Order = 0;
            this.Show = false;
            this.DateStart = string.Empty;
            this.DateEnd = string.Empty;
        }
    }
}