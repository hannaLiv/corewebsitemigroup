﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Common.Controlllers
{
    public class CollaboratorsController : BaseController
    {
        public ICollaboratorsDao collaboratorsDao { get; set; }
        public CollaboratorsController(ICollaboratorsDao _collaboratorsDao)
        {
            collaboratorsDao = _collaboratorsDao;
        }

        [Route("cong-tac-vien")]
        public IActionResult Index(string email = "", string search = "")
        {
            ViewData["IsExpand"] = false;
            ViewBag.Seach = search;
            ViewBag.Email = email;
            var lstObj = collaboratorsDao.GetOrderByEmail(email);

            return View(lstObj);
        }
    }
}
