﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Common.Controlllers
{
    public class SearchController : BaseController
    {
        [Route("tim-kiem/{keyword}")]
        public IActionResult Index(string keyword)
        {
            ViewData["IsExpand"] = false;
            var data = new MI.ES.ProductSuggestResponse();
            long page = 0;
            try
            {
                if (keyword == "All")
                {
                    keyword = "";
                }
                keyword = keyword.Replace("_", " ");
                data = MI.ES.BCLES.AutocompleteService.SuggestEnterBuyAsync(keyword, Utils.Utility.DefaultLang, 1, 24, out page, 0, new List<string>());
            }
            catch (Exception ex)
            {

            }
            ViewBag.TuKhoa = keyword;

            ViewBag.total = page;
            return View(data);
        }
        public IActionResult ViewMore(string keyword = "All", int pageIndex = 1, int pageSize = 12)
        {
            var data = new MI.ES.ProductSuggestResponse();
            long page = 0;
            try
            {
                if (keyword == "All")
                {
                    keyword = "";
                }
                keyword = keyword.Replace("_", " ");
                data = MI.ES.BCLES.AutocompleteService.SuggestEnterBuyAsync(keyword, Utils.Utility.DefaultLang, pageIndex, pageSize, out page, 0, new List<string>());
            }
            catch (Exception ex)
            {

            }
            return PartialView(data);
        }
    }
}
