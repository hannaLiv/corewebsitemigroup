﻿using System.Net;
using System.Text;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RamData;

namespace Enterbuy.Services.Common.Controlllers
{
    public class AboutController : BaseController
    {
        private readonly IArticleDao _articleDao;
        private readonly IHttpContextAccessor _httpcontextaccessor;
        private readonly IConfig _configDao;
        public AboutController(IArticleDao articleDao, IHttpContextAccessor httpcontextaccessor, IConfig configDao)
        {
            _articleDao = articleDao;
            _httpcontextaccessor = httpcontextaccessor;
            _configDao = configDao;
        }
        [Route("gioi-thieu.html")]
        [Route("van-pham.html")]
        [Route("dong-the-nguyen.html")]
        [Route("nguyen-minh-tuan.html")]
        [Route("chinh-sach-va-quy-dinh.html")]
        [Route("faq.html")]
        //[Route("lien-he.html")]
        [Route("ho-tro.html")]
        [Route("huong-dan-mua-hang.html")]
        [Route("chinh-sach-khach-hang.html")]
        [Route("chinh-sach-bao-mat.html")]
        [Route("chinh-sach-bao-hanh.html")]
        [Route("chinh-sach-doi-tra.html")]
        [Route("giao-nhan-hang.html")]
        [Route("thanh-toan.html")]
        [Route("phong-chuyen-gia-nuoc.html")]
        public IActionResult Index()
        {
            ViewData["IsExpand"] = false;

            var url = _httpcontextaccessor.HttpContext.Request.Path.Value.Replace("/", "").Replace(".html", "");
            var artical = _articleDao.GetObjByAlias(url, Utils.Utility.DefaultLang);
            if (artical != null && artical.Id > 0)
            {
                return View(artical);
            }
            else
            {
                return NotFound();
            }
        }

        [Route("sitemap.xml")]
        public ActionResult SitemapXml()
        {
            var sitemapNodes = _configDao.GetByName("SiteMap");
            var url = Utils.UIHelper.StoreFilePath(sitemapNodes, false);
            var textFromFile = (new WebClient()).DownloadString(url);
            return this.Content(textFromFile, "text/xml", Encoding.UTF8);
        }
        [Route("robots.txt")]
        public ActionResult RobotsTxt()
        {
            var sitemapNodes = _configDao.GetByName("Robotxt");
            var url = Utils.UIHelper.StoreFilePath(sitemapNodes, false);
            var textFromFile = "";
            try
            {
                textFromFile = (new WebClient()).DownloadString(url);
            }
            catch
            {

            }

            return this.Content(textFromFile, "text/plain", Encoding.UTF8);
        }
    }
}
