﻿using System;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Common.Controlllers
{
    public class P404Controller : BaseController
    {
        private readonly IConfigDao _config;
        public P404Controller(IConfigDao config)
        {
            _config = config;
        }
        [Route("Error/404.html")]
        public IActionResult P404()
        {
           
            ViewData["IsExpand"] = false;
            return View();
        }
    }
}
