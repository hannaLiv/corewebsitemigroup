﻿using System.Collections.Generic;

namespace Enterbuy.Http.Exceptions
{
    public class UnprocessableEntityException : BaseCustomException
    {
        public UnprocessableEntityException(List<string> errors) : base(errors, System.Net.HttpStatusCode.UnprocessableEntity)
        {
        }

        public UnprocessableEntityException(string error) : base(new List<string>() { error }, System.Net.HttpStatusCode.UnprocessableEntity)
        {
        }
    }
}