﻿using System.Collections.Generic;

namespace Enterbuy.Http.Exceptions
{
    public class NotFoundException : BaseCustomException
    {
        public NotFoundException(string message = "Cannot find object") : base(new List<string>() { message }, System.Net.HttpStatusCode.NotFound)
        {
        }
    }
}