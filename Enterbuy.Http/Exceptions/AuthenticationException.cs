﻿using System.Collections.Generic;
using System.Net;

namespace Enterbuy.Http.Exceptions
{
    public class AuthenticationException : BaseCustomException
    {
        public AuthenticationException(string message) : base(new List<string>() { message }, HttpStatusCode.Unauthorized)
        {
        }
    }
}