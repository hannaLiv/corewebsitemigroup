﻿using Enterbuy.Http.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace Enterbuy.Http.MvcBehaviors
{
    public class CustomApiBehaviorOptions
    {
        public static Func<ActionContext, IActionResult> InvalidModelStateResponseFactory = (actionContext) =>
        {
            var errors = actionContext.ModelState
                .Where(e => e.Value.Errors.Count > 0)
                .SelectMany(e => e.Value.Errors)
                .Select(error => error.ErrorMessage)
                .ToList();
            var errorModel = new ErrorModel(System.Net.HttpStatusCode.BadRequest, errors);
            return new BadRequestObjectResult(errorModel);
        };
    }
}