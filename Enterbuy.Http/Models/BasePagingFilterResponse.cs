﻿namespace Enterbuy.Http.Models
{
    public class BasePagingFilterResponse
    {
        public PaginationModel Pagination { get; set; }
    }
}