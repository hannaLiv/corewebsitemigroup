﻿namespace Enterbuy.Http.Models
{
    public class PaginationModel
    {
        public int Count { get; set; }
        public int Page { get; set; }
        public int Limit { get; set; }
    }
}