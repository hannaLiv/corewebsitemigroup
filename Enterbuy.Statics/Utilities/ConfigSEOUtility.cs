﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Enterbuy.Statics.Utilities
{
    public static class ConfigSeoUtility
    {
        public static string ConfigTagMetaNoIndex(bool isNoIndex)
        {
            var r = "";
            if (isNoIndex)
            {
                r += "<meta name=\"robots\" content=\"noindex,nofollow,all\" />\r\n";
            }
            else
            {
                r += "<meta name=\"robots\" content=\"index,follow,all\" />\r\n";
            }
            return r;
        }
        public static string ConfigTagMetaCanonical(string domain, bool isCanonical, string valCanonical)
        {
            var r = "";
            if (isCanonical)
            {
                r += "<link rel=\"canonical\" href=\"" + valCanonical + "\" />\r\n";
            }
            else
            {
                r += "<link rel=\"canonical\" href=\"" + domain + "\" />\r\n";
            }
            return r;
        }
        public static string ConfigMeta(string title, string description, string keyword, string valcarial)
        {
            var r = "";
            r += "<title>" + title + "</title>";
            r += "<meta name=\"description\" content=\"" + description + "\">";
            r += "<meta name=\"keywords\" content=\"" + keyword + "\">";
            //r += "<link href=\"" + carial + "\" rel=\"canonical\" type=\"text/html\">";
            return r;
        }

        public static string ConfigShemaOrg(string domain_full, string title, string avatar, DateTime createdDate, DateTime modifiedDate, string metaTitle, string web_name, string logo, string description)
        {
            var r = "";
            r += "<script type=\"application/ld+json\">";
            r += "{";
            r += "\"@context\":\"http://schema.org\",";
            r += "\"@type\":\"Article\",";
            r += "\"mainEntityOfPage\":{\"@type\":\"WebPage\",\"@id\":\"" + domain_full + "\"},";
            r += "\"headline\":\"" + title + "\",";
            r += "\"image\":{\"@type\":\"ImageObject\",\"url\":\"" + avatar + "\",\"height\":600,\"width\":800},";
            r += "\"datePublished\":\"" + createdDate + "\",";
            r += "\"dateModified\":\"" + modifiedDate + "\",";
            r += "\"author\":{  \"@type\":\"Person\",\"name\":\"" + web_name + "\"},";
            r += "\"publisher\":{\"@type\":\"Organization\",\"name\":\"" + web_name + "\",\"logo\":{\"@type\":\"ImageObject\",\"url\":\"" + logo + "\",\"width\":35,\"height\":34}},";
            r += "\"description\":\"" + description + "\"";
            r += "}";
            r += "</script>";
            return r;
        }

        public static string ConfigSocialMeta(string fbAppId = "", string url = "", string title = "", string description = "", string image = "")
        {
            var r = "";
            r += "<meta property=\"fb:app_id\" content=\"" + fbAppId + "\" />\r\n";
            r += "<meta property=\"og:url\" content=\"" + url + "\" />\r\n";
            r += "<meta property=\"og:type\" content=\"article\" />\r\n";
            r += "<meta property=\"og:title\" content=\"" + title + "\" />\r\n";
            r += "<meta property=\"og:description\" content=\"" + description + "\" />\r\n";
            r += "<meta property=\"og:image\" content=\"" + image + "\" />\r\n";
            return r;
        }
        public static string ConfigWebsite(string domain, string page_name)
        {
            var r = "";
            r += "<meta name=\"viewport\" content=\"width = device-width, initial-scale = 1\">\r\n";
            r += "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\r\n";
            r += "<meta name=\"theme-color\" content=\"#081b62\" />\r\n";
            r += "<meta name=\"msapplication-navbutton-color\" content=\"#081b62\">\r\n";
            r += "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\r\n";
            r += "<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\r\n";
            r += "<meta http-equiv=\"Content-Language\" content=\"vi\" />\r\n";
          
            r += "<meta http-equiv=\"audience\" content=\"general\" />\r\n";
            r += "<meta name=\"resource-type\" content=\"document\" />\r\n";
            r += "<meta name=\"abstract\" content=\"" + page_name + "\" />\r\n";
            r += "<meta name=\"classification\" content=\"" + page_name + "\" />\r\n";
            r += "<meta name=\"area\" content=\"" + page_name + "\" />\r\n";
            r += "<meta name=\"placename\" content=\"Việt Nam\" />\r\n";
            r += "<meta name=\"author\" content=\"" + domain + "\" />\r\n";
            r += "<meta name=\"copyright\" content=\"©2020" + domain + "\" />\r\n";
            r += "<meta name=\"owner\" content=\"" + domain + "\" />\r\n";
            r += "<meta name=\"distribution\" content=\"Global\" />\r\n";
            r += "<link rel=\"alternate\" href=\"" + domain + "\" />\r\n";
            //r += "<link rel=\"canonical\" href=\"" + domain + "\" />\r\n";
            return r;
        }

        public static string ToUrlSlug(string value)
        {
            value = RemoveUnicode(value);
            //First to lower case 
            value = value.ToLowerInvariant();

            //Remove all accents
            var bytes = Encoding.GetEncoding("Cyrillic").GetBytes(value);

            value = Encoding.ASCII.GetString(bytes);

            //Replace spaces 
            value = Regex.Replace(value, @"\s", "-", RegexOptions.Compiled);

            //Remove invalid chars 
            value = Regex.Replace(value, @"[^\w\s\p{Pd}]", "", RegexOptions.Compiled);

            //Trim dashes from end 
            value = value.Trim('-', '_');

            //Replace double occurences of - or \_ 
            value = Regex.Replace(value, @"([-_]){2,}", "$1", RegexOptions.Compiled);

            return value;
        }

        public static string RemoveUnicode(string text)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                "đ",
                "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
                "í","ì","ỉ","ĩ","ị",
                "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
                "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
                "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
                "d",
                "e","e","e","e","e","e","e","e","e","e","e",
                "i","i","i","i","i",
                "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
                "u","u","u","u","u","u","u","u","u","u","u",
                "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return text;
        }
    }
}
