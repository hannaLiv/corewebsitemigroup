﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Enterbuy.Services.AgentRegistration.Components
{
    public class BannerInAgentRegistrationViewComponent : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View();
        }
    }
}