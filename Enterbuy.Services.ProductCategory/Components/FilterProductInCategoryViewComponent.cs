﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.ProductCategory.Components
{
    public class FilterProductInCategoryViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;
        private readonly IManufactureDao _manufactureDao;

        public FilterProductInCategoryViewComponent(IZoneDao zoneDao, IManufactureDao manufactureDao)
        {
            _zoneDao = zoneDao;
            _manufactureDao = manufactureDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(int zoneId, ZoneDetail zone)
        {
            ViewBag.ZoneDetail = zone;
            var result = await _manufactureDao.GetManufactures(Utils.Utility.DefaultLang, zone.ManufacturerId);
            return View(result);
        }
    }
}