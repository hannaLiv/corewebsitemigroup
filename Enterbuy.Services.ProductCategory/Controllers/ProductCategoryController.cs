﻿using System;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.ProductCategory.Controllers
{
    public class ProductCategoryController : BaseController
    {
        private readonly IZoneDao _zoneDao;
        private readonly IConfigDao _config;
        public ProductCategoryController(IZoneDao zoneDao, IConfigDao config)
        {
            _zoneDao = zoneDao;
            _config = config;
        }
        [Route("lien-he/{alias}")]
        [Route("thong-tin-enterbuy/{alias}")]
        public IActionResult RedirectActionOld(string alias, int? pageIndex, int? pageSize)
        {
            bool redrect = false;
            var data = HttpContext.Request.Path.Value;
            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                        redrect = true;
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                        redrect = true;
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            if (!redrect)
            {
                return NotFound();
            }
            else
            {
                return Content("");
            }
        }

       

        [Route("{alias}")]
        [Route("{alias}.html")]
        public async Task<IActionResult> Index(string alias, int? pageIndex, int? pageSize)
        {
            ViewData["IsExpand"] = false;
            bool redrect = false;
            var data = HttpContext.Request.Path.Value;
            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                        redrect = true;
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                        redrect = true;
                    }
                }
            }
            if (!redrect)
            {
                pageIndex = pageIndex ?? 1;
                pageSize = pageSize ?? 10;
                var alsplit = alias.Split('.');

                var zoneTarget = await _zoneDao.GetZoneByAlias(alsplit[0].ToString(), Utils.Utility.DefaultLang);
                if (zoneTarget != null)
                {
                    if (zoneTarget.Type != 1)
                    {
                        HttpContext.Response.Redirect(Utils.BaseBA.UrlCategoryNews(zoneTarget.Url), true);
                    }
                    else
                    {
                        if (alsplit[0].ToString().Equals(zoneTarget.Url) && data.Contains(".html"))
                        {
                            ViewBag.ZoneId = zoneTarget.Id;
                            ViewBag.Type = zoneTarget.Type;
                            ViewBag.Parent = zoneTarget.ParentId;
                            ViewBag.MetaCanonical = zoneTarget.MetaCanonical;
                            ViewBag.MetaNoIndex = zoneTarget.MetaNoIndex;
                            ViewBag.PageIndex = pageIndex;
                            ViewBag.PageSize = pageSize;
                        }
                        else
                        {

                            HttpContext.Response.Redirect(Utils.BaseBA.UrlCategory(zoneTarget.Url), true);
                        }
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            return View();
        }
    }
}