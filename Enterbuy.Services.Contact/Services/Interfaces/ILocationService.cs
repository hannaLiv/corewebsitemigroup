﻿using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Contact.Services.Interfaces
{
    public interface ILocationService
    {
        List<StoreResponse> GetNearLocation(float Longitude, float Latitude, int distance, string langCode, int sortOrder, out int totalRows);
    }
}
