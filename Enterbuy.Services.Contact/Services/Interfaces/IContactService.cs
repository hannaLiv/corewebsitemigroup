﻿using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Contact.Services
{
    public interface IContactService
    {
        int Insert(ContactViewModel contactViewModel);
    }
}
