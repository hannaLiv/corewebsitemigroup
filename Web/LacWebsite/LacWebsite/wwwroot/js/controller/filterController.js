﻿R.Filter = {
    Init: function () {
        R.Filter.PagePaging();
        R.Filter.RegisterEvent();
        R.Filter.BeforePaging = "";
    },
    RegisterEvent: function () {
        $('.tim-kiem-nhanh').keyup(function (e) {
            if (e.keyCode == 13) {
                var f = $(this).val();
                R.Filter.FilterResult(f);
            }
        });
        $('.tim-kiem-bai-viet').off('click').on('click', function () {
            var f = $(this).parent().parent().find('.tim-kiem-nhanh').val();
            R.Filter.FilterResult(f);
        })
        //
    },
    FilterResult: function (f) {
        
        //alert(f);
        var index = 1;
        var size = 10;
        var url = "/FilterArticle/FilterByKeyword?filter=" + f + "&index=" + index + "&size=" + size + "";
        window.location.href = url;
    },
    PagePaging: function (isAjax, url, params, binding) {
        if (isAjax == false) {
            if ($('.pagin').length > 0) {
                var total = parseInt($('.paging-container').data('total'));
                var size = parseInt($('.paging-container').data('size'));
                var current = parseInt($('.paging-container').data('currentpage'));
                var arr = [];
                for (var i = 0; i < total; i++) {
                    arr.push(i);
                }
                $('.pagin').pagination({
                    dataSource: arr,
                    pageSize: size,
                    showPrevious: true,
                    showNext: true,
                    pageNumber: current,
                    afterPageOnClick: function () {
                        var el = $('.paginationjs-page.active');
                        ////console.log(el.text());
                        var page = el.text();
                        var url = window.location.pathname;
                        var a = url.split('/');
                        R.Filter.BeforePaging = "/" + a[1];
                        url = R.Extra.BeforePaging + '?index=/' + page + '&?size=10';
                        //console.log(url);
                        window.location.href = url;
                    },
                    afterPreviousOnClick: function () {
                        var el = $('.paginationjs-page.active');
                        ////console.log(el.text());
                        var page = el.text();

                        var url = window.location.pathname;
                        var a = url.split('/');
                        R.Filter.BeforePaging = "/" + a[1];
                        url = R.Extra.BeforePaging + '?index=/' + page + '&?size=10';
                        //console.log(url);
                        window.location.href = url;
                    },
                    afterNextOnClick: function () {
                        var el = $('.paginationjs-page.active');
                        ////console.log(el.text());
                        var page = el.text();
                        var url = window.location.pathname;
                        var a = url.split('/');
                        R.Filter.BeforePaging = "/" + a[1];
                        url = R.Extra.BeforePaging + '?index=/' + page + '&?size=10';
                        //console.log(url);
                        window.location.href = url;
                    },
                    callback: function (data, pagination) {
                        $('.pagin').find('ul').first().addClass('number-page');
                        $('.pagin').find('a').each(function (element) {
                            //nb-txt nb-hover
                            $(this).addClass('nb-txt');
                            $(this).addClass('nb-hover');

                        })
                    }
                })
            }
        }
        if (isAjax == true) {
            if ($('.pagin').length > 0) {
                var total = parseInt($('.paging-container').data('total'));
                var size = parseInt($('.paging-container').data('size'));
                var current = parseInt($('.paging-container').data('currentpage'));
                var arr = [];
                for (var i = 0; i < total; i++) {
                    arr.push(i);
                }
                $('.pagin').pagination({
                    dataSource: arr,
                    pageSize: size,
                    showPrevious: true,
                    showNext: true,
                    pageNumber: current,
                    afterPageOnClick: function () {
                        var el = $('.paginationjs-page.active');
                        ////console.log(el.text());
                        var page = el.text();
                        params.pageNumber = parseInt(page)
                        $.post(url, params, function (response) {
                            console.log(response);
                            binding.html('').html(response);
                            slidermenu();
                            R.Extra.BindingExtraToProduct();
                            R.Product.BindingTotal();
                            R.Product.RegisterEvent();
                        })
                    },
                    afterPreviousOnClick: function () {
                        var el = $('.paginationjs-page.active');
                        ////console.log(el.text());
                        var page = el.text();
                        params.pageNumber = parseInt(page)
                        $.post(url, params, function (response) {
                            console.log(response);
                            binding.html('').html(response);
                            slidermenu();
                            R.Extra.BindingExtraToProduct();
                            R.Product.BindingTotal();
                            R.Product.RegisterEvent();
                        })
                    },
                    afterNextOnClick: function () {
                        var el = $('.paginationjs-page.active');
                        ////console.log(el.text());
                        var page = el.text();
                        params.pageNumber = parseInt(page)
                        $.post(url, params, function (response) {
                            console.log(response);
                            binding.html('').html(response);
                            slidermenu();
                            R.Extra.BindingExtraToProduct();
                            R.Product.BindingTotal();
                            R.Product.RegisterEvent();
                        })
                    },
                    callback: function (data, pagination) {
                        $('.pagin').find('ul').first().addClass('number-page');
                        $('.pagin').find('a').each(function (element) {
                            //nb-txt nb-hover
                            $(this).addClass('nb-txt');
                            $(this).addClass('nb-hover');

                        })
                    }
                })
            }
        }

    }
}

R.Filter.Init();