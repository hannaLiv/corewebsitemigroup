        // ====================Scroll to top========================
        // jQuery(document).ready(function($) {

        //     var visible = false;
        //     //Check to see if the window is top if not then display button
        //     $(window).scroll(function() {
        //         var scrollTop = $(this).scrollTop();
        //         if (!visible && scrollTop > 100) {
        //             $(".scrollToTop").fadeIn();
        //             visible = true;
        //         } else if (visible && scrollTop <= 100) {
        //             $(".scrollToTop").fadeOut();
        //             visible = false;
        //         }
        //     });
        //     //Click event to scroll to top
        //     $(".scrollToTop").click(function() {
        //         $("html, body").animate({
        //             scrollTop: 0
        //         }, 800);
        //         return false;
        //     });
        // });
        // ===============type it=====================
        // new TypeIt('.toppic', {
        //     speed: 60,
        //     breakLines: false,
        //     waitUntilVisible: true,
        //     lifeLike: true,
        //     cursor: false,

        // }).go();
        // $(".scrollToTop").click(function() {
        //     $("html, body").animate({
        //         scrollTop: 0
        //     }, 800);
        //     return false;
        // });

        // <!-- ------------------wow----------- -->
        // new WOW({
        //     boxClass: 'wow',
        //     mobile: false
        // }).init();
        // =============SLide=Top===================
        $(document).ready(function() {
            $('.slide').slick({
                autoplay: true,
                autoplaySpeed: 3000,
                arrows: false
            });

            $(".item-dot").click(function() {
                let slideIndex = $(this).attr('data-i');
                $('.slide').slick('slickGoTo', slideIndex);
                $('.item-dot').removeClass('acv');
                $('.item-dot').children('.cycle').removeClass('aatt');
                $(this).children('.cycle').addClass('aatt')
                $(this).addClass('acv')
            });

            $('.slide').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
                if (currentSlide === parseInt(currentSlide, 10)) {
                    $('.item-dot').removeClass('acv');
                    $('.item-dot').children('.cycle').removeClass('aatt');
                    $('.item-dot[data-i=' + currentSlide + ']').addClass('acv');
                    $('.item-dot[data-i=' + currentSlide + ']').children('.cycle').addClass('aatt');
                };
            });
            $('.slide-next').click(function() {
                $(".slide").slick('slickNext');
            });
            $('.slide-pre').click(function() {
                $(".slide").slick('slickPrev');
            });
        });
        // ==============Slide=Customer=============
        $(document).ready(function() {
            $('.slide-customer').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 2000,
                arrows: false,
                infinite: true,
                dots: false,

                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }]
            });
            $('.slide-next').click(function() {
                $(".slide-customer").slick('slickNext');
            });
            $('.slide-pre').click(function() {
                $(".slide-customer").slick('slickPrev');
            });
        });

        // --------------------mau-js----------------
        $(document).ready(function() {
            $(window).scroll(function() {
                // sticky navbar on scroll script
                if (this.scrollY > 20) {
                    $('.navbar').addClass("sticky");
                } else {
                    $('.navbar').removeClass("sticky");
                }

                // scroll-up button show/hide script
                if (this.scrollY > 500) {
                    $('.scroll-up-btn').addClass("show");
                } else {
                    $('.scroll-up-btn').removeClass("show");
                }
            });

            // slide-up script
            $('.scroll-up-btn').click(function() {
                $('html').animate({ scrollTop: 0 });
                // removing smooth scroll on slide-up button click
                $('html').css("scrollBehavior", "auto");
            });

            $('.navbar .menu li a').click(function() {
                // applying again smooth scroll on menu items click
                $('html').css("scrollBehavior", "smooth");
            });

            // toggle menu/navbar script
            $('.menu-btn').click(function() {
                $('.navbar .menu').toggleClass("active");
                $('.menu-btn i').toggleClass("active");
            });

            // typing text animation script
            var typed = new Typed(".typing", {
                strings: ["YouTuber", "Developer", "Blogger", "Designer", "Freelancer"],
                typeSpeed: 100,
                backSpeed: 60,
                loop: true
            });

            var typed = new Typed(".typing-2", {
                strings: ["YouTuber", "Developer", "Blogger", "Designer", "Freelancer"],
                typeSpeed: 100,
                backSpeed: 60,
                loop: true
            });

            // owl carousel script
            $('.carousel').owlCarousel({
                margin: 20,
                loop: true,
                autoplayTimeOut: 2000,
                autoplayHoverPause: true,
                responsive: {
                    0: {
                        items: 1,
                        nav: false
                    },
                    600: {
                        items: 2,
                        nav: false
                    },
                    1000: {
                        items: 3,
                        nav: false
                    }
                }
            });
        });
        // ==============
        $(document).ready(function() {
            $('.slide-room-detail').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.img-thumb'
            });
            $('.img-thumb').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: '.slide-room-detail',
                dots: false,
                arrows: false,
                centerMode: true,
                focusOnSelect: true,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }]

            });
            $('.next-thumb').click(function() {
                $(".slide-room-detail").slick('slickNext');
            });
            $('.pre-thumb').click(function() {
                $(".slide-room-detail").slick('slickPrev');
            });
            $('.next-thumb').click(function() {
                $(".img-thumb").slick('slickNext');
            });
            $('.pre-thumb').click(function() {
                $(".img-thumb").slick('slickPrev');
            });
        });
        // ======Home=full-pages=======
        //using document ready...
        $(document).ready(function() {

            //initialising fullpage.js in the jQuery way
            $('#fullpage').fullpage({
                sectionsColor: ['#ffffff', '#ffffff', '#0e0e2d1a', '#ffffff', '#0a0a271a', '#0e0e2d', '#ffffff'],
                navigation: true,
                slidesNavigation: true,
            });

            // calling fullpage.js methods using jQuery
            $('#moveSectionUp').click(function(e) {
                e.preventDefault();
                $.fn.fullpage.moveSectionUp();
            });

            $('#moveSectionDown').click(function(e) {
                e.preventDefault();
                $.fn.fullpage.moveSectionDown();
            });
        });