using LacWebsite.Utility;
using MI.Cache;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System.Threading.Tasks;

namespace LacWebsite.Middleware
{
    public static class MiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {

        }

        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();

        }
        public static IApplicationBuilder RedirectMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<RedirectMiddlewareConfiguration>();
        }
        public static IApplicationBuilder ClearCacheMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<CustomClearCacheMiddleware>();
        }
    }
    public class RedirectMiddlewareConfiguration
    {
        private readonly RequestDelegate _next;
        private readonly IRedirectHelper _helper;

        public RedirectMiddlewareConfiguration(IRedirectHelper helper, RequestDelegate next)
        {
            _helper = helper;
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var a = _helper.RedirectMiddleWare(httpContext);
            await _next(httpContext);
        }
    }
    public class CustomClearCacheMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _configuration;
        //cache 
        private readonly IDistributedCache _distributedCache;
        private readonly IConnectionMultiplexer _multiplexer;
        public CustomClearCacheMiddleware(RequestDelegate next, IConfiguration configuration, IDistributedCache distributedCache, IConnectionMultiplexer multiplexer)
        {
            _next = next;
            _configuration = configuration;
            _distributedCache = distributedCache;
            _multiplexer = multiplexer;
        }
        public async Task Invoke(HttpContext httpContext)
        {
            RedisUtils.DeleteAllCacheAsynForce(_multiplexer, _configuration);
            await _next(httpContext);
        }
    }
}
