﻿using LacWebsite.Services.Product.Repository;
using LacWebsite.Services.Zone.Repository;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LacWebsite.ViewComponents
{
    public class ProductListProviderViewComponent : ViewComponent
    {
        private readonly IProductRepository _productRepository;
        private readonly IZoneRepository _zoneRepository;
        private readonly IStringLocalizer<ProductListProviderViewComponent> _localizer;
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        public ProductListProviderViewComponent(IProductRepository productRepository, IStringLocalizer<ProductListProviderViewComponent> localizer, IZoneRepository zoneRepository)
        {
            _localizer = localizer;
            _productRepository = productRepository;
            _zoneRepository = zoneRepository;
        }
        public IViewComponentResult Invoke(int zone_id)
        {
            //var total_row = 0;
            var model = _zoneRepository.GetZoneDetail(zone_id, CurrentLanguageCode);
            //ViewBag.Total = total_row;
            return View(model);
        }
    }
}
