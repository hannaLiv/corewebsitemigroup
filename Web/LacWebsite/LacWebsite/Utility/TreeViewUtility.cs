﻿using LacWebsite.ExecuteCommand;
using LacWebsite.Services.Zone.ViewModal;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utils;

namespace LacWebsite.Utility
{
    public interface ITreeviewHelper
    {
        string RenderTag(List<ZoneByTreeViewMinify> list, int level, int parentId);
        string RenderTagWithMaxLevel(List<ZoneByTreeViewMinify> list, int level, int parentId, int maxLevel);
        string ReturnHTMLTree(List<ZoneByTreeViewMinify> list, int parentId);
    }

    public class TreeviewHelper : ITreeviewHelper
    {
        private readonly IConfiguration _configuration;
        private readonly IExecuters _executers;
        //cache 
        private readonly IDistributedCache _distributedCache;

        private readonly IConnectionMultiplexer _multiplexer;
        public TreeviewHelper(IConfiguration configuration, IExecuters executers, IDistributedCache distributedCache, IConnectionMultiplexer multiplexer)
        {
            _configuration = configuration;
            _executers = executers;
            _distributedCache = distributedCache;
            _multiplexer = multiplexer;
        }

        /*
         var keyCache = string.Format("banner_{0}_{1}", code, langCode);
            var r = new BannerAdsViewModel();
            //Get in cache
            var result_after_cache = _distributedCache.Get(keyCache);
            if (result_after_cache != null)
            {
                r = Newtonsoft.Json.JsonConvert.DeserializeObject<BannerAdsViewModel>(Encoding.UTF8.GetString(result_after_cache));
            }
            if (result_after_cache == null)
            {
                var p = new DynamicParameters();
                var commandText = "usp_Web_Get_BannerAds_By_Code";
                p.Add("@langCode", langCode);
                p.Add("@code", code);
                r = _executers.ExecuteCommand(_connStr, conn => conn.QueryFirstOrDefault<BannerAdsViewModel>(commandText, p, commandType: System.Data.CommandType.StoredProcedure));
                //Add cache
                var add_to_cache = Newtonsoft.Json.JsonConvert.SerializeObject(r);
                result_after_cache = Encoding.UTF8.GetBytes(add_to_cache);
                var cache_options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(30)).SetAbsoluteExpiration(DateTime.Now.AddMinutes(int.Parse(_configuration["Redis:CachingExpireMinute"])));
                _distributedCache.Set(keyCache, result_after_cache, cache_options);
            }
            //var p = new DynamicParameters();
            //var commandText = "usp_Web_Get_BannerAds_By_Code";
            //p.Add("@langCode", langCode);
            //p.Add("@code", code);
            //var r = _executers.ExecuteCommand(_connStr, conn => conn.QueryFirstOrDefault<BannerAdsViewModel>(commandText, p, commandType: System.Data.CommandType.StoredProcedure));

            return r;
         
         */
        public string RenderTag(List<ZoneByTreeViewMinify> list, int level, int parentId)
        {

            if (list.Count > 0)
            {
                var result = "";
                var max_leaf = list.Max(r => r.level);
                var _af = list.Where(r => r.level >= level && r.ParentId == parentId).ToList();
                foreach (var item in _af)
                {
                    var link_tar = string.Format("/{0}", item.Url);
                    var p = item.Id;
                    var _tt1 = level + 1;
                    var _tt2 = level + 2;
                    result += "<li class=\"li-tree-lv-" + _tt1 + "\">";
                    result += string.Format("<span class=\"span-tree-node tree-lv-{2}\" data-url=\"{0}\" data-sp=" + 1 + ">{1}</span>", link_tar, UIHelper.TrimByWord(item.Name, 6, "..."), level + 1);
                    result += "<ul class=\"ul-tree-lv-" + _tt2 + "\">";
                    result += RenderTag(list, level + 1, p);
                    result += "</ul>";
                    result += "</li>";

                }
                return result;
            }
            return "";
        }
        public string RenderTagWithMaxLevel(List<ZoneByTreeViewMinify> list, int level, int parentId, int maxLevel)
        {
            if (list.Count > 0 && level <= maxLevel)
            {
                var result = "";
                var max_leaf = list.Max(r => r.level);
                var _af = list.Where(r => r.level >= level && r.ParentId == parentId).OrderBy(r => r.SortOrder).ToList();
                foreach (var item in _af)
                {
                    var link_tar = string.Format("/{0}", item.Url);
                    var p = item.Id;
                    var _tt1 = level + 1;
                    var _tt2 = level + 2;
                    result += "<li class=\"li-tree-lv-" + _tt1 + "\">";
                    result += string.Format("<a class=\"span-tree-node tree-lv-{2}\" href=\"{0}\" data-url=\"{0}\">{1}</a>", link_tar, UIHelper.TrimByWord(item.Name, 6, "..."), level + 1);
                    result += "<ul class=\"ul-tree-lv-" + _tt2 + "\">";
                    result += RenderTagWithMaxLevel(list, level + 1, p, maxLevel);
                    result += "</ul>";
                    result += "</li>";
                }
                return result;
            }
            return "";
        }

        public string ReturnHTMLTree(List<ZoneByTreeViewMinify> list, int parentId)
        {
            var result = "";
            try
            {
                result += RenderTag(list, 0, parentId);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
            }

            return result;
        }
    }

    public class Test
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Root { get; set; }
        public int Level { get; set; }
        public int ParentId { get; set; }
    }
}
