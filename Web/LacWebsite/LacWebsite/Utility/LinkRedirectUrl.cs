﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LacWebsite.Utility
{
    public static class LinkRedirectUrl
    {
        public static string LinkProductRedirect(string alias)
        {
            return string.Format("/{0}.html", alias);
        }
        public static string LinkArticleRedirect(string alias)
        {
            return string.Format("/{0}.html", alias);
        }
        public static string LinkZoneRedirect(string alias)
        {
            return string.Format("/{0}", alias);
        }
    }
}
