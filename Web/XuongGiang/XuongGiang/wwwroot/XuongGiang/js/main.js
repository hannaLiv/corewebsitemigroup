// ====================Scroll to top========================
jQuery(document).ready(function($) {
    var visible = false;
    //Check to see if the window is top if not then display button
    $(window).scroll(function() {
        var scrollTop = $(this).scrollTop();
        if (!visible && scrollTop > 100) {
            $(".scrollToTop").fadeIn();
            visible = true;
        } else if (visible && scrollTop <= 100) {
            $(".scrollToTop").fadeOut();
            visible = false;
        }
    });
    //Click event to scroll to top
    $(".scrollToTop").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});
// ==============Slide=Customer=============
$(document).ready(function() {
    $('.slide-bangkhen').slick({

        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        infinite: true,
        dots: false,

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.slide-next1').click(function() {
        $(".slide-bangkhen").slick('slickNext');
    });
    $('.slide-pre1').click(function() {
        $(".slide-bangkhen").slick('slickPrev');
    });
});
// ==============Slide=prod-detail=============
$(document).ready(function() {
    $('.slide-img-prod').slick({

        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        infinite: true,
        dots: false,

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.slide-next2').click(function() {
        $(".slide-img-prod").slick('slickNext');
    });
    $('.slide-pre2').click(function() {
        $(".slide-img-prod").slick('slickPrev');
    });
});