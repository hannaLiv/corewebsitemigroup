﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XuongGiang.Services.Article.Repository;
using XuongGiang.Services.Product.Repository;
using XuongGiang.Services.Zone.Repository;
using Microsoft.AspNetCore.Mvc;

namespace XuongGiang.Controllers
{
    public class FilterArticleController : BaseController
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IProductRepository _productRepository;
        private readonly IArticleRepository _articleRepository;

        public FilterArticleController(IZoneRepository zoneRepository, IProductRepository productRepository, IArticleRepository articleRepository)
        {
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
            _articleRepository = articleRepository;

        }
        public IActionResult FilterArticleByTag(string tag, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            var total = 0;
            var model = _articleRepository.GetArticlesSameTag(tag, CurrentLanguageCode, pageIndex, pageSize, out total);
            ViewBag.Total = total;
            ViewBag.Tag = tag;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            return View(model);
        }

        public IActionResult FilterByKeyword(string filter, int? index, int? size)
        {
            index = index ?? 1;
            size = size ?? 10;
            var total = 0;
            var model = _articleRepository.GetArticlesByFilterKeywordt(filter, CurrentLanguageCode, index.Value, size.Value, out total);
            ViewBag.Key = filter;
            ViewBag.Total = total;
            ViewBag.Index = index.Value;
            ViewBag.Size = size.Value;
            return View(model);
        }
        
    }
}