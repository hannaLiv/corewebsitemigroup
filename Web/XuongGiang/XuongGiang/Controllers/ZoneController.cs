﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using XuongGiang.Services.Article.Repository;
using XuongGiang.Services.Extra.Repository;
using XuongGiang.Services.Product.Repository;
using XuongGiang.Services.Zone.Repository;
using XuongGiang.Services.Zone.ViewModal;
using Microsoft.AspNetCore.Mvc;
using MI.Entity.Enums;

namespace XuongGiang.Controllers
{
    public class ZoneController : BaseController
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IProductRepository _productRepository;
        private readonly IExtraRepository _extratRepository;
        private readonly IArticleRepository _articleRepository;
        //private readonly IStringLocalizer<HomeController> _localizer;

        public ZoneController(IZoneRepository zoneRepository, IProductRepository productRepository, IExtraRepository extraRepository, IArticleRepository articleRepository)
        {
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
            _extratRepository = extraRepository;
            _articleRepository = articleRepository;
        }

        public IActionResult RedirectAction(string alias, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 12;
            var isAll = false;
            if (alias == "blog" || alias == "bai-viet-moi")
                isAll = true;
            var zone_tar = _zoneRepository.GetZoneByAlias(alias, CurrentLanguageCode);
            if (zone_tar != null || isAll)
            {
                ViewBag.ZoneId = isAll ? 0 : zone_tar.Id;
                ViewBag.Type = isAll ? (int)TypeZone.Article :  zone_tar.Type;
                ViewBag.Parent = isAll ? 0 : zone_tar.ParentId;
                ViewBag.PageIndex = pageIndex;
                ViewBag.PageSize = pageSize;
                ViewBag.IsHaveChild = isAll ? 0 : zone_tar.isHaveChild;
                return View();
            }
            return View("~/Views/P404/P404.cshtml");
        }


        public List<ZoneSugget> GetZoneSugget()
        {
            var zone_tar = _zoneRepository.GetZoneSugget(CurrentLanguageCode);
            return zone_tar;
        }

    }
}