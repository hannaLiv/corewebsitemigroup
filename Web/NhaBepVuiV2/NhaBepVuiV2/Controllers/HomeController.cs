﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NhaBepVuiV2.Models;
using NhaBepVuiV2.Services.Zone.Repository;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using NhaBepVuiV2.Services.Locations.Repository;
using NhaBepVuiV2.Utility;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using Utils;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Net.Http;

namespace NhaBepVuiV2.Controllers
{
    public class HomeController : BaseController
    {
        private IHostingEnvironment _env;
        private readonly IZoneRepository _zoneRepository;
        private readonly ILocationsRepository _locationsRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICookieUtility _cookieUtility;
        private readonly IActionContextAccessor _accessor;
        public HomeController(IHostingEnvironment envrnmt, IZoneRepository zoneRepository, ILocationsRepository locationsRepository, IHttpContextAccessor httpContextAccessor, ICookieUtility cookieUtility, IActionContextAccessor accessor)
        {
            _zoneRepository = zoneRepository;
            _locationsRepository = locationsRepository;
            _cookieUtility = cookieUtility;
            _httpContextAccessor = httpContextAccessor;
            _env = envrnmt;
            _accessor = accessor;

        }
        [HttpPost]
        public IActionResult SetLanguage(string culture)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return RedirectToAction("IndexPublic");
        }
        public ActionResult RedirectToDefaultCulture()
        {
            var culture = CurrentLanguage;
            if (string.IsNullOrEmpty(culture))
                culture = "vi";

            return RedirectToAction("IndexPublic");
        }
        public IActionResult Index()
        {


            return View();
        }

        public IActionResult IndexPublic()
        {
            //Console.WriteLine(customer_ip);
            //GetIpValue(out customer_ip);
            //Lay location from IP

            //Kiem tra co ton tai cookie location_id khong
            CookieOptions cookie = new CookieOptions()
            {
                Path = "/",
                HttpOnly = false,
                Secure = false,
                IsEssential = true
            };
            if (Request.Cookies[CookieLocationId] == null)
            {
                //Lay IP
                var location_default = _locationsRepository.GetLocationFirst(CurrentLanguageCode);
                if (location_default != null)
                {
                    cookie.Expires = DateTime.Now.AddDays(7);
                    _httpContextAccessor.HttpContext.Response.Cookies.Append(CookieLocationId, location_default.Id.ToString(), cookie);
                    _httpContextAccessor.HttpContext.Response.Cookies.Append(CookieLocationName, location_default.Name, cookie);
                    var result = new Utility.CookieLocation() { LocationId = location_default.Id, LocationName = location_default.Name };
                }
            }
            return View();
        }

        [HttpPost]
        public IActionResult SwitchRegion(int region_id)
        {
            return ViewComponent("SwitchRegion", new { region_id = region_id });
        }
        [HttpPost]
        public IActionResult ViewMoreRegion(int zone_parent_id, int locationId, int skip, int size)
        {
            return ViewComponent("ViewMoreRegion", new { zone_parent_id = zone_parent_id, locationId = locationId, skip = skip, size = size });
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("~/Views/P404/P404.cshtml");
            //return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
