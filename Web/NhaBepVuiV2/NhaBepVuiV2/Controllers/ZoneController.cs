﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NhaBepVuiV2.Services.Article.Repository;
using NhaBepVuiV2.Services.Extra.Repository;
using NhaBepVuiV2.Services.Product.Repository;
using NhaBepVuiV2.Services.Zone.Repository;
using NhaBepVuiV2.Services.Zone.ViewModal;
using Microsoft.AspNetCore.Mvc;

namespace NhaBepVuiV2.Controllers
{
    public class ZoneController : BaseController
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IProductRepository _productRepository;
        private readonly IExtraRepository _extratRepository;
        private readonly IArticleRepository _articleRepository;
        //private readonly IStringLocalizer<HomeController> _localizer;

        public ZoneController(IZoneRepository zoneRepository, IProductRepository productRepository, IExtraRepository extraRepository, IArticleRepository articleRepository)
        {
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
            _extratRepository = extraRepository;
            _articleRepository = articleRepository;
        }

        public IActionResult RedirectAction(string alias, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 12;

            var zone_tar = _zoneRepository.GetZoneByAlias(alias, CurrentLanguageCode);
            if (zone_tar != null)
            {
                ViewBag.ZoneId = zone_tar.Id;
                ViewBag.Type = zone_tar.Type;
                ViewBag.Parent = zone_tar.ParentId;
                ViewBag.PageIndex = pageIndex;
                ViewBag.PageSize = pageSize;
                ViewBag.IsHaveChild = zone_tar.isHaveChild;
                return View();
            }
            return View("~/Views/P404/P404.cshtml");
        }


        public List<ZoneSugget> GetZoneSugget()
        {
            var zone_tar = _zoneRepository.GetZoneSugget(CurrentLanguageCode);
            return zone_tar;
        }

    }
}