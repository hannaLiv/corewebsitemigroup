﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NhaBepVuiV2.Services.Article.Repository;
using NhaBepVuiV2.Services.Product.Repository;
using NhaBepVuiV2.Services.Zone.Repository;
using Microsoft.AspNetCore.Mvc;

namespace NhaBepVuiV2.Controllers
{
    public class CategoriesController : BaseController
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IProductRepository _productRepository;
        private readonly IArticleRepository _articleRepository;

        public CategoriesController(IZoneRepository zoneRepository, IProductRepository productRepository, IArticleRepository articleRepository)
        {
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
            _articleRepository = articleRepository;
        }

        public IActionResult CategoriesList()
        {
            return View();
        }

        public IActionResult CategoriesList1(int zoneId, int? pageIndex, int? pageSize)
        {
            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            ViewBag.PageIndex = pageIndex;
            ViewBag.PageSize = pageSize;
            ViewBag.CateId = zoneId;
            return View();
        }
    }
}