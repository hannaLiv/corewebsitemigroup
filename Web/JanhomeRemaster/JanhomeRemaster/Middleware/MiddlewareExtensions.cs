using JanhomeRemaster.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace JanhomeRemaster.Middleware
{
    public static class MiddlewareExtensions
    {
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {

        }

        public static void ConfigureCustomExceptionMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();

        }
        public static IApplicationBuilder RedirectMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<RedirectMiddlewareConfiguration>();
        }
    }
    public class RedirectMiddlewareConfiguration
    {
        private readonly RequestDelegate _next;
        private readonly IRedirectHelper _helper;

        public RedirectMiddlewareConfiguration(IRedirectHelper helper, RequestDelegate next)
        {
            _helper = helper;
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var a = _helper.RedirectMiddleWare(httpContext);
            await _next(httpContext);
        }
    }
}
