﻿using JanhomeRemaster.Services.Extra.Repository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JanhomeRemaster.Utility
{
    public interface IRedirectHelper
    {
        bool RedirectMiddleWare(HttpContext context);
    }
    public class RedirectHelper : IRedirectHelper
    {
        public IExtraRepository _repository;
        public RedirectHelper(IExtraRepository extraRepository)
        {
            _repository = extraRepository;
        }
        public bool RedirectMiddleWare(HttpContext context)
        {
            var path = context.Request.Path.ToString();
            var ls = _repository.GetListRedirectUrl();
            var a = ls.Where(r => r.UrlOld.Equals(path)).FirstOrDefault();
            if (a != null)
            {
                var b = false;
                if (a.UrlType == 301) b = true;
                if (a.UrlType == 302) b = false;
                context.Response.Redirect(a.UrlNew, b);
                return true;
            }

            return false;
        }
    }
}
