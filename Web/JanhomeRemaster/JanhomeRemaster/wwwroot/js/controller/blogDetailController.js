﻿R.BlogDetail = {
    Init: function () {
        R.BlogDetail.location_id = R.CurrentLocationId();
        R.BlogDetail.culture = R.Culture();
        R.BlogDetail.LoadListProduct();
        R.BlogDetail.AddViewCountArticle();
        R.BlogDetail.RegisterEvent();
        R.BlogDetail.FigureRender();
    },
    RegisterEvent: function () {
        $('.toc_title').off('click').on('click', function () {
            $('.toc_list').toggle();
        })

    },
    LoadListProduct: function () {

        $('product').each(function (element) {
            var el = $(this);
            var product_list = $(this).data('id-list');
            var params = {
                product_ids: product_list,
                location_id: R.BlogDetail.location_id
            }
            var url = R.BlogDetail.culture + "/Blog/ProductsInArticle";
            $.post(url, params, function (response) {
                console.log(response);
                el.replaceWith(response);
                R.Extra.BindingExtraToProduct();
                R.BlogDetail.RegisterEvent();
            })
        })
    },
    AddViewCountArticle: function () {

        var id_san_pham = $('.detail-container').data('id');
        var url = "/Extra/CreateViewCount";
        var params = {
            objectId: id_san_pham,
            type: 'article'
        }
        $.post(url, params, function (response) {
            console.log(response);
        })

    },
    FigureRender: function () {
        var dt = $('.detail-container');
        var checker = dt.data('isnewrender');
        var id = dt.attr('id');
        console.log(checker);
        if (checker == "True") {
            var count = 1;
            var imgs = $('.detail-container').find('p img').each(function (element) {
                var item = $(this);
                //class="lazy size-full wp-image-89341 cust-ag"
                item.addClass('lazy');
                item.addClass('size-full');
                item.addClass('cust-ag');
                $(this).attr('id', 'img-' + count);
                var original = document.getElementById('img-' + count).outerHTML;
                var _alt = item.attr('alt');
                var htm = "";
                htm += "<figure class=\"wp-caption alignnone\">";
                htm += original;
                htm += "<figcaption class=\"wp-caption-text\">";
                htm += "<em>" + _alt + "</em>";
                htm += "</figcaption>";
                htm += "</figure>";
                console.log(htm);
                item.closest('p').replaceWith(htm);
                count++;
            })
        }
    }
}



$(function () {
    R.BlogDetail.Init()
})