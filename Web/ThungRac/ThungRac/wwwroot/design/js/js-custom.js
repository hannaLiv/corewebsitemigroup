// Slide Home
var js_slide_home = new Swiper(".js-slide-home", {
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    pagination: {
        el: ".swiper-pagination",
    },
});



var swiper = new Swiper('.slide-product-detail .swiper-container.gallery-top', {

    slidesPerView: 1,
    loop: true,
    autoplay: false,
    navigation: {
        nextEl: '.swiper-container .swiper-button-next',
        prevEl: '.swiper-container .swiper-button-prev',
    }
});



var swiper2 = new Swiper('.slide-product-detail-2 .swiper-container.gallery-top-2', {

    slidesPerView: 1,
    loop: true,
    autoplay: false,
    navigation: {
        nextEl: '.swiper-container .swiper-button-next',
        prevEl: '.swiper-container .swiper-button-prev',
    }
});



// Banner Home
var js_banner_home = new Swiper(".js-banner-home", {
    slidesPerView: 2,
    spaceBetween: 0,
    breakpoints: {
        640: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 1,
        },
        1024: {
            slidesPerView: 2,
        },
    },
});

// customer
var js_customer = new Swiper(".js-customer", {
    slidesPerView: 5,
    spaceBetween: 10,
    lazy: true,

    pagination: {
        el: ".swiper-pagination",
    },
    breakpoints: {
        640: {
            slidesPerView: 4,
        },
        768: {
            slidesPerView: 4,
        },
        1024: {
            slidesPerView: 4,
        },
    },
});

// partner
var js_partner = new Swiper(".js-partner", {
    slidesPerView: 4,
    spaceBetween: 15,
    lazy: true,
    autoplay: {
        delay: 4000,
    },
    pagination: {
        el: ".swiper-pagination",
    },
    breakpoints: {
        640: {
            slidesPerView: 3,
        },
        768: {
            slidesPerView: 3,
        },
        1024: {
            slidesPerView: 4,
        },
    },
});

// News mobile
var js_news_mobile = new Swiper(".js-news-mobile", {
    slidesPerView: "auto",
    spaceBetween: 30,
    lazy: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
});

// Scroll to top
$('#return-to-top').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 500);
});
