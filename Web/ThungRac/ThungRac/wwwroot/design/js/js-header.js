// Menu mobile
$(document).ready(function() {
  $('.js-menu-mobile').on('click', function() {
    $('.js-menu-mobile').addClass('open');
    $('.header__menu-mobile__ovelay').addClass('active');
  });
  $('.header__menu-mobile__ovelay').on('click', function() {
    $('.js-menu-mobile').removeClass('open');
    $('.header__menu-mobile__ovelay').removeClass('active');
  });
});

// Sub menu mobile
$('.header__menu-mobile__item').on('click', function() {
  $('.header__menu-mobile__sub-list.active').removeClass('active');
  $(this).toggleClass('active');
});
