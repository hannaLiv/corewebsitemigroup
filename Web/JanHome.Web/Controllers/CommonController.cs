﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JanHome.Web.Services.Zone.Repository;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace JanHome.Web.Controllers
{
    public class CommonController : Controller
    {
        private readonly IZoneRepository _zoneRepository;
        private readonly IStringLocalizer<HomeController> _localizer;
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        public CommonController(IZoneRepository zoneRepository, IStringLocalizer<HomeController> localizer)
        {
            _localizer = localizer;
            _zoneRepository = zoneRepository;
        }
        public PartialViewResult ZoneMenu() {
            var model = _zoneRepository.GetZoneByTreeViewMinifies(1, CurrentLanguage, 0);
            return PartialView(model);
        }
    }
}