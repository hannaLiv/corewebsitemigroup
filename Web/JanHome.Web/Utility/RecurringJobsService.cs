﻿using Hangfire;
using Hangfire.Annotations;
using Hangfire.Server;
using JanHome.Web.Services.FlashSale.Repository;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace JanHome.Web.Utility
{
    internal class RecurringJobsService : BackgroundService
    {
        private readonly IBackgroundJobClient _backgroundJobs;
        private readonly IRecurringJobManager _recurringJobs;
        private readonly ILogger<RecurringJobScheduler> _logger;
        private readonly IFlashSaleRepository _flashSale;

        public RecurringJobsService(
            [NotNull] IBackgroundJobClient backgroundJobs,
            [NotNull] IRecurringJobManager recurringJobs,
            [NotNull] ILogger<RecurringJobScheduler> logger,
            IFlashSaleRepository flashSale)
        {
            _backgroundJobs = backgroundJobs ?? throw new ArgumentNullException(nameof(backgroundJobs));
            _recurringJobs = recurringJobs ?? throw new ArgumentNullException(nameof(recurringJobs));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _flashSale = flashSale;
        }

        //public IRecurringJobManager RecurringJobs => _recurringJobs;

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                //_backgroundJobs.Enqueue<Services>(x => x.LongRunning(JobCancellationToken.Null));
                //_backgroundJobs.Enqueue(() => )
                _recurringJobs.RemoveIfExists("minutely");
                _recurringJobs.AddOrUpdate("hourly", () => _flashSale.AutoUpdateFlashSale(), Cron.Hourly);
                //_recurringJobs.AddOrUpdate("minutely", () => WebHelper.ImmotalCMS(), Cron.Minutely);
                

            }
            catch (Exception e)
            {
                _logger.LogError("An exception occurred while creating recurring jobs.", e);
            }

            return Task.CompletedTask;
        }
    }
}
