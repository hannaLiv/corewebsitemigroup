﻿using JanHome.Web.Services.BannerAds.Repository;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Text;

namespace JanHome.Web.Cache.CachePage
{
    public class HomePage
    {
        public string Slide { get; set; }
        public string Region { get; set; }
        public string Video { get; set; }
        public string TuVan { get; set; }


        public HomePage()
        {
            this.Slide = string.Empty;
            this.Region = string.Empty;
            this.Video = string.Empty;
            this.TuVan = string.Empty;
        }
    }

    public class CacheHome
    {
        IDistributedCache _distributedCache;
        public CacheHome(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public void LoadCache(string languageCode)
        {
            string key = $"Home_{languageCode}";

            var Home = new HomePage();
            var resultCache = _distributedCache.Get(key);
            if (resultCache != null)
            {
                Home = Newtonsoft.Json.JsonConvert.DeserializeObject<HomePage>(Encoding.UTF8.GetString(resultCache));
            }
            else
            {

            }
        }
        private string BuildSlideHome()
        {
            //StringBuilder sb = new StringBuilder();
            //sb.Append();


            return string.Empty;
        }

    }
}
