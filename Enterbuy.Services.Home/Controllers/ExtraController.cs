﻿using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;
using Utils;

namespace Enterbuy.Services.Home.Controllers
{
    public class ExtraController : BaseController
    {
        private readonly IBannerAdsDao _bannerAdsRepository;

        public ExtraController(IBannerAdsDao bannerAdsRepository)
        {
            _bannerAdsRepository = bannerAdsRepository;
        }
        [HttpPost]
        public IActionResult SendMail(string type, string body)
        {
            var list_mail_receivers = _bannerAdsRepository.GetConfigByName(Utils.Utility.DefaultLang, "MailManager");

            if (type == "order")
            {
                //var list_mail_receivers = _bannerAdsRepository.GetConfigByName(CurrentLanguageCode,"MailManager");
                if (list_mail_receivers != null)
                {
                    var x = list_mail_receivers.Split(",");
                    foreach (var item in x)
                        Email.Send(item, "THÔNG BÁO ĐƠN HÀNG", body, null);
                }
            }
            if (type == "comment")
            {
                if (list_mail_receivers != null)
                {
                    var x = list_mail_receivers.Split(",");
                    foreach (var item in x)
                        Email.Send(item, "THÔNG BÁO COMMENT", body, null);
                }
            }
            if (type == "rating")
            {
                if (list_mail_receivers != null)
                {
                    var x = list_mail_receivers.Split(",");
                    foreach (var item in x)
                        Email.Send(item, "THÔNG BÁO RATING", body, null);
                }
            }
            return Ok();
        }
    }
}