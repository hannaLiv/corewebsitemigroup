﻿using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Home.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IZoneDao _zoneDao;

        public HomeController(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }

        public async Task<IActionResult> Index()
        {
            var listZoneTypeRegion = await _zoneDao.GetZoneByTreeViewShowMenuMinifies((int)TypeZone.Region,"vi-VN", 0, 1);

            ViewData["IsExpand"] = true;
            return View(listZoneTypeRegion);
        }
        [HttpPost]
        public IActionResult SwitchRegion(int region_id)
        {
            
            return ViewComponent("SwitchRegion", new { region_id = region_id });
        }
        [HttpPost]
        public IActionResult ViewMoreRegion(int zone_parent_id, int locationId, int skip, int size)
        {
            return ViewComponent("ViewMoreRegion", new { zone_parent_id = zone_parent_id, locationId = locationId, skip = skip, size = size });
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}