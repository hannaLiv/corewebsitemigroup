﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Home.Services;
using Utils;

namespace Enterbuy.Services.Home.Components
{
    public class ThirdBannerViewComponent : ViewComponent
    {
        private readonly IBannerHomePageService _bannerHomePageService;

        public ThirdBannerViewComponent(IBannerHomePageService bannerHomePageService)
        {
            _bannerHomePageService = bannerHomePageService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var result = await _bannerHomePageService.BannerTrangchuNgang2("vi-VN");
            result.Image = UIHelper.StoreFilePath(result.Image, false);
            return View(result);
        }
    }
}