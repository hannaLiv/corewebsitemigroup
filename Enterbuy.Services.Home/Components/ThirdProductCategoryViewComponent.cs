﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.Home.Components
{
    public class ThirdProductCategoryViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneDao;

        public ThirdProductCategoryViewComponent(IZoneDao zoneDao)
        {
            _zoneDao = zoneDao;
        }

        public async Task<IViewComponentResult> InvokeAsync(List<ZoneByTreeViewMinify> listZoneTypeRegion)
        {
            var result = listZoneTypeRegion.Where(r => r.ParentId == 0).OrderBy(r => r.SortOrder).ToList();
            var resultListProductRegion = new List<ZoneByTreeViewMinify>();
            if (result.Count > 2)
            {

                resultListProductRegion = listZoneTypeRegion.Where(x => x.ParentId == result[2].Id).ToList();
            }

            ViewBag.RegionParent = (result.Count > 2) ? result[2] : new ZoneByTreeViewMinify();
            ViewBag.Banner = string.Empty;
            ViewBag.Slide = 2;
            return View(resultListProductRegion);
        }
    }
}