﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;


using Microsoft.AspNetCore.Http;
using Enterbuy.Data.SqlServer.ModelDto;
using System.Threading.Tasks;
using System.Linq;

namespace Enterbuy.Services.Home.Components
{
    public class ModalAreaViewComponent : ViewComponent
    {
        private readonly ILocationDao _locationsRepository;
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    if (feature != null)
                    {
                        _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                    }
                    else
                        _currentLanguage = "vi-VN";
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    if (feature != null)
                    {
                        _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                    }
                    else
                        _currentLanguageCode = "vi-VN";
                }

                return _currentLanguageCode;
            }


        }
        public ModalAreaViewComponent(ILocationDao locationsRepository)
        {

            _locationsRepository = locationsRepository;
        }
        public IViewComponentResult Invoke()
        {
            var model = _locationsRepository.GetLocations(CurrentLanguageCode);
            ViewBag.Selected = model.FirstOrDefault();
            return View(model);
        }
    }
}
