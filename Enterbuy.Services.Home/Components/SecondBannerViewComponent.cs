﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Services.Home.Services;
using Utils;
using System;
using Enterbuy.Services.Common.Models;

namespace Enterbuy.Services.Home.Components
{
    public class SecondBannerViewComponent : ViewComponent
    {
        private readonly IBannerHomePageService _bannerHomePageService;

        public SecondBannerViewComponent(IBannerHomePageService bannerHomePageService)
        {
            _bannerHomePageService = bannerHomePageService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var now = DateTime.Now;
            var item = new BannerHomePage();
            if (!string.IsNullOrEmpty(item.DateStart) && !string.IsNullOrEmpty(item.DateEnd))
            {
                DateTime DateStart, DateEnd;
                if (DateTime.TryParse(item.DateStart, out DateStart) && DateTime.TryParse(item.DateEnd, out DateEnd))
                {
                    if (now >= DateStart && now <= DateEnd)
                    {
                        item = await _bannerHomePageService.GetBannerInBodySecond("vi-VN");
                        item.Image = UIHelper.StoreFilePath(item.Image, false);
                    }
                }
            }
            
            return View(item);
        }
    }
}