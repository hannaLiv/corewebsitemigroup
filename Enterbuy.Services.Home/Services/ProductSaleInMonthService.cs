﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.Home.Services
{
    public class ProductSaleInMonthService:IProductSaleInMonthService
    {
        private readonly IProductDao _productDao;

        public ProductSaleInMonthService(IProductDao productDao)
        {
            _productDao = productDao;
        }

        public List<ProductSaleInMonth> GetProductInFlashSale(int fSaleId, int locationId, string lanngCode, int pageIndex, int pageSize, out int total)
        {
            var result = _productDao.GetProductInFlashSale(fSaleId, locationId, lanngCode, 1, 10, out total);
            return result;
        }

        public async Task<List<DiscountProgram>> GetFlashSaleByTime(DateTime time)
        {
            var result = await _productDao.GetFlashSaleByTime(time);
            return result;
        }
    }
}