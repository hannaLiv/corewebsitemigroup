﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Enterbuy.Services.Common.Models;

namespace Enterbuy.Services.Home.Services
{
    public interface IBannerHomePageService
    {
        Task<BannerHomePage> GetBannerInBodyFirst(string langCode);
        Task<BannerHomePage> GetBannerInBodySecond(string langCode);
        Task<BannerHomePage> GetBannerInBodyThird(string langCode);
        Task<BannerHomePage> BannerTrangchuTrencung(string langCode);
        Task<List<BannerHomePage>> StemMenu(string langCode);
        Task<BannerHomePage> BannerTrangchuNgang2(string langCode);
        Task<BannerHomePage> BannerHethongdaily(string langCode);
        Task<BannerHomePage> BannerKhuyenmaiTrangchu(string langCode);
        Task<List<BannerHomePage>> SlideBody(string langCode);

    }
}