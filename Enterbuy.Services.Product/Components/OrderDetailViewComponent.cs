﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Mvc;

namespace Enterbuy.Services.Product.Components
{
    public class OrderDetailViewComponent : ViewComponent
    {
        private readonly IProductDao _productDao;
        public Utils.ICookieLocationUtility _cookie { get; set; }

        public OrderDetailViewComponent(IProductDao productDao, Utils.ICookieLocationUtility cookie)
        {

            _cookie = cookie;
            _productDao = productDao;
        }

        public IViewComponentResult Invoke(List<ProductVoucher> lstObj)
        {
            //Lay danh sach san pham theo list product_id
            var location = _cookie.SetCookieDefault();
            var total = 0;
            var model = _productDao.GetProductInListProductsMinify(String.Join(",", lstObj.Select(x => x.ProductId).ToList()), location.LocationId, Utils.Utility.DefaultLang, 1, lstObj.Count, out total);
            var lstPromotions = new List<PromotionViewModel>();
            var lstCoupon = new List<Coupon>();
            //v2: Lay tat ca khuyen mai, sau nay co the cache cai nay
            if (model != null && model.Any())
            {
                var lstIdPromotions = model.SelectMany(x => Utils.Utility.SplitStringToListInt(x.PromotionIds)).Distinct().ToList();
                lstPromotions = _productDao.GetPromotionsById(Utils.Utility.DefaultLang, lstIdPromotions);
            }

            ViewBag.Promotions = lstPromotions;

            if (lstObj.Any())
            {
                lstCoupon = _productDao.GetCouponsById(lstObj.Select(x => x.Voucher).Distinct().ToList(), lstObj.Select(x => x.ProductId).Distinct().ToList());

                //model.ForEach(x =>
                //x.Voucher = lstObj.FirstOrDefault(a => a.ProductId == x.ProductId) != null ? lstObj.FirstOrDefault(a => a.ProductId == x.ProductId).Voucher : "");
            }
            ViewBag.Coupons = lstCoupon;
            return View(model);
        }
    }
}
