﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JanHome.Web.ViewComponents
{
    [ViewComponent(Name = "ProductList")]
    public class ProductListViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneRepository;
        private readonly IProductDao _productRepository;
        private readonly IStringLocalizer<ProductListViewComponent> _localizer;
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        
        public ProductListViewComponent(IProductDao productRepository, IStringLocalizer<ProductListViewComponent> localizer)
        {
            _localizer = localizer;
            _productRepository = productRepository;
        }

        public IViewComponentResult Invoke(int zone_parent_id, int locationId)
        {
            var total = 0;
            var model = _productRepository.GetProductMinifiesTreeViewByZoneParentId(zone_parent_id, "vi-VN", locationId, 1, 9, out total);
            ViewBag.Total = total;
            ViewBag.Id = zone_parent_id;
            return View(model);
        }
    }
}
