﻿using Enterbuy.Data.SqlServer.Dao;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.Dto;
using Enterbuy.Data.SqlServer.ModelDto;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Product.Components
{
    public class FilterSpectificationInZoneViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneRepository;
        private readonly IProductDao _productRepository;
        private readonly IArticleDao _articleRepository;
        const string CookieLocationId = "_LocationId";
        const string CookieLocationName = "_LocationName";
        private string _currentLanguage;
        private string _currentLanguageCode;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        private string CurrentLanguageCode
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguageCode))
                    return _currentLanguageCode;

                if (string.IsNullOrEmpty(_currentLanguageCode))
                {
                    IRequestCultureFeature feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguageCode = feature.RequestCulture.Culture.ToString();
                }

                return _currentLanguageCode;
            }


        }
        public FilterSpectificationInZoneViewComponent(IZoneDao zoneRepository, IProductDao productRepository, IArticleDao articleRepository)
        {
            _zoneRepository = zoneRepository;
            _productRepository = productRepository;
            _articleRepository = articleRepository;
        }
        public IViewComponentResult Invoke(FilterProductBySpectification fp)
        {
            var parentId = fp.parentId;
            var total = 0;
            var model = _productRepository.FilterProductBySpectificationsInZone(fp, out total);
            ViewBag.Total = 0;
            ViewBag.ZoneParent = parentId;
            return View(model);
        }
    }
}
