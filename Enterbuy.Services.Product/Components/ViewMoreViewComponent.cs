﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enterbuy.Services.Product.Components
{
    public class ViewMoreViewComponent : ViewComponent
    {
        private readonly IZoneDao _zoneRepository;
        private readonly IProductDao _productRepository;
        private readonly Utils.ICookieLocationUtility _cookieLocationUtility;

        public ViewMoreViewComponent(IProductDao productRepository, Utils.ICookieLocationUtility cookieLocationUtility)
        {
            _productRepository = productRepository;
            _cookieLocationUtility = cookieLocationUtility;
        }

        public IViewComponentResult Invoke(int zone_parent_id, int locationId, int skip, int size)
        {

            var location = _cookieLocationUtility.SetCookieDefault();

            var total = size;
            ViewBag.Total = size;
            ViewBag.Id = zone_parent_id;
            var model = _productRepository.GetProductsInRegionByZoneParentIdShowLayout_Skipping(zone_parent_id, "vi-VN", location.LocationId, skip, size);
            return View(model);
        }
    }
}
