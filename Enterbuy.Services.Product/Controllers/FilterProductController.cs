﻿using System.Collections.Generic;
using System.Linq;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Product.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.WebInterface.Models;
using Microsoft.AspNetCore.Hosting;
using Enterbuy.Data.SqlServer.Dto;
using System;

namespace Enterbuy.Services.Product.Controllers
{
    public class FilterProductController : BaseController
    {
        private readonly IProductDao _productRepository;
        private readonly IZoneDao _zoneRepository;
        private Utils.ICookieLocationUtility _cookieUtility;
        private readonly IConfigDao _config;

        public FilterProductController(IProductDao productRepository, IZoneDao zoneRepository, Utils.ICookieLocationUtility cookieUtility, IConfigDao configDao)
        {
            _productRepository = productRepository;
            _zoneRepository = zoneRepository;
            _cookieUtility = cookieUtility;
            _config = configDao;
        }

        [Route("danh-muc/{region}.html")]
        public async Task<IActionResult> FilterProductByRegion(string region, int? pageIndex)
        {

            var pageSize = 12;
            ViewData["IsExpand"] = false;
            bool redrect = false;
            var data = HttpContext.Request.Path.Value;
            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                        redrect = true;
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                        redrect = true;
                    }
                }
            }
            if (!redrect)
            {
                var reg = await _zoneRepository.GetZoneByAlias(region, Utils.Utility.DefaultLang);
                if (reg != null)
                {
                    var total = 0;
                    var model = _productRepository.GetProductInRegionByZoneIdMinify(reg.Id, _cookieUtility.SetCookieDefault().LocationId, Utils.Utility.DefaultLang, pageIndex == null ? 1 : pageIndex.Value, pageSize, out total);
                    if (pageIndex == null)
                    {
                        ViewBag.Total = total;
                        ViewBag.Size = pageSize;
                        ViewBag.Url = region;
                        ViewBag.ZoneId = reg.Id;
                        return View(model);
                    }
                    if (pageIndex.Value > 1)
                    {
                        ViewBag.Total = total;
                        ViewBag.Size = pageSize;
                        ViewBag.Url = region;
                        ViewBag.ZoneId = reg.Id;
                        return PartialView("~/Views/Product/FilterProductSharing.cshtml", model);
                    }
                }
            }
            if (!redrect)
            {
                return View();
            }
            else
            {
                return Content("");
            }
        }
        public IActionResult FilterProductByTag(string tag, int? pageIndex)
        {

            //var pageSize = 12;
            //var reg = _extraRepository.GetTagTarget(tag);
            //if (reg != null)
            //{
            //    var total = 0;
            //    var model = _productRepository.GetProductInTagMinify(tag, _cookieUtility.SetCookieDefault().LocationId, CurrentLanguageCode, pageIndex == null ? 1 : pageIndex.Value, pageSize, out total);
            //    if (pageIndex == null)
            //    {
            //        ViewBag.Total = total;
            //        ViewBag.Size = pageSize;
            //        ViewBag.Url = tag;
            //        ViewBag.TagName = reg.Name;
            //        return View(model);
            //    }
            //    if (pageIndex.Value > 1)
            //    {
            //        ViewBag.Total = total;
            //        ViewBag.Size = pageSize;
            //        ViewBag.Url = tag;
            //        ViewBag.TagName = reg.Name;
            //        return PartialView("~/Views/Product/FilterProductSharing.cshtml", model);
            //    }
            //}
            return View();
        }

    }
}