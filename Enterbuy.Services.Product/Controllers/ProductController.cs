﻿using System.Collections.Generic;
using System.Linq;
using Enterbuy.Http.Controllers;
using Enterbuy.Services.Product.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.WebInterface.Models;
using Microsoft.AspNetCore.Hosting;
using Enterbuy.Data.SqlServer.Dto;
using MI.ES;
using System;

namespace Enterbuy.Services.Product.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductDao _productDao;
        private readonly IZoneDao _zoneDao;
        private readonly RamData.IConfig _config;
        private readonly IHostingEnvironment _env;
        private Utils.ICookieLocationUtility _cookie;

        public ProductController(IProductDao productDao, IZoneDao zoneDao, IHostingEnvironment env, Utils.ICookieLocationUtility cookie, RamData.IConfig config)
        {
            _productDao = productDao;
            _zoneDao = zoneDao;
            _env = env;
            _cookie = cookie;
            _config = config;

            Task.Run(() =>
            {
                if (Utils.Utility.DateMerge < DateTime.Now)
                {
                    Utils.Utility.DateMerge = DateTime.Now.AddHours(2);
                    var Check = Utils.Settings.AppSettings.GetByKey("ESEnable").ToLower();
                    if (Check == "True".ToLower())
                    {
                        MI.Service.SyncProductToES.RunEnterBuy();

                    }
                    else
                    {
                        Utils.Utility.DateMerge = DateTime.Now.AddHours(2);
                    }
                }
            });

        }

        [Route("gio-hang")]
        public IActionResult Cart()
        {
            ViewData["IsExpand"] = false;
            return View();
        }
        [Route("tra-gop/{url}-p{id}.html")]
        public async Task<IActionResult> Installment(int id)
        {
            ViewData["IsExpand"] = false;

            //Get Product 
            var p_detail = await _productDao.GetProductInfomationDetail(id, Utils.Utility.DefaultLang);
            //Get Price
            var location = _cookie.SetCookieDefault();
            var l_ProductPriceDetail = await _productDao.GetProductPriceInLocationDetail(id, Utils.Utility.DefaultLang);
            ProductPriceInLocationDetail o_ProductPriceInLocationDetail = null;
            foreach (var o in l_ProductPriceDetail)
            {
                if (o.LocationId == location.LocationId)
                {
                    o_ProductPriceInLocationDetail = o;
                    break;
                }
            }

            //Get Bank
            var l_BankInstallment = await _productDao.GetAllBankInstallment(Utils.Utility.DefaultLang);
            var promotionsInProduct = await _productDao.GetPromotionInProduct(id, Utils.Utility.DefaultLang);
            InstallmentViewModel o_InstallmentViewModel = new InstallmentViewModel();
            o_InstallmentViewModel.v_ProductDetail = p_detail;
            o_InstallmentViewModel.v_ProductPriceInLocationDetail = o_ProductPriceInLocationDetail;
            o_InstallmentViewModel.v_BankInstallment = l_BankInstallment;
            o_InstallmentViewModel.v_ProductInPromotion = promotionsInProduct;
            return View(o_InstallmentViewModel);
        }

        [HttpGet]
        public ProductSuggestResponse Get(string keyword, int pageIndex = 0, int pageSize = 10)
        {
            //MI.Service.SyncProductToES.Run();
            var Check = Utils.Settings.AppSettings.GetByKey("ESEnable").ToLower();
            if (Check == "True".ToLower())
            {
                long total = 0;
                return MI.ES.BCLES.AutocompleteService.SuggestEnterBuyAsync(keyword, Utils.Utility.DefaultLang, pageIndex, pageSize, out total);
            }
            else return new ProductSuggestResponse();

        }
        [HttpGet]
        public IActionResult Merge()
        {
            MI.Service.SyncProductToES.RunEnterBuy();
            return Ok("OK");
        }

        [HttpGet]
        public async Task<IActionResult> LoadInfoCard()
        {
            return Ok();
        }
        [HttpPost]
        public IActionResult LoadDropdownCart(string productIds)
        {
            return ViewComponent("DropdownCart", new { productIds = productIds });

        }
        [HttpPost]
        public IActionResult GetProductByZoneId(int zone_Id, int location_id)
        {
            return ViewComponent("ProductList", new { zone_parent_id = zone_Id, locationId = location_id });
        }

        [HttpPost]
        public IActionResult LoadOrderDetail(List<ProductVoucher> productIds)
        {
            return ViewComponent("OrderDetail", new { lstObj = productIds });

        }

        [HttpPost]
        public IActionResult GetComboByLocationId(int product_id, int location_id)
        {
            return ViewComponent("ComboInProductByLocation", new { product_id = product_id, location_id = location_id });
        }
        [HttpPost]
        public IActionResult LoadOrderDetailJson(string productIds)
        {
            var total = 0;
            var ck = _cookie.SetCookieDefault();
            var result = _productDao.GetProductInListProductsMinify(productIds, ck.LocationId, Utils.Utility.DefaultLang, 1, 1000, out total);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult GetQuanHuyen(string locationType, string parent)
        {
            var provinces_result = new Dictionary<string, QuanHuyen>();
            var provinces_json_part = _env.WebRootFileProvider.GetFileInfo("hanhchinhvn-master/dist/" + locationType + ".json")?.PhysicalPath;
            if (provinces_json_part != null)
            {
                var file_content = System.IO.File.ReadAllText(provinces_json_part);
                if (file_content != null)
                {
                    provinces_result = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, QuanHuyen>>(file_content);

                    var result = provinces_result.Where(r => r.Value.parent_code.Equals(parent));
                    var result_cooked = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                    return Ok(result_cooked);
                }
            }
            return BadRequest();
        }
        [HttpPost]
        public IActionResult VLastSeen(string product_ids)
        {
            return ViewComponent("ProductLastSeen", new { product_ids = product_ids });
        }

        [HttpPost]
        public IActionResult CreateOrder(OrderViewModel order)
        {
            var vouchers = _productDao.GetCouponsById(order.Products.Select(x => x.Voucher).Distinct().ToList(), order.Products.Select(x => x.ProductId).Distinct().ToList()).ToDictionary(x => x.ProductId, x => x);
            foreach (var item in order.Products)
            {
                if (vouchers.ContainsKey(item.ProductId))
                {
                    item.Voucher = vouchers[item.ProductId].Code;
                    var Type = vouchers[item.ProductId].DiscountOption;
                    switch (Type)
                    {
                        case 2:
                            item.VoucherPrice = vouchers[item.ProductId].ValueDiscount * item.Quantity;
                            break;
                        case 1:
                            var number = (((double)vouchers[item.ProductId].ValueDiscount / 100) * (double)item.LogPrice * item.Quantity);
                            item.VoucherPrice = number;
                            break;
                    }
                    item.VoucherType = Type;
                    item.VoucherMeta = "Giảm giá: " + Utils.UIHelper.FormatNumber(vouchers[item.ProductId].ValueDiscount, false) + (vouchers[item.ProductId].DiscountOption == 1 ? "%" : "đ");
                }
                if (item.Vat && item.VatPrice == 99999)
                {
                    double promotion = 0;
                    if (item.Promotions != null && item.Promotions.Count > 0)
                    {
                        promotion = item.Promotions.Where(x => x.LogType != "free-setup").Sum(x => x.LogType != "discount-percent" ? (double)x.LogValue : ((double)x.LogValue * 0.01) * (double)item.LogPrice);
                    }
                    var price = (double)item.LogPrice - item.VoucherPrice - promotion;

                    item.VatPrice = (price * item.Quantity) * 0.1;
                }
            }
            var result = _productDao.CreateOrderInWebsite(order);
            if (result > 0)
            {
                Utils.Utility.SendMail("Vừa có một yêu cầu mua sản phẩm mới được tạo . Đăng nhập cms để kiểm tra", _config.GetByName("MailMess"), _config.GetByName("MailManager"));
            }

            return Ok(result);
        }
        [HttpPost]
        public IActionResult CreateOrderInstallment(OrderViewModel order)
        {

            var result = _productDao.CreateOrderInstallmentInWebsite(order);
            if (result > 0)
            {
                Utils.Utility.SendMail("Vừa có một yêu cầu mua sản phẩm mới được tạo . Đăng nhập cms để kiểm tra", _config.GetByName("MailMess"), _config.GetByName("MailManager"));
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult CreateOrderStringtify(string order)
        {
            var serialized = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderViewModel>(order);
            var result = _productDao.CreateOrderInWebsite(serialized);
            if (result > 0)
            {
                Utils.Utility.SendMail("Vừa có một yêu cầu mua sản phẩm mới được tạo . Đăng nhập cms để kiểm tra", _config.GetByName("MailMess"), _config.GetByName("MailManager"));
            }
            return Ok(result);
        }
        [HttpPost]
        public IActionResult FilterSpectificationInZone(FilterProductBySpectification fp)
        {
            return ViewComponent("FilterSpectificationInZoneProductList", new { fp = fp });
        }

        [HttpPost]
        public IActionResult GetPropertyDetails()
        {
            var result = _productDao.GetPropertyDetails(Utils.Utility.DefaultLang);
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject(result));
        }
        [HttpPost]
        public IActionResult ViewMore(int zone_parent_id, int locationId, int skip, int size = 11)
        {
            var ck = _cookie.SetCookieDefault();
            return ViewComponent("ViewMore", new { zone_parent_id = zone_parent_id, locationId = ck.LocationId, skip = skip, size = size });
        }


        [HttpGet]
        public IActionResult CheckVoucher(int productId, string voucher)
        {
            KeyValuePair<bool, Coupon> obj = new KeyValuePair<bool, Coupon>(false, new Coupon { Name = "Mã này không phù hợp" });
            var coupon = _productDao.ExistVoucher(productId, voucher);
            if (coupon != null && coupon.Id > 0)
            {
                try
                {
                    string u = coupon.DiscountOption == 1 ? "%" : "đ";
                    coupon.Name = coupon.Name.Replace("[Price]", $"{Utils.UIHelper.FormatNumber2(coupon.ValueDiscount)} {u}");
                }
                catch (Exception ex)
                {

                    throw;
                }
                obj = new KeyValuePair<bool, Coupon>(true, coupon);
            }
            return Ok(obj);

        }

    }
}