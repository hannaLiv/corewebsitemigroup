﻿using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.DbContexts;
using Enterbuy.Data.SqlServer.ModelDto;
using System.Collections.Generic;

namespace Enterbuy.Services.Product.Services
{
    public class ProductService : IProductService
    {
        private readonly EnterbuyDbContext _enterbuyDbContext;
        private readonly IProductDao _productDao;

        public ProductService(EnterbuyDbContext enterbuyDbContext, IProductDao productDao)
        {
            this._enterbuyDbContext = enterbuyDbContext;
            _productDao = productDao;
        }

       
    }
}