﻿using Enterbuy.Services.Common.Services.Interfaces;
using Utils;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Core.Enumerations;
namespace Enterbuy.Services.Common.Components
{
    public class Top5BlogViewComponent : ViewComponent
    {
        private readonly IArticleDao _articleDao;

        public Top5BlogViewComponent(IArticleDao articleDao)
        {
            _articleDao = articleDao;
        }

        public IViewComponentResult Invoke(TypeZone type, TypeArticle typeArticle, string lstId = "")
        {

            if (string.IsNullOrEmpty(lstId))
            {
                int totalRow = 0;
                var result = _articleDao.GetArticlesInZoneId_Top5(0, (int)type, (int)typeArticle, Utility.DefaultLang, "", 1, 5, out totalRow);
                return View(result);
            }
            else
            {
                var result = _articleDao.GetArticlesRelated(Utility.DefaultLang, lstId);
                return View(result);

            }

        }
    }
}