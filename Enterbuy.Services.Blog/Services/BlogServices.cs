﻿using System;
using System.Collections.Generic;
using System.Text;
using Dapper;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Data.SqlServer.ModelDto;

namespace Enterbuy.Services.Blog.Services
{
    public class BlogServices : IBlogServices
    {
        private readonly IArticleDao _articleDao;

        public BlogServices(IArticleDao articleDao)
        {
            _articleDao = articleDao;
        }
        public ArticleDetail GetArticleDetail(int article_id, string lang_code)
        {
            ArticleDetail articleDetail = new ArticleDetail();
            try
            {
                articleDetail = _articleDao.GetArticleDetail(article_id, lang_code);
            }
            catch (Exception ex)
            {
            }
            return articleDetail;
        }
        public List<ArticleMinify> GetArticlesInZoneId_Minify_Rss(int zoneId, int zone_type, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            List<ArticleMinify> lstData = new List<ArticleMinify>();
            totalRow = 0;
            try
            {
                lstData = _articleDao.GetArticlesInZoneId_Minify_Rss(zoneId, zone_type, lang_code, filter, pageIndex, pageSize, out totalRow);
            }
            catch (Exception ex)
            {

            }
            return lstData;
        }
        public List<ArticleMinify> GetArticlesInZoneId_Minify(int zoneId, int zone_type, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            List<ArticleMinify> lstData = new List<ArticleMinify>();
            totalRow = 0;
            try
            {
                lstData = _articleDao.GetArticlesInZoneId_Minify(zoneId, zone_type, lang_code, filter, pageIndex, pageSize, out totalRow);
            }
            catch (Exception ex)
            {

            }
            return lstData;
        }

        public List<ArticleMinify> GetArticlesInZoneId_Minify_FullFilter(int zoneId, int zone_type, int article_type, int? isHot, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            List<ArticleMinify> articleMinifies = new List<ArticleMinify>();
            totalRow = 0;
            try
            {
                articleMinifies = _articleDao.GetArticlesInZoneId_Minify_FullFilter(zoneId, zone_type, article_type, isHot, lang_code, filter, pageIndex, pageSize, out totalRow);
            }
            catch (Exception ex)
            {

            }
            return articleMinifies;
        }

        public List<ArticleMinify> GetArticlesInZoneId_Minify_AddFilterHot(int zoneId, int zone_type, int? isHot, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow)
        {
            List<ArticleMinify> articleMinifies = new List<ArticleMinify>();
            totalRow = 0;
            try
            {
                articleMinifies = _articleDao.GetArticlesInZoneId_Minify_AddFilterHot(zoneId, zone_type, isHot, lang_code, filter, pageIndex, pageSize, out totalRow);
            }
            catch (Exception ex)
            {

            }
            return articleMinifies;
        }

        public List<ArticleMinify> GetArticlesSameTag(string tag, string lang_code, int? pageIndex, int? pageSize, out int total)
        {
            List<ArticleMinify> articleMinifies = new List<ArticleMinify>();
            total = 0;
            try
            {
                articleMinifies = _articleDao.GetArticlesSameTag(tag, lang_code, pageIndex, pageSize, out total);
            }
            catch (Exception ex)
            {

            }
            return articleMinifies;
        }

        //public RedrectDetail GetObjectByAlias(string url, string lang_code)
        //{
        //    RedrectDetail redirechArticle = new RedrectDetail();
        //    try
        //    {
        //        redirechArticle = _articleDao.GetObjectByAlias(url, lang_code);
        //    }
        //    catch (Exception ex)
        //    {
                
        //    }
        //    return redirechArticle;
        //}

        public List<RedrectDetail> GetCategoryByIdObj(List<int> lstId, int type, string lang_code)
        {
            List<RedrectDetail> redirechArticle = new List<RedrectDetail>();
            try
            {
                redirechArticle = _articleDao.GetCategoryByIdObj(lstId, type, lang_code);
            }
            catch (Exception ex)
            {

            }
            return redirechArticle;
        }
    }
}
