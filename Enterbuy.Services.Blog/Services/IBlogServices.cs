﻿using Enterbuy.Data.SqlServer.ModelDto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Enterbuy.Services.Blog.Services
{
    public interface IBlogServices
    {
        List<RedrectDetail> GetCategoryByIdObj(List<int> lstId, int type, string lang_code);
        List<ArticleMinify> GetArticlesInZoneId_Minify_Rss(int zoneId, int zone_type, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow);
        List<ArticleMinify> GetArticlesInZoneId_Minify(int zoneId, int zone_type, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow);
        List<ArticleMinify> GetArticlesInZoneId_Minify_FullFilter(int zoneId, int zone_type, int article_type, int? isHot, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow);
        List<ArticleMinify> GetArticlesInZoneId_Minify_AddFilterHot(int zoneId, int zone_type, int? isHot, string lang_code, string filter, int? pageIndex, int? pageSize, out int totalRow);
        List<ArticleMinify> GetArticlesSameTag(string tag, string lang_code, int? pageIndex, int? pageSize, out int total);
        ArticleDetail GetArticleDetail(int article_id, string lang_code);
        
    }
}
