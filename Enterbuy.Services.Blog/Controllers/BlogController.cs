﻿using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.ModelDto;
using Enterbuy.Http.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Enterbuy.Services.Common.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System;
using Enterbuy.Services.Blog.Services;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using System.Text.RegularExpressions;

namespace Enterbuy.Services.Blog.Controllers
{
    public class BlogController : BaseController
    {
        private readonly IArticleDao _blogServices;
        private readonly IZoneDao _menuZoneServices;
        private readonly IConfigDao _config;

        public BlogController(IZoneDao menuZoneServices, IArticleDao blogServices, IConfigDao config)
        {
            _menuZoneServices = menuZoneServices;
            _blogServices = blogServices;
            _config = config;
        }
        public async Task<IActionResult> Index(string alias)
        {
            var data = HttpContext.Request.Path.Value;
            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                    }
                }
            }

            var zone_tar = await _menuZoneServices.GetZoneByAlias(alias, Utils.Utility.DefaultLang);
            ViewData["IsExpand"] = false;
            if (zone_tar != null && zone_tar.Id > 0)
            {
                var list_zone_blog = await _menuZoneServices.GetZoneByTreeViewMinifies((int)TypeZone.Article, Utils.Utility.DefaultLang, 0);
                var zone_blog_parent = list_zone_blog.Where(r => r.ParentId == 0 && r.Id == zone_tar.Id).FirstOrDefault();

                if (zone_blog_parent != null)
                {
                    ViewBag.ZoneParent = zone_blog_parent;
                    var list_blog_childs = list_zone_blog.Where(r => r.ParentId == zone_blog_parent.Id).ToList();
                    if (list_blog_childs != null)
                        ViewBag.ZoneChilds = list_blog_childs;
                    else
                        ViewBag.ZoneChilds = new List<ZoneByTreeViewMinify>();
                }
            }
            else
            {
                return NotFound();
            }

            return View();
        }

        [Route("news/{alias}")]
        [Route("news/{alias}/page/{pageIndex}")]
        public IActionResult RedirectActionOld(string alias, int? pageIndex, int? pageSize)
        {
            bool redriect = false;
            var data = HttpContext.Request.Path.Value;
            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                        redriect = true;
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                        redriect = true;
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            if (!redriect)
            {
                return NotFound();
            }
            else
            {
                return Content("");
            }
            //var zone_tar = await _menuZoneServices.GetZoneByAlias(alias, Utils.Utility.DefaultLang);
            //if (zone_tar != null)
            //{
            //    return RedirectToAction("RedirectAction", new { alias = zone_tar.Url, pageIndex = pageIndex, pageSize = pageSize });
            //}
            //else
            //{
            //    return NotFound();
            //}
        }

        [Route("news/{alias}.html")]
        [Route("news/{alias}/page/{pageIndex}.html")]
        public async Task<IActionResult> RedirectAction(string alias, int? pageIndex, int? pageSize)
        {

            var data = HttpContext.Request.Path.Value;
            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                    }
                }
            }

            pageIndex = pageIndex ?? 1;
            pageSize = pageSize ?? 10;
            ViewData["IsExpand"] = false;
            var zone_tar = await _menuZoneServices.GetZoneByAlias(alias, Utils.Utility.DefaultLang);
            if (zone_tar != null)
            {
                if (zone_tar.Url.Equals(alias))
                {
                    ViewBag.ZoneId = zone_tar.Id;
                    ViewBag.Type = zone_tar.Type;
                    ViewBag.Parent = zone_tar.ParentId;
                    ViewBag.PageIndex = pageIndex;
                    ViewBag.PageSize = pageSize;
                    ViewBag.IsHaveChild = zone_tar.isHaveChild;
                    ViewBag.MetaCanonical = zone_tar.MetaCanonical;
                    ViewBag.MetaNoIndex = zone_tar.MetaNoIndex;
                }
                else
                {
                    HttpContext.Response.Redirect(Utils.BaseBA.UrlCategoryNews(alias), true);
                }
            }
            else
            {
                return NotFound();
            }
            return View();
        }

        [Route("news/{category}/{url}-{articleId}.html")]
        [Route("news/{url}-{articleId}.html")]

        public IActionResult RedirectDetailOld(string url, int articleId, int? pageIndex, int? pageSize)
        {
            var redirect = false;

            var data = HttpContext.Request.Path.Value;
            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                        redirect = true;
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                        redirect = true;
                    }
                }

            }
            if (!redirect)
            {
                var objBlog = _blogServices.GetRedirectByArticleId(articleId, Utils.Utility.DefaultLang);
                if (objBlog != null && !String.IsNullOrEmpty(objBlog.Url) && objBlog.Url.ToLower() == url.ToLower())
                {
                    HttpContext.Response.Redirect(Utils.BaseBA.UrlNews("", objBlog.Url), true);
                }
                else
                {
                    return NotFound();
                }
            }
            return Content("");
        }

        [Route("tin-tuc/{url}.html")]
        public IActionResult Details(string url)
        {
            var redirect = false;
            ArticleDetail articleDetail = new ArticleDetail();
            ViewData["IsExpand"] = false;

            var data = HttpContext.Request.Path.Value;

            if (!String.IsNullOrEmpty(data))
            {
                var obj = _config.GetUrlRedrect(data);
                if (obj != null && !String.IsNullOrEmpty(obj.UrlOld))
                {
                    if (obj.UrlType == 301)
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, true);
                        redirect = true;
                    }
                    else
                    {
                        HttpContext.Response.Redirect(obj.UrlNew, false);
                        redirect = true;
                    }
                }
            }

            try
            {
                if (!redirect)
                {

                    articleDetail = _blogServices.GetObjByAlias(url, Utils.Utility.DefaultLang);
                    if (articleDetail != null && articleDetail.Id > 0)
                    {
                        if (articleDetail.Url != url)
                        {
                            if (articleDetail.Url.ToLower() == url.ToLower())
                            {
                                HttpContext.Response.Redirect(Utils.BaseBA.UrlNews("", articleDetail.Url), true);
                                redirect = true;
                            }
                            else
                            {
                                if (articleDetail.UrlOld != url)
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    HttpContext.Response.Redirect(Utils.BaseBA.UrlNews("", articleDetail.Url), true);
                                    redirect = true;
                                }
                            }
                        }

                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            catch (Exception ex)
            {

            }
            if (redirect)
            {
                return Content("");
            }
            else
            {
                return View(articleDetail);
            }

        }
        [HttpPost]
        public IActionResult ProductsInArticle(string product_ids, int location_id)
        {
            return ViewComponent("ProductListInArticle", new { product_ids = product_ids, location_id = location_id });
        }
    }
}