﻿using Enterbuy.Core.Enumerations;
using Enterbuy.Data.SqlServer.Dao.Interfaces;
using Enterbuy.Services.Blog.Services;
using Enterbuy.Services.Common.Services.Interfaces;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Utils;

namespace Enterbuy.Services.Blog.Controllers
{
    [Produces("application/xml")]
    public class RssController : Controller
    {
        private readonly IBlogServices _blogServices;        
        private readonly IMenuZoneServices _menuZoneServices;
        private readonly IProductDao _IProductDao;
        private readonly ICookieLocationUtility _Cookie;
        public RssController(IMenuZoneServices menuZoneServices, IBlogServices blogServices, IProductDao IProductDao, ICookieLocationUtility Cookie)
        {
            _menuZoneServices = menuZoneServices;
            _blogServices = blogServices;
            _IProductDao = IProductDao;
            _Cookie = Cookie;
        }
        [Route("RSS")]
        public IActionResult Index()
        {
            ViewData["IsExpand"] = false;
            return View();
        }
        private string cutTaga (string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, "(<[a|A][^>]*>|)", string.Empty);
        }
        [Route("RSS/Article/{url}")]
        public IActionResult DetailArticle(int id, int type)
        {
            var domain_full = HttpContext.Request.GetDisplayUrl();
            var location = _Cookie.SetCookieDefault();
            var host = HttpContext.Request.Host;
            //var scheme = HttpContext.Request.Scheme + "://";
            var scheme = "https" + "://";
            string alias = domain_full.Split("/").LastOrDefault();
            XNamespace ns = "http://www.w3.org/2005/Atom";
            var rss = new XElement("rss", new XAttribute("version", "2.0"), new XAttribute(XNamespace.Xmlns + "atom", ns));
            if (!string.IsNullOrEmpty(alias))
            {

                var channel = new XElement("channel",
                    new XElement("title", "EnterBuy RSS"),
                    new XElement("description", "EnterBuy RSS"),
                    new XElement("language", Utils.Utility.DefaultLang),
                    new XElement("copyright", $"Copyright 2020-{DateTime.UtcNow.Year} EnterBuy"),
                    new XElement("image", UIHelper.StoreFilePath("/2020/05/20/logo-enterbuy-1%201.png"))
                 );
                int total = 0;
                var listData = _blogServices.GetArticlesInZoneId_Minify_Rss(id, type, Utility.DefaultLang, "", 1, 15, out total);                
                foreach (var item in listData)
                {
                    var postInRss = new XElement("item");
                    postInRss.Add(new XElement("title", item.Title));
                    postInRss.Add(new XElement("description", item.Description));
                    postInRss.Add(new XElement("content", cutTaga(item.Body)));
                    postInRss.Add(new XElement("link", scheme + host + "/tin-tuc/" + item.Url + "-" + item.Id + ".html"));
                    postInRss.Add(new XElement("pubDate", item.CreatedDate.ToString("dd/MM/yyyy hh:mm")));                    
                    postInRss.Add(new XElement("image", UIHelper.StoreFilePath(item.Avatar)));
                    channel.Add(postInRss);
                } 
                rss.Add(channel);
            }    
            return Ok(rss);
        }
        [Route("RSS/Product/{url}")]
        public IActionResult DetailsProduct(int id, int type)
        {
            var host = HttpContext.Request.Host;
            //var scheme = HttpContext.Request.Scheme + "://";
            var scheme = "https" + "://";
            var domain_full = HttpContext.Request.GetDisplayUrl();
            var location = _Cookie.SetCookieDefault();
            string alias = domain_full.Split("/").LastOrDefault();
            XNamespace ns = "http://www.w3.org/2005/Atom";
            var rss = new XElement("rss", new XAttribute("version", "2.0"), new XAttribute(XNamespace.Xmlns + "atom", ns));
            if (!string.IsNullOrEmpty(alias))
            {

                var channel = new XElement("channel",
                    new XElement("title", "EnterBuy RSS"),
                    new XElement("description", "EnterBuy RSS"),
                    new XElement("language", Utils.Utility.DefaultLang),
                    new XElement("copyright", $"Copyright 2020-{DateTime.UtcNow.Year} EnterBuy"),
                    new XElement("image", UIHelper.StoreFilePath("/2020/05/20/logo-enterbuy-1%201.png"))
                 );
                int total = 0;                
                var listData = _IProductDao.GetProductMinifiesTreeViewByZoneParentId_Rss(id, Utility.DefaultLang, location.LocationId, 1, 10, out total);
                foreach (var item in listData)
                {                    
                    var postInRss = new XElement("item");
                    postInRss.Add(new XElement("title", item.Title));
                    postInRss.Add(new XElement("description", item.MetaDescription));
                    postInRss.Add(new XElement("content", cutTaga(item.Content)));
                    postInRss.Add(new XElement("link", scheme + host + "/san-pham/" + item.Url + ".html" ));                    
                    postInRss.Add(new XElement("image", UIHelper.StoreFilePath(item.Avatar)));
                    channel.Add(postInRss);
                }
                rss.Add(channel);
            }
            return Ok(rss);
        }
    }
}
